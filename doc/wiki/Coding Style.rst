=================
**Coding Style**
=================
Here we describe the coding standards to make the source code more readable and maintainable.

**1. Python coding Style**
--------------------------------------
`autopep8`_ tool is python code formatter, which will format you code to fit `Python coding standard`_. This will free you from code style (indention,comments,..etc) issues. 

Usage
^^^^^^^^^
`autopep8`_ comes with `pydev`_ utility, but it's not enabled by default. to enable and use it, follow steps bellow .

- First you should have `pydev`_ installed
- In you eclipse open *Window > PyDev > Editor > Code Style > Code Formatter*
- Check 'Use autopep8.py  for code formatting ?', then click *Apply*
- Now, in any python code you want to format(and you should) press :code:`Ctrl+Shift+F`
- It's done.

**2. Our Style**
-------------------------
Although, python coding standard and its tool will free us from coding style stuff, we still have to aware of **Naming Convention** issues. please apply this issues to your code before commit it.

Classes & packages
^^^^^^^^^^^^^^^^^^^^^^^^^^
- packages name should be in lower case and meaningful, that is all :).
- class name should be meaningful and in *CamelCase* .
- class documentation must describe at least the usage,and author of class
- *add here*

Methods
^^^^^^^^^^^^
- methods name should be meaningful and in lower case letters
- if the name contains of more than one word should be seperated with :code:`_`, i.e :code:`get_name`.

- also documentation must describe at least the usage,and author of this method.
- *add here*

Documentation
^^^^^^^^^^^^^^^^^^^^
all documentation and wiki pages(like this) should follow python **reStructuredText** syntax, for more info about RST see `reStructuredText site`_

|

*Regards*

**\- Ahmed -**


.. _`Python coding standard`: https://www.python.org/dev/peps/pep-0008/
.. _`autopep8`: https://pypi.python.org/pypi/autopep8/
.. _`pydev`: http://pydev.org
.. _`reStructuredText site`: http://docutils.sourceforge.net/rst.html




