Wiki
=======

This is home wiki page, add here what you want.


Project Structure
---------------------

.. code::

	@- team-studio
	├———————o doc
	|       └———o wiki
	|		|	├——— ( All wiki pages)
	|		|	├——— Home.rst
	|		|	└——— ...
	|       └——— ( Any other documentation)
    |
	├———————o src
	|       ├———o api (our modules)
	|		|	├———o base
	|		|	├———o db
	|		|	├———  ...
	|		|	└———  __init__.py
	|		|
	|       ├———o template
	|		|	├———o static
	|		|	├———o css
	|		|	├———o fonts
	|		|	├———o js
	|		|	├——— ( any other html file )
	|		|	└——— ...
	|		|
	|       └———  main.py (the main entry)
	|
	├———————o test
	|       └——— (should be same as 'src')
	|
	├———————  .project
	├———————  .pydevproject
	├———————  license.txt
	└———————  readme.rst


Notes
---------------------
- **templates**

  All templates should be written in foundation for app version, and keep your design simple and flat.
  
- **database**

  All model classes should extend :code:`api.db.model.BaseModel` class. and use :code:`api.db.model.database` instance to initilize connection to the target database engine.
  
- **documentation**

  Keep your code well documented ( when needed), and have meaningful nouns.


Project Plan
---------------------

+-------+-------+----------------------------------------+
| From  |  To   |              Description               |
+=======+=======+========================================+
|...    |...    |....                                    |
+-------+-------+----------------------------------------+
|...    |...    |....                                    |
+-------+-------+----------------------------------------+