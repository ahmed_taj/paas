====================
**Introduction** 
====================

Welcome, Ayman and Hammad, here we go.

|

====================
**Getting started**
====================

This section provides useful information of how to start contributing to this project


- **Coding Styles**

  Before getting started, please read `coding style`_ ,and home_ wiki pages.

..

- **Questions and Discusssions**

  All team member's discussions or questions will use `hipChat`_ communication tool (in elite-sudan room), in order
  to gather some requirements.
  
  You can use `web based`_ or desktop_ version of `hipChat`_ ( I recommend desktop version, you know why).
  
  When you use `hipChat`_ you will receive all notifications about this repository (issues,commits,wiki changes...etc).

..

- **Features List**

  Please visit `features list`_ page to see what is going on. and add features/bugs/tasks. 
  
  **Notes :**
  
  * each new task should start in new (if it's independent ) branch, and merged when task complete (at least without syntax error) .
  * if you found any bug, feel great to report it using the same link above (by creating new bug) .
  * when you finished coding (not testing) of task, you can mark it as :code:`testing` to announce team members to contribute in testing process.
  * when **all** team agreed a task has been fully tested, you can then mark it as :code:`finished`.
  * you must **provide** test cases of your module in a seperated directory in :code:`test` folder. i.e :code:`test\mymodule`
  * you are **responsible** of creating wiki pages (when needed) for your tasks.
  * you are **responsible** of manual merging (when needed) of your branch.
  * keep it **simple** and **reusable**.
  * always start task that has high priority in `features list`_.
  * try your best follow `Project Plan`_ deadlines.
..

- **Submit changes**

  Please visit `this tutorial`_ . Note it's assume you have a Git_ version control system installed.

  This is also useful link_ to learn Git_ basics.

..
 
- **Documentations**

  Please, please, please , Always update your wiki pages and source code documentation . 

|

*Regards.*

  
**\- Ahmed -**

.. _Git: http://git-scm.com/
.. _`hipChat` : http://hipchat.com
.. _desktop : https://www.hipchat.com/downloads/latest/qtwindows
.. _`web based` : https://elite-sudan.hipchat.com/chat
.. _`features list` : https://bitbucket.org/ahmed_taj/team-studio/issues
.. _`documentation` : https://bitbucket.org/ahmed_taj/team-studio/src/master/doc/wiki/Home.rst
.. _home : https://bitbucket.org/ahmed_taj/team-studio/src/master/doc/wiki/Home.rst
.. _`Project Plan` : https://bitbucket.org/ahmed_taj/team-studio/src/master/doc/wiki/Home.rst
.. _`coding style` : https://bitbucket.org/ahmed_taj/team-studio/src/master/doc/wiki/Coding%20Style.rst
.. _link : http://rogerdudler.github.io/git-guide/
.. _`this tutorial` : https://confluence.atlassian.com/display/BITBUCKET/Clone+your+Git+repository+and+add+source+files#CloneyourGitrepositoryandaddsourcefiles-Step1.Cloneyourrepositorytoyourlocalsystem