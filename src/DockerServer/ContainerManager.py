'''
Created on May 31, 2015

@author: hammad
'''
import os
import DockerAPI as Docker
from tornado.escape import json_encode,json_decode

repo = os.environ["REPO_ROOT"]
redis = "test"

"""
all containers in these examples are not real
only used for desribing hierhcyasdhcyhyshd of containers
"""
#TODO: fix docker

conf = {}
def writeConf(conf):
    file = open('docker.conf','w')
    file.write(json_encode(conf))    

def read_conf():
    global conf
    file = open('docker.conf','r').read()
    conf = json_decode(file)
    

def get_conf(id):
    global conf
    return conf[id]

#deprecated and useless only remains to remember the scheme of conf
def get_container_conf(containerID):
    conf = {}
    conf["base"] = "ubuntu"
    conf["volume_from"] = "/home/hammad/Desktop/fakerepo/users/hammad/Elite/Grad-Project";
    conf["depend"] = [] # ['container1','container2']
    return conf

def start_project(user,project,company,usrcmd):
    read_conf()
    containerID = get_container_id(user, project, company)
    #containerID = "container0"
    run_container_depdencies(containerID)
    print("trying to run python")
    #usrcmd = "python app/hello.py"
    Docker.run_command(containerID,usrcmd)
    #print("docker runs: ",containerID,usrcmd)
    
def run_container_depdencies(containerID):
    #get_current container dependencies
    config = get_conf(containerID)
    depend = config.get("depend")
    if(depend != None):
        for container in depend:
            run_container_depdencies(container)
    try:
        Docker.run_container(containerID)
    except:
        Docker.start(containerID)
        #Docker.run_container(containerID)
    print("container started",containerID)

def get_container_id(user,project,company):
    return user+"."+company+"."+project


#print(get_container_id("hammad", "grad", "elite"))
#start_project("hammad", "grad", "elite", "python app/hello.py")
