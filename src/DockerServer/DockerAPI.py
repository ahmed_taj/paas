'''
Created on Jun 23, 2015

@author: hammad
'''
import redis
import subprocess

redis = redis.Redis('127.0.0.1')

#TODO: add save image
#TODO: add edit container
def add_container(name,id="",type="ubuntu",link="",link_name="",to_run="bin/bash",ports="",volume_from="",volume_to="/app"):
    if(not redis.exists("docker:container:"+name)):
        redis.hset("docker:container:"+name,"name",name)
        if(id!=""):
            redis.hset("docker:container:"+name,"id",id)
        if(type!=""):
            redis.hset("docker:container:"+name,"type",type)
        if(link!=""):
            redis.hset("docker:container:"+name,"link",link)
        #TODO:add multiple links
        
        if(link_name!=""):
            redis.hset("docker:container:"+name,"link_name",link_name)
        
        redis.hset("docker:container:"+name,"to_run",to_run)
        if(ports!=""):
            redis.hset("docker:container:"+name,"ports",ports)
        if(volume_from!=""):
            redis.hset("docker:container:"+name,"volume_from",volume_from)
        if(volume_to!=""):
            redis.hset("docker:container:"+name,"volume_to",volume_to)
        print('container added: ',name)
    else:
        print("container exists")
#add_container('hammad.elite.grad', type="python/flashapps",volume_from="/home/hammad/Desktop/fakerepo/users/hammad/Elite/Grad-Project")
        
def edit_container(name,id="",type="",link="",link_name="",to_run="",ports="",volume_from="",volume_to=""):
    if(redis.exists("docker:container:"+name)):
        redis.hset("docker:container:"+name,"name",name)
        if(id!=""):
            redis.hset("docker:container:"+name,"id",id)
        if(type!=""):
            redis.hset("docker:container:"+name,"type",type)
        if(link!=""):
            redis.hset("docker:container:"+name,"link",link)
        #TODO:add multiple links
        if(link_name!=""):
            redis.hset("docker:container:"+name,"link_name",link_name)
        if(to_run!=""):
            redis.hset("docker:container:"+name,"to_run",to_run)
        if(ports!=""):
            redis.hset("docker:container:"+name,"ports",ports)
        if(volume_from!=""):
            redis.hset("docker:container:"+name,"volume_from",volume_from)
        if(volume_to!=""):
            redis.hset("docker:container:"+name,"volume_to",volume_to)
    else:
        print("container doesn't exists")
                
def remove_container(name):
    if(redis.delete("docker:container:"+name)==1):
        subprocess.check_output("sudo docker rm -f "+name,shell=True)
    else:
        print("doesnt exist")

def run_container(name):
    try:
        rname = "docker:container:"+name
        if(redis.exists(rname)):
            container = b"sudo docker run -i -d --name "+name.encode()
            link = redis.hget(rname,"link")
            link_name = redis.hget(rname,"link_name")
            if(link!=None and link_name != None): #test this
                container+= b" --link "+link+b":"+link_name
            ports = redis.hget(rname,"ports")
            if(ports!=None):
                container+=b" -p "+ports
            volume_from = redis.hget(rname,"volume_from")
            volume_to = redis.hget(rname,"volume_to")

            if(volume_from!=None and volume_to!=None):
                container += b" -v "+volume_from+b":"+volume_to
            print(container)
            container+=b" "+redis.hget(rname,"type")+b" "+redis.hget(rname,"to_run")
            print("docker")
            x = subprocess.check_output(container,shell=True)
            print(b"this is my id:"+x)
        else:
            print("container doesn't exist")
    except subprocess.CalledProcessError as e:
            print(e)
            raise Exception("running container Failed")

def run_command(name,command):
    print("docker:container:"+name)
    if(redis.exists("docker:container:"+name)):
        try:
            x = subprocess.check_output("sudo docker exec "+name+" "+command,shell=True)
            print(x)
        except subprocess.CalledProcessError as e:
            print(e)
    else:
        print("Container doesn't exist")

def start(name):
    if(redis.exists("docker:container:"+name)):
        try:
            x = subprocess.check_output("sudo docker start "+name,shell=True)
            print(x)
        except subprocess.CalledProcessError as e:
            print(e)
    else:
        print("Container doesn't exist")

        
def stop(name):
    if(redis.exists("docker:container:"+name)):
        try:
            x = subprocess.check_output("sudo docker stop "+name,shell=True)
            print(x)
        except subprocess.CalledProcessError as e:
            print(e)
    else:
        print("Container doesn't exist")

#remove_container("container0")
#add_container(name='hammad.elite.grad',type='python/flashapps',volume_from="/home/hammad/Desktop/fakerepo/users/hammad/Elite/Grad-Project")