'''
Created on Aug 27, 2015

@author: hammad
'''
import socket     
import pickle
import sys
print("\t\t\t\t***Client***")
print("Connecting...")
s = socket.socket()         # Create a socket object

def initiate_connection():
    global s
    host = socket.gethostname() # Get local machine name
    port = 12345                # Reserve a port for your service.
    for i in range(20):
        try:
            s.connect((host, port+i))
            print("Connected!")
            break
        except:
            print("trying another port")
    
def send_data(name,project,company,cmd):
    list= {"user":name,"project":project,"company":company,'cmd':cmd}
    data = pickle.dumps(list)
    s.send(data)
    response = pickle.loads(s.recv(2048))
    #FIXME: add repetitive redis database
    
    if(response["status"]==1):
        print("created")
        print(response['data'])
        return response["data"]
    else:
        print("error")
        return None

initiate_connection()
"""
while True:
    name = input("press enter username")
    project = input("press enter projectname")
    company = input("press enter company")
    cmd = input("press enter cmd")
    send_data(name,project,company,cmd)
"""