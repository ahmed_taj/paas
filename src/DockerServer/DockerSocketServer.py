'''
Created on Aug 27, 2015

@author: hammad
'''
import sys
import socket
import pickle
import ContainerManager
import threading
from time import sleep
print("\t\t\t\t***Docker Server***")
print("Waiting For Connection...")
s = socket.socket()         # Create a socket object
c = None
addr = None

def initiate_server():
    global s,c,addr
    host = socket.gethostname() # Get local machine name
    port = 12345                # Reserve a port for your service.
    s.bind((host, port))        # Bind to the port
    
    s.listen(1)                 # Now wait for client connection.
    
    c, addr = s.accept()     # Establish connection with client.
    print ('Got connection from', addr)

def start_docker_handler():
    global c

    while True:
        print("trying to get connection")
        recv = c.recv(2048)
        print(recv)
        list = pickle.loads(recv)
        t = threading.Thread(target=start_container,args=(list,))
        t.start()
        print("accepted a connection ")
def start_container(list):
    user = list["user"]
    project = list["project"]
    company = list["company"]
    usrcmd = list["cmd"]
    print(list)
    ContainerManager.start_project(user, project, company, usrcmd)
    print("Container started")
    
try:
    initiate_server()
    start_docker_handler()
except :
    print("exception")
    for i in sys.exc_info():
        print(i)
    s.close()

        