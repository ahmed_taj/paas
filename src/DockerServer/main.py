'''
Created on Aug 20, 2015

@author: hammad
'''
#public_key = 
import tornado.web as web
import tornado.ioloop as ioloop
import ContainerManager as Containers
from tornado.escape import json_decode
def make_app():
    return web.Application([
                            (r"/",IndexHandler),
                            (r"/run",RunContainerHandler)])

class IndexHandler(web.RequestHandler):
    def get(self):
        print("hello world")
        self.write("hello world")
    
class RunContainerHandler(web.RequestHandler):
    def get(self):
        print("handler")
        print(self.request.body)
        data = json_decode(self.request.body)
        print("in handler")
        #Containers.start_project("hammad", "grad-project", "elite", "cmd command")
        print(data)
        
    def post(self):
        print("handler")
        print(self.request.body)
        data = json_decode(self.request.body)
        print("in handler")
        #Containers.start_project("hammad", "grad-project", "elite", "cmd command")
        print(data)
def main():
    app = make_app()
    app.listen(9090)
    ioloop.IOLoop.current().run_container()
print("started")    
main()
