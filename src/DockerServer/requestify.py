import tornado.gen as gen
import tornado.httpclient as httpclient
import tornado.ioloop as ioloop
from tornado.escape import json_encode

@gen.coroutine
def async_request(url,data,method):
    http_client = httpclient.AsyncHTTPClient()
    request = httpclient.HTTPRequest(url,body = data,method=method)
    #request.
    
    response = yield http_client.fetch(request,raise_error=False)
    return response
    
def post(url,data=None):
    return ioloop.IOLoop.current().run_sync(lambda : async_request(url,data,method='POST'))
def get(url):
    return ioloop.IOLoop.current().run_sync(lambda : async_request(url,method='GET'))
#
print(post("http://localhost:9090/run",data=json_encode({"data":"hammad","id":1})))