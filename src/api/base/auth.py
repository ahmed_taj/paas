'''
Created on Apr 2, 2015

@author: One
'''

from api.base.handle import BaseHandler
from api.db.model import User, database, Company, ProjectMember, ProjectInvites , Project 
from tornado.escape import json_decode, json_encode
import tornado.web 
import peewee as orm
import os
import api.vcs.git2 as git
from api.utils.general import checkTemp
import pytz
import sys


# database env
db_url = os.environ["DB_URL"]
db_name = os.environ["DB_NAME"]
db_username = os.environ["DB_USERNAME"]
db_password = os.environ["DB_PASSWORD"]
db_port = os.environ["DB_PORT"]

# init database instance
database.init(db_name, host=db_url,user=db_username, password=db_password, port=db_port)

class SignInHandler(BaseHandler):
    '''
    SignHandler class , responsible for login authentication
    '''
    def get(self):
        curr_user = self.get_current_user()
        if (curr_user is None):
            self.render('signin.html')
        else:
            self.redirect("/")

    def post(self):
        self.set_header('Content-Type','text/json')
        data = json_decode(self.request.body)
        username = data['_username']
        password = data['_password']
        # get record from database
        status ={}
        try:
            User.get(User.username == username, User.password == password)
            self.set_secure_cookie('_username_', username)
            status["status"]="ok"
            self.write(json_encode(status))
        except orm.DoesNotExist:
            status ["status"]="wrong"
            self.write(json_encode(status))
            
class InviteHandler(BaseHandler):
    def get(self):
        temp = self.request.uri[8:]
        try:
            inviter,email,role,project = checkTemp(temp)
            self.render("invite2.html",email=email,uri=temp)
        except Exception as e:
            print(e)
            raise tornado.web.HTTPError(400)
            
class InviteUserHandler(BaseHandler):
    def post(self):
        print("here")
        self.set_header('Content-Type','text/json')
        data= json_decode(self.request.body)  
        username = data["username"]
        uri = data["uri"]   
        temp = uri[uri.find("/invite/")+8:]
        

            
        message = {}
        message["status"] = "ok"
        message["uri"] = "ok"
        message["username"] = "ok"
        
        exists = User.select().where(User.username == username).limit(1).count()
        print("exists = :",exists)
        #TODO: add authentication
        #if(len(username)<6 or len(data["password"]<6) or exists != 0 or len(data["actualName"])<4):
        #   flag = True
        flag = False
        if(flag):
            message["status"] = "nope"
            message["username"] = "nope"
        else:
            try:
                inviter,email,role,project_id = checkTemp(temp)
                if(email==data["email"]):
                    message["status"] = "ok"
                    print('email match')
            #TODO:fix create user to handle phone and timezone
                    user = User.create(username=data["username"],password=data["password"],actual_name=data["actualName"],email=email)
                    proj_obj = Project.get(Project.id==project_id)
                    comp_name = proj_obj.company_id.name
    
                    ProjectMember.create(project_id=project_id,username=data["username"])
                    git.UserRepository(comp_name,proj_obj.name,user,create=True)
            except Exception as e:
                for i in sys.exc_info():
                    print(i)
                database.rollback()
                message["status"] = "nope"
                print("invalid link")
                print(e)
            
        message = json_encode(message)
        self.write(message)
 
class SignupHandler(BaseHandler):
    def get(self):
        print("here")
        self.render("signup.html")
        
    def post(self):
        self.set_header('Content-Type','text/json')
        data = json_decode(self.request.body)
        username = data['_username']
        password = data['_password']
        actualname = data['_actualName']
        email = data['_email']
        phone = data['_phone']
        timezone = data['_timezone']
        
        # validate timezone value
        if timezone not in pytz.all_timezones:
            timezone = "GMT"
        
        companyname = data['_companyName']
        
        try:
            # phone is optional so here we check if the user provided it.
            phone = int(phone)
        except:
            phone = 0
        
        # add record to database
        try:
            User.create(username=username, password=password,
                       actual_name=actualname, email=email, phone=phone,time_zone=timezone)

            Company.create(name=companyname, owner=username)
            message = {}
            message["status"] = "ok"
            message = json_encode(message)
            self.write(message)
        except Exception as e:
            database.rollback()
            self.write(json_encode({'status':'error'}))
            print(e)

class SignOutHandler(BaseHandler):
    
    @tornado.web.authenticated
    def get(self):
        self.set_header('Content-Type','text/json')
        self.clear_cookie('_username_')
        self.redirect("/")
