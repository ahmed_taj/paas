'''
Created on Apr 2, 2015

@author: One
'''
from DockerServer import DockerSocketClient
import tornado.web
import tornado.websocket
from tornado.escape import json_decode, json_encode,xhtml_escape
from api.db.model import *
import peewee as orm
import api.utils.general as utility
import api.utils.project as proj_util
import api.vcs.git2 as git
import pygit2
import os
import sys
import pytz
from datetime import datetime, timezone, timedelta,tzinfo
import functools # used to order repository tree
import re
from hashlib import sha256
from tornado.web import authenticated
from tornado import gen

# database env
db_url = os.environ["DB_URL"]
db_name = os.environ["DB_NAME"]
db_username = os.environ["DB_USERNAME"]
db_password = os.environ["DB_PASSWORD"]
db_port = os.environ["DB_PORT"]

repo_root = os.environ['REPO_ROOT'].replace('\\','/') 

# init database instance
database.init(db_name, host=db_url,user=db_username, password=db_password, port=db_port)

class BaseHandler(tornado.web.RequestHandler):
    '''
    This is the base class of all request handers. All request handlers should extend it.
    '''
    def get_current_user(self):
        cookie_user =  self.get_secure_cookie("_username_")
        try:
            usr = User.get(User.username == cookie_user)
            return usr
        except orm.DoesNotExist:
            return None
            
    def write_error(self,code, **kwargs):
        if code == 400:
            self.write("400: bad request ")
        elif code == 403:
            self.write("403: You shouldn't be here")
        elif code == 404:
            self.write("404: You are searching for ghost")
        else:
            self.write("N/A: something went wrong")
      

class WebSocketHandler(tornado.websocket.WebSocketHandler):
    '''
    This is the base class of all websocket handers. All websocket handlers should extend it.
    '''
    def get_current_user(self):
        cookie_user =  self.get_secure_cookie("_username_")
        try:
            usr = User.get(User.username == cookie_user)
            return usr
        except orm.DoesNotExist:
            return None
            
    def write_error(self,code, **kwargs):
        if code == 400:
            self.write("400: bad request ")
        elif code == 403:
            self.write("403: You shouldn't be here")
        elif code == 404:
            self.write("404: You are searching for ghost")
        else:
            self.write("N/A: something went wrong")
            
class MainHandler(BaseHandler):

    @tornado.web.authenticated
    def get(self):
        curr_user  = self.get_current_user()
    
        # get recent 3 accessed projects 

        projects = ProjectMember.select(ProjectMember.project_id,               \
                                        Project.name,Project.last_activity,     \
                                        Project.manager,                        \
                                        Project.company_id,                     \
                                        Company.name.alias("company_name"))     \
                                .join(Project)                                  \
                                .join(Company)                                  \
                                .where(ProjectMember.username == curr_user.username)\
                                .order_by(ProjectMember.last_access.desc())     \
                                .naive()                                        \
                                .limit(3)

        if(projects.count()<1):
        	projects.name=""
        	projects.company_id=""
        	projects.manager=""
        	projects.description=""

        self.render("index.html",user_name=curr_user.username,projects = projects )
        

################################################################################
#                              Project                                         #
################################################################################

class ProjectHandler(BaseHandler): 

    @tornado.web.authenticated
    def get(self,company,project):
        try:
            # get current user
            cur_user = self.get_current_user()

            # check for existence of project
            target_project = proj_util.get_project(company,project)

            # if project not found
            if(target_project is None):
                raise tornado.web.HTTPError(404)

            # check for user as a member
            project_member = proj_util.get_member(target_project.id, cur_user.username)
            # if not
            if(project_member is None):
                raise tornado.web.HTTPError(403)


            sprint_list = Sprint.select().where(Sprint.project_id==target_project.id)\
                            .order_by(Sprint.is_active.desc())
            curr_date = utility.current_time()
            
            # quick fix after deleteing GroupMember table
            event_list = []

            active_sprint_id = None
            userData = ProjectMember.select(ProjectMember.username)\
                        .where((ProjectMember.project_id == target_project.id))

            for s in sprint_list:
                if s.is_active == True:
                    active_sprint_id = s.id
                    break
            # stupid vars
            comp_id = target_project.company_id
            comp_name = target_project.company_id.name
            proj_name = target_project.name
            proj_id = target_project.id
            user_repo = git.UserRepository(comp_name,target_project.name,cur_user)
            
            #FIXME: integrate tree.js 
            project_files = list(set(os.listdir(user_repo.url)) - set(['.git']))

            # update last access date
            project_member.last_access = utility.current_time()
            project_member.save()

            self.render("projects/index.html",user_name=cur_user.username, 
                        sprint_list = sprint_list,company_id = comp_id,company_name=comp_name,
                        active_sprint_id=active_sprint_id,project_files=project_files,
                        userData=userData,project_name=proj_name,project_id=proj_id)
        except Exception as e:
            database.rollback()
            print("exception from ProjectHandler")
            print(e)

class CreateProject(BaseHandler):

    @tornado.web.authenticated
    def post(self):
        self.set_header('Content-Type','text/json')
        try:
            print("company>>>>>>>>>>>>>>>>>>>>>");
            cur_user = self.get_current_user()
            data = json_decode(self.request.body)
            name= data['_name']
            company_id =  data['_company_id']
            description = data['_description']
            print(name)

            #
            # security issue: we need to check for the current user and see if he
            #                 has the permission to create project, also we have
            #                 to check if there is a company that its id matchs the 
            #                 company_id field
            #company = Company.get(Company.name == company_id)
            id = "1";
            new_project = Project.create(name=name,company_id=id ,manager=cur_user.username, \
                                        description=description)
            ProjectMember.create(project_id=new_project.id,username=cur_user.username,role="admin")


            # repository creation
            main_repo = git.BaseRepository(new_project.company_id.name,new_project.name,create=True)
            if(not main_repo.created):
                # do something
                #TODO: do something like killing hammad
                print("not created")
            print("after main repo")
            # make a clone for this user
            user_clone = git.UserRepository(new_project.company_id.name,new_project.name,cur_user,create=True)
            if(not user_clone.created):
                print("main repo is not cloned to ",curr_user.username)
            print("after user repo")    
            ChatGroup.create(project_id=new_project.id)
            status= {}
            if(new_project is not None):
                status['status'] = 'ok'
                status['id'] = new_project.id
                status['company_name'] = new_project.company_id.name
                status['last_activity'] = new_project.last_activity

                self.write(json_encode(status))
            else:
                status['status'] = 'error'
                self.write(json_encode(status))
        except Exception as e:
            database.rollback()
            status = {}
            status['status'] = 'error'
            self.write(json_encode(status))


class DeleteProject(BaseHandler):
    """
    @tornado.web.authenticated
    def post(self,company,project):
        try:
            cur_user = self.get_current_user()

            # check for existence of project
            target_project = proj_util.get_project(company,project)
            
            # if project not found
            if(target_project is None):
                raise tornado.web.HTTPError(404)

            # check for user authority 
            if(proj_util.is_admin(target_project.id,cur_user.username)):
                # delete project instance
                target_project.delete_instance()

            raise tornado.web.HTTPError(403)
        except Exception as e:
            database.rollback()
            print(e)
            raise tornado.web.HTTPError(403)
        
    """
    @tornado.web.authenticated
    def post(self):
        data = json_decode(self.request.body)
        status = {}
        try:
            cur_user = self.get_current_user()
            project = Project.get(Project.id == data["id"])
            if(proj_util.is_admin(project,cur_user.username)):
                status["name"]=project.name
                # delete project instance
                #TODO: FIX THIS DELETE IS NOT WORKING
                project.delete_instance()
                print("shhould've deleted project: ",project.id)
                status["status"] = "ok"  
                print(status["name"]) 
            else:
                status["status"]="error"
        
        except:
            status["status"] = "error"
            database.rollback()
            for i in sys.exc_info():
                print(i)
        
        self.write(json_encode(status))
class UpdateProjectHandler(BaseHandler):
    @tornado.web.authenticated
    def post(self):
        self.set_header('Content-Type','text/json')
        print("in update project handeler ")
        data = json_decode(self.request.body)
        response={}
        try:


            print(data["_name"])
            print(data["_company_id"])
            print(data["_manager"])
            print(data["_description"])
            print(data["_start_date"])
            print(data["_end_date"])

            record = Project.get(id = data["_id"])
            if(data["_name"] != ''):
                record.name=data["_name"]
            record.description=data["_description"]
            if(data["_start_date"] != ''):
                record.start_date=data["_start_date"]
            if(data["_end_date"] != ''):
                record.end_date=data["_end_date"]
            record.last_activity= current_time()
            record.save()


            print("project updated ")
            response["status"] = "ok"
            self.write(json_encode(response)) 
        except Exception as e:
            #TODO: change json to error message
            print("exception in update project handler",e)
            database.rollback()
            response["status"] = "error"
            self.write(json_encode(response)) 


class GetProjectData(BaseHandler):

    @tornado.web.authenticated
    def post(self):
        print("in Project Handler")
        self.set_header('Content-Type','text/json')
        data = json_decode(self.request.body)

        projectData = Project.select().where(Project.id == data["name"]).first()
        memberData = ProjectMember.select().where(ProjectMember.project_id == projectData.id)
        active_sprint_id = Sprint.select().where((projectData.id == Sprint.project_id) &( Sprint.is_active == True)).first()
        print(projectData)

        members ={}
        i = 0 
        for member in memberData :
            members[str(i)] = member.username.username
            i=i+1 
                    
        response={}
        response["name"] = projectData.name 
        response["manager"] = projectData.manager.username
        response["member"] =  members
        response["company"] = projectData.company_id.name
        response["description"]= projectData.description
        response["startDate"] = str(projectData.start_date)
        end_date = projectData.end_date

        if(end_date !=None):
            response["endDate"]=str(projectData.end_date) 
        else:
            response["endDate"]=''
        response["lastActivity"]= str(projectData.last_activity)
        if(active_sprint_id !=None):
            response["sprint_id"]= active_sprint_id.id
        else:
            response["sprint_id"]= None
        self.write(json_encode(response))	    	


        
###############################################################################
#                              Sprints                                        #
###############################################################################

class SprintChartHandler(BaseHandler):

    @tornado.web.authenticated
    def post(self):
        self.set_header('Content-Type','text/json')
        try:
            data = json_decode(self.request.body)
            #print("chart data = "+str(data["sprint_id"]))
            axisX , axisY = utility.calculateSprintGraph(data["sprint_id"])
            result = [axisX,axisY]
            #print(axisX)
            #print(axisY)
            self.write(json_encode(result))
        except Exception as e:
            print(e)

     
class CreateSprintWithName(BaseHandler):

    @tornado.web.authenticated
    def post(self):
        self.set_header('Content-Type','text/json')
        data = json_decode(self.request.body)
        name = data["name"]
        project = data["project_name"]
        company = data["company_name"]
        print(company)
        print(project)
        response={}
        try:
            company_id = Company.get(Company.name == company).id
            proj_id = Project.get((Project.company_id == company_id,)&
                                     (Project.name == project)).id
        
            new_sprint = Sprint.create(name=name,project_id=proj_id)
            
            # add default sprint columns
            SprintColumn.create(sprint_id= new_sprint.id,name="New",order=0)
            SprintColumn.create(sprint_id= new_sprint.id,name="Commited",order=1)
            SprintColumn.create(sprint_id= new_sprint.id,name="Approved",order=2)
            SprintColumn.create(sprint_id= new_sprint.id,name="Done",order=3)
            
            response["status"] = "ok"
            response["sprint_id"] = new_sprint.id
            self.write(json_encode(response))
        except Exception as e:
            database.rollback()
            print(e)
            response["status"] = "error"
            self.write(json_encode(response))  
        
class GetSprint(BaseHandler):

    @tornado.web.authenticated
    def post(self):
        try:
            self.set_header('Content-Type','text/json')
            data = json_decode(self.request.body)
            sprint_id = data['sprint_id']
            result = SprintColumn.select(SprintColumn.id.alias('col_id')
                                          ,SprintColumn.name.alias('col_name')
                                          ,Task.id.alias('task_id'),
                                          Task.name.alias('task_name'),
                                          Task.assign_to,
                                          Task.priority,
                                          Task.worked_hours,
                                          Task.estimated_hours).                   \
                                          where(SprintColumn.sprint_id==sprint_id) \
                                          .join(Task,orm.JOIN_LEFT_OUTER)          \
                                          .order_by(SprintColumn.order).naive() 
            json_list = []
            for r in result:
                temp = {}
                temp['col_id'] = r.col_id
                temp['task_id'] = r.task_id
                temp['col_name'] = r.col_name
                temp['task_name'] = r.task_name
                temp['assign_to'] = r.assign_to
                temp['estimated_hours'] = r.estimated_hours
                temp['worked_hours'] = r.worked_hours
                temp['priority'] =r.priority
                json_list.append(temp)
            self.write(json_encode(json_list))
        except Exception as e:
            print(e)
        
class DeleteSprintHandler (BaseHandler):
    @tornado.web.authenticated
    def get(self):
        try:
            sprintData = Sprint.select()
            self.render("delete-sprint.html", sprint_data=sprintData)
        except Exception as e:
            print(e)

    @tornado.web.authenticated
    def post(self):
        try:
            sprintName = self.get_argument('_sprintName')
            sprint = Sprint.get(Sprint.name == sprintName)
            sprint.delete_instance()
        except Exception as e:
            database.rollback()
            print(e)

class AddSprintColumnHandler(BaseHandler):
    @tornado.web.authenticated
    def post(self):
        self.set_header('Content-Type','text/json')
        try:
            data = json_decode(self.request.body)
            name = data["name"]
            order = data["order"]
            sprint_id = data["sprint_id"]

            columns = SprintColumn.select().where( (SprintColumn.order >= order)\
                                & (SprintColumn.sprint_id==data["sprint_id"]))\
                                .order_by(SprintColumn.order.desc())

            for column in columns:
                print(column.name)
                print(column.order)
                column.order = column.order+1
                print(column.order)
                column.save()

            SprintColumn.create(sprint_id=data["sprint_id"],name = data["name"], order = data["order"])

            print(name,order,sprint_id)
            print('yo')
            self.write(json_encode({"status":"ok"}))
        except Exception as e:
            print(e)
            database.rollback()

class GetColumnNumberHandler(BaseHandler):
    @tornado.web.authenticated
    def post(self):
        data = json_decode(self.request.body)
        columns = SprintColumn.select().where(SprintColumn.sprint_id == data["sprint_id"]).count()
        print("columns is:",columns)
        print("sprint id is :",data["sprint_id"])
        result = {"status" : "ok","number":columns}
        self.set_header('Content-Type','text/json')
        self.write(json_encode(result))
        
###############################################################################
#                              Tasks                                          #
###############################################################################

class UpdateTask(BaseHandler):

    @tornado.web.authenticated
    def post(self):
        data = json_decode(self.request.body)
        status={}
        self.set_header('Content-Type','text/json')
        try:
            record = Task.get(id = data["id"])
            record.status = data["draged_id"]
            record.save()
            status["status"]="ok"

            self.write( json_encode(status))
        except orm.DoesNotExist:
            database.rollback()
            status["status"] = "error"
            self.write( json_encode(status))
            
class UpdateTaskName (BaseHandler):

    @tornado.web.authenticated
    def post(self):
        data = json_decode(self.request.body)
        status ={}
        self.set_header('Content-Type','text/json')
        try :
            record = Task.get(id=data["id"])
            record.name = data["newValue"]
            record.save()
            self.write(json_encode(status))
        except Exception as e:
            database.rollback()
            status['status'] = 'error'
            self.write(json_encode(status)) 

        
class AddTask(BaseHandler):

    @tornado.web.authenticated
    def post(self):
        data = json_decode(self.request.body)
        status ={}
        self.set_header('Content-Type','text/json')
        try:
            print(data["name"])
            print(data["sprint_id"])
            col = SprintColumn.select(SprintColumn.id).where((SprintColumn.sprint_id==data["sprint_id"]) & (SprintColumn.order=="0" ) ).limit(1)

            new_task = Task.create(name=data["name"], status=col[0].id,
                                   sprint_id=data["sprint_id"],order="2")
            
            status["status"]= "ok"
            status["id"] = new_task.id
            print("stat returned : "+status["status"])
            self.write(json_encode(status))
        except Exception as e:
            database.rollback()
            print(e)
            status["status"]= "error"
            self.write(json_encode(status))

class GetTaskData(BaseHandler):

    @tornado.web.authenticated
    def post(self,company , project ):
        data = json_decode(self.request.body)
        print("in get task data ")
        print("from client is "+data["id"])
        status ={}
        self.set_header('Content-Type','text/json')
        
        try:
            cur_user = self.get_current_user()

            # check for existence of project
            target_project = proj_util.get_project(company,project)

            # if project not found
            if(target_project is None):
                raise tornado.web.HTTPError(404)

            taskData = Task.get(Task.id == data["id"])
            userData = ProjectMember.select(ProjectMember.username)\
                        .where((ProjectMember.project_id == target_project.id))
            sprintData = Sprint.select().where(Sprint.project_id==target_project.id)\
                            .order_by(Sprint.is_active.desc())
            columns = SprintColumn.select(SprintColumn.id,SprintColumn.name)\
                        .where(SprintColumn.sprint_id == taskData.sprint_id)

            sprint_list =[]
            for sprint in sprintData : 
                temp={}
                temp["name"] = sprint.name
                sprint_list.append(temp)
            status["sprint"] = sprint_list 
            status["estimated_hours"] = taskData.estimated_hours
            status["worked_hours"]=taskData.worked_hours
            user_list=[]
            for user in userData : 
                temp ={}
                temp["name"]=user.username.username
                print("in user loop ")
                print(temp["name"])
                user_list.append(temp)

            status["users"]= user_list
            col_list = []
            for col in columns:
                temp = {}
                temp["id"] = col.id
                temp["name"] = col.name
                print("columns names : "+temp["name"])
                col_list.append(temp)

            status["status"] = "ok"
            status["name"]= taskData.name
            if(taskData.assign_to!=None):
                status["assignTo"] =taskData.assign_to.username
            else:
                status["assignTo"] =taskData.assign_to
            status["task_status"] = taskData.status.name
            status["columns"] = col_list
        except orm.DoesNotExist:
            status["status"] = "error"
        print("i hate this "+status["status"])
        print(status)
        self.write(json_encode(status))

class AddTaskObject(BaseHandler):

    @tornado.web.authenticated
    def post(self):
        print ("in server from save")
        data = json_decode(self.request.body)
        print(data)
        
        status ={}
        self.set_header('Content-Type','text/json')
        try :
            record = Task.get(Task.id == data["id"])
            record.name = data["name"]
            record.assign_to = data["assign"]
            record.status = data["status"]
            record.estimated_hours =data["estimated"]
            record.worked_hours = data["worked"]
            #record.order = 2
            record.save()
            status["status"] = "ok"
        except Exception as e:
            database.rollback()
            self.write('task not found') 
            status["status"]="error"
   
        self.write(json_encode(status))
        
class DeleteTask(BaseHandler):
    #TODO:COMPLETE THIS THIS IS INCOMPLETE
    @tornado.web.authenticated
    def post(self):
        id = json_decode(self.request.body)['id'][6:]
        data = {}
        try:
            
            task = Task.get(Task.id == id)
            data["name"]=task.name
            task.delete_instance()
            data["status"]='ok'
        except Exception as e:
            database.rollback()
            data["status"]='error'
            print(e)
        
        print(id)
        
        
        self.write(json_encode(data))
###############################################################################
#                              Events                                         #
###############################################################################

class AddEvent(BaseHandler):
    @tornado.web.authenticated
    def get(self,company,project):
        self.write("It's Event handler\nYou Shouldn't be here.")
    
    @tornado.web.authenticated
    def post(self,company,project):  
        data = None
        try:
            data = json_decode(self.request.body)
        except Exception as e:
            raise tornado.web.HTTPError(400) # bad request
        
        ev = {} 
        # user timeonze
        utz = pytz.timezone(self.current_user.time_zone)
        try:
            ev['title'] = data["title"]
            ev['type'] = data["type"]
            ev['desc'] = data["desc"]

            """
            the expected format for datetimes is:
                "%Y-%m-%dT%H:%M:%S" + timezone

            because we don't care about timezone we have to ignore it
            timezone is on this format:
                [+|-](0-9){2}:(0-9){2} // 6 characters
            so, we always eliminate the last 6 characters, then treat
            the reset as user default timezone.

            """
            stwtz = data["start_date"][:-6]
            etwtz = data["end_date"][:-6]

            sdt = datetime.datetime.strptime(stwtz,"%Y-%m-%dT%H:%M:%S")
            edt = datetime.datetime.strptime(etwtz,"%Y-%m-%dT%H:%M:%S")

            ev['start_date']=   utz.localize(sdt).isoformat()

            ev['end_date'] =    utz.localize(edt).isoformat()
            
            try:
                cid = Company.get(Company.name == company).id
                ev['project_id'] = Project.get(Project.name == project,
                                         Project.company_id == cid).id
            except orm.DoesNotExist:
                # project not found
                raise tornado.web.HTTPError(404)
                
        except :
            """ KeyError or Invalid datetime format """
            raise tornado.web.HTTPError(400)
            
        status = {}
        self.set_header('Content-Type','text/json')
        try:
            n = Event.create(author_id=self.current_user,title=ev['title'],
                         description=ev['desc'],type=ev['type'],
                         project_id=ev['project_id'],start_date=ev['start_date'],
                         end_date=ev['end_date'])
            """
            WARN:
                peewee doesn't return event's dates as datetime python class in variable
                (n), I don't know why
                To fix this I reselect the new event from datebase in variable (n_ev)
            
            """
            n_ev = Event.get(Event.id == n.id);
            
            """
            TODO: add scedular and notification code here
            """
            status["status"] = "ok"

            # return new event data
            status["event"] = {}
            status["event"]["id"] = n_ev.id
            status["event"]["title"] = n_ev.title
            status["event"]["type"] = n_ev.get_type_name()
            status["event"]["type_id"] = n_ev.type
            status["event"]["desc"] = n_ev.description
            status["event"]["author"] = n_ev.author_id.username
            status["event"]["creation_date"] = utz.normalize(n_ev.creation_date.astimezone(utz))\
                                                .isoformat()
            status["event"]["start"] = utz.normalize(n_ev.start_date.astimezone(utz))\
                                        .isoformat()
            status["event"]["end"] = utz.normalize(n_ev.end_date.astimezone(utz))\
                                        .isoformat()
        except Exception as e:
            database.rollback()
            print(e)
            status["status"] = "error"
            status["message"] = "Oops, something wen't wrong, event not added."
        self.write(json_encode(status))
        
class UpdateEvent(BaseHandler):

    @tornado.web.authenticated
    def post(self,company,project):
        #
        # Company, and project fields maybe used in future.
        #                               - Ahmed Taj elsir -
        data = None
        try:
            data = json_decode(self.request.body)
        except:
            raise tornado.web.HTTPError(400) # bad request

        status = {}
        self.set_header('Content-Type','text/json')
        
        try:
            event = Event.get(Event.id == data['id'])

            # timeonzes convertion
            utz = pytz.timezone(self.current_user.time_zone)      
            stwtz = data["start_date"]
            etwtz = data["end_date"]

            sdt = datetime.datetime.strptime(stwtz,"%Y-%m-%dT%H:%M:%S")
            edt = datetime.datetime.strptime(etwtz,"%Y-%m-%dT%H:%M:%S")

            # 1--
            event.start_date = utz.localize(sdt).isoformat()
            # 2--
            event.end_date = utz.localize(edt).isoformat()
            event.save()
            
            # return the updated details
            status["status"] = "ok"
            
        except:
            database.rollback()
            status["status"] = "error"
            status["message"] = "Something went wrong"
        self.write(json_encode(status))
        
class UpdateFullEvent(BaseHandler):

    @tornado.web.authenticated
    def post(self,company,project):
        #
        # Company, and project fields maybe used in future.
        #                               - Ahmed Taj elsir -
        data = None
        try:
            data = json_decode(self.request.body)
        except:
            raise tornado.web.HTTPError(400) # bad request

        status = {}
        self.set_header('Content-Type','text/json')
        
        try:
            event = Event.get(Event.id == data['id'])

            # 1--
            event.title = data['title']

            # 2--
            event.description = data['desc']

            # timeonzes convertion
            utz = pytz.timezone(self.current_user.time_zone)      
            stwtz = data["start_date"][:-6]
            etwtz = data["end_date"][:-6]

            sdt = datetime.datetime.strptime(stwtz,"%Y-%m-%dT%H:%M:%S")
            edt = datetime.datetime.strptime(etwtz,"%Y-%m-%dT%H:%M:%S")

            # 3--
            event.start_date = utz.localize(sdt).isoformat()
            # 4--
            event.end_date = utz.localize(edt).isoformat()

            event.save()

            # return the updated details
            status["status"] = "ok"
            status["event"] = {}

            status["event"]["title"] = event.title
            status["event"]["desc"] = event.description
            status["event"]["start_date"] = event.start_date
            status["event"]["end_date"] = event.end_date
            
        except:
            database.rollback()
            status["status"] = "error"
            status["message"] = "Something went wrong"
        self.write(json_encode(status))
            
class EventsJSONFeed(BaseHandler):
    """
    This handler is provided as the need for JSON formated event's list
    to be used with fullcalendar.js
    
    @author: ahmed_taj
    """
    @tornado.web.authenticated
    def get(self,company,project):
    
        # check for existence of project
        target_project = proj_util.get_project(company,project)

        # if project not found
        if(target_project is None):
            raise tornado.web.HTTPError(404)

        # check for user as a member
        project_member = proj_util.get_member(target_project.id, 
                                              self.current_user.username)
        # if not
        if(project_member is None):
            raise tornado.web.HTTPError(403)
        
        start = self.get_argument('start',"1970-01-1T00:00:00")
        end = self.get_argument('end',"9999-12-31T23:59:59")
        
        events_list = []

        query_result = Event.select().where((Event.start_date >= start) &\
                                            (Event.start_date <= end))
        # user timeonze
        utz = pytz.timezone(self.current_user.time_zone)
        
        for ev in query_result:
            temp = {}
            temp["id"] = ev.id
            temp["title"] = ev.title
            temp["type"] = ev.get_type_name()
            temp["type_id"] = ev.type;
            temp["description"] = ev.description
            temp["author"] = ev.author_id.username
            temp["creation_date"] = utz.normalize(ev.creation_date.astimezone(utz))\
                                    .isoformat()
            temp["start"] = utz.normalize(ev.start_date.astimezone(utz))\
                            .isoformat()
            temp["end"] = utz.normalize(ev.end_date.astimezone(utz))\
                            .isoformat()
            events_list.append(temp)
            
        self.write(json_encode(events_list))
  

class RemoveEvent(BaseHandler):
    """
    This handler is used to delete event
    
    @author: ahmed_taj
    """
    @tornado.web.authenticated
    def post(self,company,project):
        status = {}
        self.set_header('Content-Type','text/json')
        
        try:
            event_id = json_decode(self.request.body)['id']
            
            event = Event.get(Event.id == event_id).delete_instance()
            status['status'] = 'ok'
        except:
            status['status'] = 'error'
            status['message'] = 'Something went wrong'
        self.write(json_encode(status))
        
        
###############################################################################
#                           Users                                             #
###############################################################################
 
class UserSearch(BaseHandler):
 
    @tornado.web.authenticated
    def post(self):
        print(self.request)
        project_id = json_decode(self.request.body)["id"]
        users = utility.inviteSearch("", project_id)
        emails = []
        for i in users:
            emails.append(i["email"])
        
        self.write(json_encode(emails))
        '''
        print("ayman in user search  >>>> ")
        data= json_decode(self.request.body)
        #current_user = self.get_current_user().decode()
        print("the requsted data are : ")
        print("requsted name or mail : "+data["text"])
        print("requsted project id  : "+data["project_id"])
        result = utility.inviteSearch(data["text"],data["project_id"])
        print("hammad method called and return result ")
        status ={}
        self.set_header('Content-Type','text/json')
        #print("the result is  >>>>> : "+result[0][name])

        for member in result:
            print("in result >>>>>>")
            print(member)
            print(member["name"])
            print(member["email"])
        #if(len(result)== 0):
        #    status["return"] = "erorr"
        #else : 
        status["return"] = result 
        self.write(json_encode(status))
        '''
class UserCompanies(BaseHandler):
    def post(self):
        self.set_header('Content-Type','text/json')
        print("user companies")
        user = self.get_current_user()
        print(user)
        member = CompanyMember.select(CompanyMember.company_id).where(CompanyMember.username==user.username)
        companies = []
        for company in member:
            companies.append(company.company_id.name)
        print("ended")
        print(companies)
        self.write(json_encode(companies))

class UserInvite(BaseHandler):
    @tornado.web.authenticated
    def post(self):
        user = self.current_user
        data = json_decode(self.request.body)
        print(data)
        project_id = data["project_id"]
        reply = {}
        reply["status"]="error"
        try:
            project = Project.get(Project.id==project_id)
            if(proj_util.is_admin(project,user.username)):
                mylist = []
                mylist.append(data["user"])
                print(mylist)
                proj_util.invite_users_to_project(mylist, data["project_id"])
                reply["status"]= "success"
                reply["name"]=data["user"]
            else:
                print("User is not admin")
        except:
            database.rollback()
            for i in sys.exc_info():
                print(i)
        
        self.write(json_encode(reply))
###############################################################################
#                           Notification                                      #
###############################################################################

class NotificationHandler(WebSocketHandler):
 
    listeners = []
    
    def open(self, *args):
        print("connected")
        cur_user = self.get_current_user()
        print(cur_user.username)
        self.listeners.append(self)
 
    @tornado.web.authenticated
    def on_message(self, data):
        data = json_decode(data)
        print("message received")
        message = data["message"]
        
        # quick fix after deleting GroupMember table
        group_members = [] #GroupMember.select(GroupMember.username).where(GroupMember.group_id == data["group"])
        cur_user = self.get_current_user()
        print(cur_user)
        
        for g_member in group_members:
            for listener in self.listeners:
                member = listener.get_current_user()
                if(member.username != cur_user.username):
                    if(g_member.username.username == member.username):
                        listener.write_message(message)
                        break
        #for listener in self.listeners:
        #    member = listener.get_current_user().decode()
        #    if(member != curr_user):
        #        print("in if")
        #        for g_member in group_members:
        #            print(g_member.username.username)
        #            if(g_member.username.username == member):
        #                listener.write_message(message)
        #                break
        
    def on_close(self):
        self.listeners.remove(self)
        
###############################################################################
#                                 IDE                                         #
###############################################################################

class IDEHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self,company,project,user):
        print(self.current_user)
        print(user)
        if self.current_user.username != user:
            raise tornado.web.HTTPError(403)
<<<<<<< HEAD
        self.render("editor.html")
=======
            
        self.render("editor.html",company=company,project=project,user=self.current_user.username)
>>>>>>> 4944f7538bccd5cd0ae3a9311ba2623b750e794c
     
class RawFileReader(BaseHandler):
    
    @tornado.web.authenticated
    def get(self,company,project,user,path):
        #TODO: support all file types
        #TODO: accesss management
        os_path = repo_root+'/users/'+user+'/'+company+'/'+project+'/'+path
        print(os_path)
        status = {}
        if(os.path.isfile(os_path)):
            # get file extension
            _ , file_ext = os.path.splitext(path)

            # set content type , charset!!!
            content_type = utility.get_content_type(file_ext)
            self.set_header('Content-Type',content_type)
            content = open(os_path).read()

            self.write(json_encode(content))
        else:
            raise tornado.web.HTTPError(404)

class RawFileWriter(BaseHandler):
    
    @tornado.web.authenticated
    def post(self,company,project,user,path):
        #TODO: support all file types
        #TODO: accesss management
<<<<<<< HEAD
        os_path = repo_root+'/users/'+user+'/'+company+'/'+project+'/'+path
        print(self.request.body)
        data = json_decode(self.request.body)
        content = data['content']
        if(os.path.isfile(os_path)):
            f = open(os_path,'w+')
            f.write(content)
        else:
            raise tornado.web.HTTPError(404)
        self.finish()
=======
        os_path = repo_root+'/users/'+self.current_user.username+'/'+company+'/'+project+'/'+path
        try:
            content = json_decode(self.request.body)['content']
            if(os.path.isfile(os_path)):
                f = open(os_path,'w+')
                f.write(content)
                self.write(json_encode({'status':'ok'}));
            else:
                self.write(json_encode({'status':'error'}));
        except:
            raise tornado.web.HTTPError(500)
>>>>>>> 4944f7538bccd5cd0ae3a9311ba2623b750e794c

'''
        Repository Handlers
'''
class CommitsLog(BaseHandler):
    @tornado.web.authenticated
    def get(self,company,project):
        # check for existence of project
        target_project = proj_util.get_project(company,project)

        # if project not found
        if(target_project is None):
            raise tornado.web.HTTPError(404)

        # check for user as a member
        project_member = proj_util.get_member(target_project.id, self.current_user.username)
        # if not
        if(project_member is None):
            raise tornado.web.HTTPError(403)
        try:
            repo = git.BaseRepository(company,project)

            head = None

            if not repo.is_empty:
                head = repo["c6eea83a54d1061b7331a42b9c304b77437978db"].id#repo[repo.head.target].id

            data = []
            if head is not None:
                for c in repo.walk(head,pygit2.GIT_SORT_TIME):
                    tmp = {}
                    tmp['author']  = c.author.name
                    tmp['commit']  = c.hex
                    tmp['message'] = xhtml_escape(c.message)

                    tzinf  = timezone( timedelta(minutes=c.author.offset) )
                    tmp['date']= datetime.fromtimestamp(float(c.author.time), tzinf)\
                                .isoformat()

                    data.append(tmp)

            res = {"data":data}
            self.write(json_encode(res))
        except:
            raise tornado.web.HTTPError(500)

class SourceTree(BaseHandler):
    @tornado.web.authenticated
    def get(self,company,project,branch=None,dir=None):
        # ingored for performance reasons in debug mode.
        '''
        # check for existence of project
        target_project = proj_util.get_project(company,project)

        # if project not found
        if(target_project is None):
            raise tornado.web.HTTPError(404)

        # check for user as a member
        project_member = proj_util.get_member(target_project.id, self.current_user.username)
        # if not
        if(project_member is None):
            raise tornado.web.HTTPError(403)
        '''
        #try:
        repo = git.BaseRepository(company,project)

        last = None

        tree = []
        if not repo.is_empty:
            bran = 'master' # default

            if branch is not None:
                if branch in repo.listall_branches():
                    bran = branch

            root = bran+"/"

            ref = repo.get_branch_ref(bran)
            if ref is not None:
                src_list = ref.get_object().tree

                if dir is not None:
                    try:
                        src_list = repo.get_tree(src_list[dir])
                        root += dir
                        if not root.endswith('/'):
                            root += '/'

                        # dummy link to previous folder
                        tree.append({
                            'name': '..',
                            'url' : 'tree/'+root+'..',
                            'size': ' ',
                            'type': '..'
                        })
                    except:
                        pass

                for e in src_list:
                    if e.type == "tree":
                        tree.append({'name':e.name,
                                     'size':' ',
                                     'url':'tree/'+root+e.name,
                                     'type':'tree'})
                    else:
                        s = str(round(repo.get_blob(e).size / 1024,2))+' KB'
                        tree.append({'name':e.name,
                                     'size':s,
                                     'url':'blob/'+root+e.name,
                                     'type':'blob'})


        self.write(json_encode({'data':tree}))  
        #except:
        #    raise tornado.web.HTTPError(500)

class BlobViewer(BaseHandler):
    @tornado.web.authenticated
    def get(self,company,project,branch,dir):
        # ingored for performance reasons in debug mode.
        '''
        # check for existence of project
        target_project = proj_util.get_project(company,project)

        # if project not found
        if(target_project is None):
            raise tornado.web.HTTPError(404)

        # check for user as a member
        project_member = proj_util.get_member(target_project.id, self.current_user.username)
        # if not
        if(project_member is None):
            raise tornado.web.HTTPError(403)
        '''
        repo = git.BaseRepository(company,project)
        
        if not repo.is_empty:
            ref = repo.get_branch_ref(branch)
            try:
                tree = ref.get_object().tree
                entry = tree[dir]
                if entry.type != 'blob':
                    raise tornado.web.HTTPError(404)
                blob = repo.get_blob(entry)
                res = {'name':entry.name,
                       'content':xhtml_escape(blob.data)}
                self.write(json_encode(res))
            except:
                raise tornado.web.HTTPError(404)
        else:
           raise tornado.web.HTTPError(404) 

class BranchLog(BaseHandler):
    @tornado.web.authenticated
    def get(self,company,project):
        # ingored for performance reasons in debug mode.
        '''
        # check for existence of project
        target_project = proj_util.get_project(company,project)

        # if project not found
        if(target_project is None):
            raise tornado.web.HTTPError(404)

        # check for user as a member
        project_member = proj_util.get_member(target_project.id, self.current_user.username)
        # if not
        if(project_member is None):
            raise tornado.web.HTTPError(403)
        '''
        try:
            repo = git.BaseRepository(company,project)
            res = []
            for b in repo.listall_branches():
                ref = repo.get_branch_ref(b).get_object().hex
                n = 0
                for g in repo.walk(ref,0):
                    n += 1
                res.append({'name':b,'number_of_commits':n})
            self.write(json_encode({'data':res}))
        except:
           raise tornado.web.HTTPError(404) 


class TagsLog(BaseHandler):
    @tornado.web.authenticated
    def get(self,company,project):
        # ingored for performance reasons in debug mode.
        '''
        # check for existence of project
        target_project = proj_util.get_project(company,project)

        # if project not found
        if(target_project is None):
            raise tornado.web.HTTPError(404)

        # check for user as a member
        project_member = proj_util.get_member(target_project.id, self.current_user.username)
        # if not
        if(project_member is None):
            raise tornado.web.HTTPError(403)
        '''
        try:
            repo = git.BaseRepository(company,project)
            res = []
            refs = repo.listall_references()
            regex = re.compile('^refs/tags')
            for t in list(filter(lambda r: regex.match(r), refs)):
                res.append({'name':t})
            self.write(json_encode({'data':res}))
        except:
           raise tornado.web.HTTPError(404) 
        
class CommitsDiff(BaseHandler):
    @tornado.web.authenticated
    def get(self,company,project,commit_id):
        # ingored for performance reasons in debug mode.
        '''
        # check for existence of project
        target_project = proj_util.get_project(company,project)

        # if project not found
        if(target_project is None):
            raise tornado.web.HTTPError(404)

        # check for user as a member
        project_member = proj_util.get_member(target_project.id, self.current_user.username)
        # if not
        if(project_member is None):
            raise tornado.web.HTTPError(403)
        '''
        try:
            repo = git.BaseRepository(company,project)
            commit = repo[commit_id]
            try:
                diff = commit.parents[0].tree.diff_to_tree(commit.tree)
                self.write(diff.patch)
            except:
                diff = commit.tree.diff_to_tree()
                self.write(diff.patch)
        except:
            raise tornado.web.HTTPError(404)
        

class UserRepositoryTree(BaseHandler):
    @tornado.web.authenticated
    def get(self,company,project):
        # check for existence of project
        pro = proj_util.get_project(company,project)

        # if project not found
        if(pro is None):
            raise tornado.web.HTTPError(404)

        # check for user as a member
        pro_m = proj_util.get_member(pro.id, self.current_user.username)
        # if not
        if(pro_m is None):
            raise tornado.web.HTTPError(403)
        repo = git.UserRepository(company,project,self.current_user)
        tree = utility.getTree(repo.url)
        
        self.write(json_encode(tree))
        

class VMHandler(BaseHandler):
    
    @gen.coroutine        
    def make_request (self,name,project,company):
            data = DockerSocketClient.send_data(name,project,company,"start")
            try:
                print("here we are with our data")
                print(data)
                
                reply = {}
                reply["status"]="ok"
                reply["port"]=data[0].decode()
                reply["username"]=data[1].decode()
                reply["password"]=data[2].decode()
                reply["port2"]=data[3].decode()
                self.write(json_encode(reply))
            except:
                print("Exception Caught")
                for i in sys.exc_info():
                    print(i)
    
    @tornado.web.authenticated
    def post(self,company,project):
        
        name  = self.get_current_user().username

        print(name)
        print(company)
        print(project)

        self.make_request(name, project, company)

###############################################################################
#                                 Backlog                                     #
###############################################################################

class GetBacklog(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        try:
            self.set_header('Content-Type','text/json')
            print("in get backlog handeler")
            data =self.request.body
            result = Backlog.select(Backlog.id.alias('backlog_id')
                                          ,Backlog.name.alias('backlog_name'),
                                          Backlog.priority,
                                          Backlog.description,
                                          Backlog.diffuclty,
                                          Backlog.estimated_duaration).                   \
                                          order_by(Backlog.priority).naive() 
            json_list = []
            for r in result:
                temp = {}
                temp["id"] = r.backlog_id 
                temp["name"] = r.backlog_name
                temp["duaration"] = r.estimated_duaration
                temp["description"] = r.description
                temp["priority"] = r.priority
                temp["diffuclty"] = r.diffuclty
                                
                json_list.append(temp)
                
            mydictionary = {"data":json_list}
            print(mydictionary)
            self.write(json_encode(mydictionary))

        except Exception as e:
        	print("Exception in get backlog")
        	print(e)

class AddBacklog(BaseHandler):
    @tornado.web.authenticated
    def post(self , company , project):
        try:
            curr_user  = self.get_current_user()
            print("in add backlog handler")
            self.set_header('Content-Type','text/json')
            data = json_decode(self.request.body)
            print(data)
            name = data["name"]
            duaration = data["duaration"]
            description = data["description"]
            diffuclty = data["diffuclty"]
            if(duaration == ""):
                duaration = 0 
            if(description == ""):
                description = "" 
            if(diffuclty == ""):
                diffuclty = 5 
            sele = Backlog.select().count()
            if(sele<0) : 
            	priority = 1 
            else : 
            	priority = sele+1 
            print(priority)
            print("above is priority")
            temp = {}
            # check for existence of project
            target_project = proj_util.get_project(company,project)
            
            # if project not found
            if(target_project is None):
                raise tornado.web.HTTPError(404)

            # check for user authority 
            print("target project ")
            print(target_project.id)
            print("cruuent user ")
            print(curr_user.username)
            if(proj_util.is_admin(target_project,curr_user.username)):

                """
                # check for existence of project
                target_project = proj_util.get_project(company,project)
                
                # if project not found
                if(target_project is None):
                    raise tornado.web.HTTPError(404)

                # check for user authority 
                if(proj_util.is_admin(target_project.id,cur_user.username)):
                    do sonething
                """
                Backlog.create(name=name , priority=priority,\
                                          description=description,\
                                          diffuclty=diffuclty,\
                                          estimated_duaration=duaration)
                print("added secuss ")	            
            else:
            	raise tornado.web.HTTPError(403)

        except Exception as e:
            print("some exception in add backlog ")
            temp ={}
            temp["return"] = "exception"
            self.write(json_encode(temp))
            print(e)

class UpdateSorting(BaseHandler):
    @tornado.web.authenticated
    def post(self):
        try:
        
            print("in update sorting handler ")
            data = json_decode(self.request.body)
            print(data)
            
            try:
                old_priority = Backlog.get(Backlog.id==data["id"]).priority
                print("the old priority is : ")
                print(old_priority)

                if(old_priority<int(data["priority"])):
                    print("in if")
                    query = Backlog.update(priority = Backlog.priority-1).where((Backlog.priority <= data["priority"]) & (Backlog.priority > old_priority));
                    query.execute()

                else:
                    print("in else")
                    query = Backlog.update(priority = Backlog.priority+1).where((Backlog.priority >= data["priority"]) & (Backlog.priority < old_priority));
                    query.execute()

                query = Backlog.update(priority = data["priority"]).where(Backlog.id == data["id"])
                query.execute()

                self.write(json_encode("success"))
                print("done")
            except:
                for i in sys.exec_info():
                    print(i)
                database.rollback()
                self.write(json_encode("failed"))
                print("not done")
        except Exception as e:
            print("some exception in update sorting ")
            temp ={}
            temp["return"] = "exception"
            self.write(json_encode(temp))
            print(e)

class UpdateTable (BaseHandler):
    @tornado.web.authenticated
    def post(self):
        try : 
            print("in update table ")
            data = json_decode(self.request.body)
            print(data)

            query = Backlog.update(name = data["name"] , priority = data["priority"] , description = data["description"] , diffuclty = data["diffuclty"] , estimated_duaration = data["duaration"]).where(Backlog.id == data["id"])
            query.execute()

            self.write(json_encode("success"))
        except Exception as e:
            print ("there is an exception on update table ")
            self.write(json_encode("failed"))

# TODO : the product owner only have responciple to delete the backlog
class DeleteBacklog(BaseHandler): 
    @tornado.web.authenticated
    def post(self):
        try : 
            print("in delete backlog ")
            data = json_decode(self.request.body)
            print("the data is : ")
            print(data)
            id = int(data["id"])
            backlog = Backlog.get(Backlog.id == id)
            backlog.delete_instance()
            self.write(json_encode("seccuss"))
        except Exception as e :
            print("there is an exception on delete backlog ") 
            database.rollback()
            print(e)

            
            
########################################################################
#       Meetings
########################################################################

global_rooms = {}

class Room(object):
    def __init__(self,meeting):
        self.name       = meeting.title
        self.meeting_id = meeting.id
        self.admin      = meeting.author_id
        self.project_id = meeting.project_id
        self.clients    = {}
        self.id = self.__keygen(meeting.start_date)

    def __keygen(self,date):
        #txt = str(self.name)+str(self.project_id)+str(date)
        # do we should care about encoding in production mode?
        #return sha256(txt.encode('utf-8')).hexdigest()
        return self.meeting_id
        
    def add_user(self,user):      
        # if user not in project return False
        self.clients[user.current_user.username] = user
        return True

    def remove_user(self,user):
        if user.current_user.username in self.clients:
            self.clients.pop(user.current_user.username)

    def __repr__(self):
        return self.name
    
class StartMeetingHandler(BaseHandler):

    @tornado.web.authenticated
    def get(self,company,project):
        global global_rooms
        
        id = self.get_argument('id',False)
        
        #if no meeting id supplied
        if not id:
            raise tornado.web.HTTPError(400)
        
        meeting = None
        #try:
        meeting = Event.get(id=id)
        room = Room(meeting)

        if room.id in global_rooms:
            print('Meeting Already started!')
        global_rooms[str(room.id)] = room

        self.redirect('/'+company+"/"+project+"/meeting/"+str(room.id))
        
        if meeting.author_id != self.current_user.username:
            raise tornado.web.HTTPError(403)
        #except:
        #    raise tornado.web.HTTPError(404)


class MeetingHandler(BaseHandler):

    @tornado.web.authenticated
    def get(self,company,project,meeting_id):
        global global_rooms
        
        if meeting_id not in global_rooms:
            raise tornado.web.HTTPError(404)
        self.render('meeting.html')

class MeetingWsHandler(WebSocketHandler):
    """
    When websocket connection opend
    """
    @tornado.web.authenticated
    def open(self, company,project,meeting_id):
        global global_rooms
        self.is_available = True

        if meeting_id in global_rooms:
            self.room = global_rooms[meeting_id]
        else:
            raise tornado.web.HTTPError(404)

    """
    When a message received
    """
    @tornado.web.authenticated
    def on_message(self, message):
        global global_rooms
        data = json_decode(message)
        send_to = False
        res = {}
        
        if data['type'] == 'join':
            participants = list(self.room.clients.keys())
            reply = {}
            
            if self.room.add_user(self):
                print(self.current_user.username,"connected to room ",self.room)
                reply  = {'type':'join','success':True,'peers':participants} 
            else:
                reply = {'type':'join','success':False}
                
            self.send_json(reply)
                
        elif data['type'] == 'offer':
            res = {'type':'offer','offer':data['offer'],'from':self.current_user.username}
            send_to = True
            
        elif data['type'] == 'answer':
            res = {'type':'answer','answer':data['answer'],'from':self.current_user.username}
            send_to = True
            
        elif data['type'] == 'candidate':
            res = {'type':'candidate','candidate':data['candidate'],'from':self.current_user.username}
            send_to = True
          
        elif data['type'] == 'leave':
            res = {'type':'leave','who':self.current_user.username}
            print(self.current_user.username,"disconnect from room ",self.room)
            
            # remove from room
            self.room.remove_user(self)
            
            # notify others
            for c in self.room.clients:
                self.room.clients[c].send_json(res)
            
        else:
            err = {'type':'error','message':'Unrecognized command ['+data['type']+']'}
            self.send_json(err)
        
        # we have a message to be delivered to someone!
        if send_to:
            if data['to'] in self.room.clients:
                self.room.clients[data['to']].send_json(res)

    """
    When websocket connection closed
    """
    @tornado.web.authenticated
    def on_close(self):
        res = {'type':'disconnected','who':self.current_user.username}
        self.room.remove_user(self)
        
        # notify others
        for c in self.room.clients:
            self.room.clients[c].send_json(res)
    
    """
    Shortcut for sending json message to client
    """
    def send_json(self,msg):
        self.write_message(json_encode(msg))

########################################################################
#       Chat
########################################################################

global_groups = {}

class GroupChatHandler(WebSocketHandler):

    @tornado.web.authenticated
    def open(self, company,project):
        global global_groups
        print(self.current_user.username+" connected")
        pro = proj_util.get_project(company,project)
        if pro is None:
            raise tornado.web.HTTPError(404)
            
        # check for user as a member
        pro_m = proj_util.get_member(pro.id, self.current_user.username)
        
        # if not
        if(pro_m is None):
            raise tornado.web.HTTPError(403)
        
        if pro.id not in global_groups:
            global_groups[pro.id] = {}
            
        self.group_id = ChatGroup.get(ChatGroup.project_id == pro.id).id

        self.group = global_groups[pro.id]
        self.group[self.current_user.username] = self
        
    @tornado.web.authenticated
    def on_message(self, message):
        data = json_decode(message)
        if data['type'] == 'message':
            new = GroupMessage.create(group = self.group_id,message=data['message'],\
                                author=self.current_user.username)
            res = json_encode({
                'id': new.id,
                'message':data['message'],
                'author':self.current_user.actual_name.title(),
                'date': new.date
            })

            for m in self.group:
                if self.group[m] != self:
                    self.group[m].write_message(res)
                    UserMessage.create(msg_id=new.id,                           \
                                        user=self.group[m].current_user.username)
            self.write_message(json_encode({'status':'ok'}))
        else:
            for id in data['seen']:
                m = UserMessage.get(id==id)
                m.seen = True
                m.save()

    @tornado.web.authenticated
    def on_close(self):
        self.group.pop(self.current_user.username)


class LoadChat(BaseHandler):
    @tornado.web.authenticated
    def get(self,company,project):
        pro = proj_util.get_project(company,project)
        if pro is None:
            raise tornado.web.HTTPError(404)
            
        # check for user as a member
        pro_m = proj_util.get_member(pro.id, self.current_user.username)
        
        # if not
        if(pro_m is None):
            raise tornado.web.HTTPError(403)

        messages = GroupMessage.select()
        
        res = []
        for m in messages: 
            remote = False
            if m.author.username != self.current_user.username:
                remote = True
                
            res.append({
                'id': m.id,
                'message':m.message,
                'author':m.author.actual_name.title(),
                'date': m.date.isoformat(),
                'remote':remote
            })
        self.write(json_encode(res))

