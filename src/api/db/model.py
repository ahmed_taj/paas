'''
Created on Apr 3, 2015

@author: One
'''
import peewee as orm
from playhouse.postgres_ext import PostgresqlExtDatabase, DateTimeTZField
import datetime
import pytz
from tzlocal import get_localzone

database = PostgresqlExtDatabase(None,register_hstore=False)

CompanyProxy = orm.Proxy() # to avoid NameError for Company in User model


def current_time():
    '''
    Get the current ISO formated date, time in Local timezone.

    @author: Ahmed Taj elsir
    '''
    
    return get_localzone().localize(datetime.datetime.now()).isoformat()


class BaseModel(orm.Model):
    '''
    This the BaseModel class, it's the base of all database model objects.
    you should inherit this class in order to interact with database
    '''
    class Meta:
        database = database


class User(BaseModel):
    username = orm.CharField(primary_key =True)
    password = orm.CharField()          #TODO: one day ask Ahmed to hash password :p
    actual_name = orm.CharField()
    company_id = orm.ForeignKeyField(CompanyProxy,null=True,to_field='id')
    email = orm.CharField(max_length=254,unique =True)
    phone = orm.IntegerField(null=True)
    join_date = DateTimeTZField(default=current_time()) 
    time_zone = orm.CharField(default="UTC")
    
    class Meta:
        db_table = 'users'


class Company(BaseModel):
    id = orm.PrimaryKeyField()
    name = orm.CharField(unique=True)
    owner = orm.ForeignKeyField(User,to_field='username')
    industry = orm.CharField(null=True)
    address = orm.CharField(null=True)
    phone = orm.IntegerField(null=True)
    email = orm.CharField(null=True)
    website = orm.CharField(null=True)
    time_zone = orm.CharField(default="GMT")
    creation_date = DateTimeTZField(default=current_time())
    
    class Meta:
        db_table = 'companies'

# initialize company proxy
CompanyProxy.initialize(Company)

class CompanyMember(BaseModel):
    company_id = orm.ForeignKeyField(Company,to_field='id')
    username = orm.ForeignKeyField(User,to_field='username')
    join_date = DateTimeTZField(default=current_time())
    role = orm.CharField(default="employee")

    class Meta:
        db_table = 'company_members'
        primary_key = orm.CompositeKey('company_id','username')

################################################################################
#                            Projects                                          #
################################################################################

class Project (BaseModel):
    id = orm.PrimaryKeyField()
    name= orm.CharField()
    company_id =  orm.ForeignKeyField(Company, to_field='id')
    manager= orm.ForeignKeyField(User,to_field='username')
    description = orm.CharField(null=True) 
    start_date = orm.DateField(default=current_time())
    end_date = orm.DateField(null=True)
    last_activity = DateTimeTZField(default=current_time())
    
    class Meta:
        db_table = 'projects'
        indexes = ( (('name','company_id'),True), )

class ProjectMember (BaseModel):
    project_id = orm.ForeignKeyField(Project,to_field='id')
    username = orm.ForeignKeyField(User,to_field='username')
    role = orm.CharField(default="developer")
    #TODO: remove join date
    join_date = DateTimeTZField(default=current_time())
    last_access = DateTimeTZField(default=current_time())

    class Meta:
        db_table = 'project_members'
        primary_key = orm.CompositeKey('project_id','username')

class SharedProjects(BaseModel):
    owner = orm.ForeignKeyField(User,to_field='username',related_name='sharer_username')
    viewer = orm.ForeignKeyField(User,to_field='username',related_name='viewr_username')
    project_id = orm.ForeignKeyField(Project,to_field='id')
    class Meta:
        db_table = 'shared_projects'
        primary_key = orm.CompositeKey('owner','viewer')
        
class ProjectInvites(BaseModel):
    inviter = orm.ForeignKeyField(User, to_field='username',related_name='inviter_username')
    invited = orm.ForeignKeyField(User, to_field='username',related_name='invited_username')
    #TODO: change time to invite date
    date = DateTimeTZField(default=current_time())
    class Meta:
        db_table='project_invites_Log'
        primary_key = orm.CompositeKey('inviter','invited')

################################################################################
#                            Sprint                                            #
################################################################################

class Sprint (BaseModel):
    id = orm.PrimaryKeyField()
    name = orm.CharField() 
    project_id = orm.ForeignKeyField(Project , to_field='id')
    description= orm.CharField(default="") 
    start_time= orm.DateField(default=current_time())
    end_time = orm.DateField(null=True)
    estimated_hours = orm.IntegerField(default=336) # 336 h = 2 weeks
    creation_date = DateTimeTZField(default=current_time())
    is_active = orm.BooleanField(default=False)
    
    class Meta:
        db_table = 'sprints'
        indexes = ( (('name','project_id'),True), )
    

class SprintColumn(BaseModel):
    id = orm.PrimaryKeyField()
    sprint_id = orm.ForeignKeyField(Sprint,to_field="id")
    name = orm.CharField()
    order = orm.IntegerField(default=0)
    class Meta:
        db_table='sprint_columns'
        indexes = ( (('name','sprint_id'),True), 
                    (('order','sprint_id'),True))
        
################################################################################
#                            Tasks                                             #
################################################################################

class Task (BaseModel):
    id = orm.PrimaryKeyField()
    name = orm.CharField() 
    assign_to = orm.ForeignKeyField(User , to_field='username',null=True)
    sprint_id = orm.ForeignKeyField(Sprint,to_field='id')
    status = orm.ForeignKeyField(SprintColumn , to_field='id')
    priority = orm.IntegerField(default=5)
    estimated_hours = orm.IntegerField(default=0)
    worked_hours = orm.IntegerField(default=0)
    description = orm.CharField(default="")
    order = orm.IntegerField(default=1)
    creation_date = DateTimeTZField(default=current_time())
    
    class Meta :
        db_table ='tasks'
        indexes = ( (('name','sprint_id'),True), )

class TaskProgress(BaseModel):
    task_id = orm.ForeignKeyField(Task,to_field='id')
    completed = orm.IntegerField(default=0)
    update_date = orm.DateField(default=current_time())
    sprint_id = orm.ForeignKeyField(Sprint,to_field="id")
    
    class Meta:
        db_table='task_progress'
        primary_key = orm.CompositeKey('task_id','update_date')


################################################################################
#                            Events                                            #
################################################################################

class Event(BaseModel):
    id =  orm.PrimaryKeyField()
    
    """ who, when, to which """
    author_id = orm.ForeignKeyField(User , to_field='username')
    creation_date = DateTimeTZField(default=current_time())
    project_id =  orm.ForeignKeyField(Project,to_field="id")
    
    """ information """
    title = orm.CharField()
    type = orm.IntegerField()
    description = orm.CharField(null=True)
    
    """ dates """
    start_date = DateTimeTZField()
    end_date = DateTimeTZField()
    
    """
    These attributes were deleted by Ahmed_taj 
        - location = orm.CharField(default="Online")
        - group_id =  orm.ForeignKeyField(Group,to_field="id",null=True)
    """
    def get_type_name(self):
        if self.type == 0 :
            return "Meeting"
        elif self.type == 1:
            return "To Do"
        elif self.type == 2:
            return "Milestone"
        elif self.type == 3:
            return "Other"
        else:
            return "Something went wrong!"
            
    class Meta:
        db_table='events'


################################################################################
#                            Chat                                              #
################################################################################
class ChatGroup(BaseModel):
    id          = orm.PrimaryKeyField()
    project_id  = orm.ForeignKeyField(Project,to_field="id")

class GroupMessage(BaseModel):
    id          = orm.PrimaryKeyField()
    group       = orm.ForeignKeyField(ChatGroup,to_field="id")
    message     = orm.CharField()
    type        = orm.IntegerField(default=0) # 0 for text, 1 for file
    date        = DateTimeTZField(default=current_time())
    author      = orm.ForeignKeyField(User , to_field='username')

class UserMessage(BaseModel):
    msg_id = orm.ForeignKeyField(GroupMessage,to_field="id")
    user   = orm.ForeignKeyField(User , to_field='username')
    seen   = orm.BooleanField(default=False)
    
    class Meta:
        primary_key = orm.CompositeKey('msg_id','user')


################################################################################
#                            Notification                                      #
################################################################################

class Notification(BaseModel):
    id   = orm.PrimaryKeyField()
    type = orm.IntegerField(default=0)
    date = DateTimeTZField(default=current_time())
    text = orm.CharField()

    class Meta:
        db_table='notifications'
        
class UserNotification(BaseModel):
    notification_id   =  orm.ForeignKeyField(Notification , to_field='id')
    username = orm.ForeignKeyField(User , to_field='username')
    seen     = orm.BooleanField(default=False)

    class Meta:
        db_table='user_notifications'
        primary_key = orm.CompositeKey('notification_id','username')


class Backlog(BaseModel):
    id = orm.PrimaryKeyField()
    name = orm.CharField()
    priority = orm.IntegerField()
    description = orm.CharField()
    diffuclty = orm.IntegerField()
    estimated_duaration = orm.IntegerField()

    class Meta:
        db_table='backlog'

