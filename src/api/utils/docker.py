'''
Created on Jun 22, 2015

@author: hammad
'''
import subprocess,os
import namesgenerator
from subprocess import Popen

containers={}

class container:
    

    def __init__(self,name="",id="",type="",link="",link_name="",to_run="bin/bash",ports=""):
        """ name is container name, type is container type postgre or
         python ..etc, to_run is the app that you want to run"""
        self.name = namesgenerator.get_random_name()
        if(name!=""):
            self.name = name 
        self.id = id #dont enter this field!
        self.type = type #ubuntu,centos
        self.link = link # container name
        self.link_name = link_name
        self.to_run = to_run#ex: /bin/bash , python web/app.py
        self.ports = ports
    
    def init(self):
        con_name=""
        if(self.name!=""):
            con_name="--name "+self.name
        con_type="ubuntu"
        if(self.type!=""):
            con_type=self.type
        con_link=""
        if(self.link!=""):
            con_link = "--link "+self.link+":"+self.link_name
        con_to_run = "/bin/bash"
        if(self.to_run!=""):
            con_to_run = self.to_run
        con_ports=""
        if(self.ports!=""):
            con_ports="-p "+self.ports
        con_terminal = " -i -d"
        container = "sudo docker run "+con_terminal+" "+con_name+" "+con_link+" "+con_ports+" "+con_type+" "+con_to_run
        print(container)
        x = subprocess.check_output(container,shell=True)
        print(x)
        self.id = x
        
    def run(self,command):
        try:
            x = subprocess.check_output("sudo docker exec "+self.name+" "+command,shell=True)
            print(x)
        except subprocess.CalledProcessError as e:
            print(e)
    def stop(self):
        x = subprocess.check_output("sudo docker stop "+self.name,shell=True)
        print(x)
        
#"companyName/projectName/username/pythonApp"
    
def addContainer(name="",id="",type="",link="",link_name="",to_run="bin/bash",ports=""):
    temp_con = container(name=name,id=id,type=type,link=link,link_name=link_name,to_run=to_run,ports=ports)
    containers[temp_con.name]=temp_con

def removeContainer(name):
    x = subprocess.check_output("sudo docker rm -f "+name,shell=True)
    #print(x)
    containers.pop(name)

