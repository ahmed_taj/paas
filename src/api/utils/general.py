'''
Created on May 18, 2015

@author: moh
'''
from api.db.model import *
import math
import os
import mimetypes

import hashlib
import binascii
import sys
import smtplib
from Crypto.Cipher import AES


# database env
db_url = os.environ["DB_URL"]
db_name = os.environ["DB_NAME"]
db_username = os.environ["DB_USERNAME"]
db_password = os.environ["DB_PASSWORD"]
db_port = os.environ["DB_PORT"]

# init database instance
database.init(db_name, host=db_url,user=db_username, password=db_password, port=db_port)

def calculateSprintGraph(sprint_id):
    
    sprint = Sprint.select(Sprint.start_time,Sprint.end_time,Sprint.estimated_hours).where(Sprint.id==sprint_id)
    #getting the length of a sprint using dates
    
    try:
        sprintLength= int((sprint[0].end_time-sprint[0].start_time).days)
        #number of weeks to be show in chart
        weeks = math.ceil(sprintLength/7.0)
    
        #creating X Axis for the chart
        axisX = ["Start"]
        for i in range(0,weeks):
            axisX.extend(["day 2","day 3","day 4","day 5","day 6","day 7","Week "+str(i+1)])
        dates = TaskProgress.select(TaskProgress.update_date).distinct().where(TaskProgress.sprint_id==1)\
                                                            .order_by(TaskProgress.update_date.asc())
        axisY= []
        TaskProgress
        day = 0
        total = 0
        print(dates.count())
        print(weeks)
        for date in dates:
            total = 0
            day = day +1
            print(date.update_date)
            hours = TaskProgress.select(TaskProgress.completed,TaskProgress.update_date)\
                                    .where(TaskProgress.update_date == date.update_date) \
                                    .where( sprint_id==TaskProgress.sprint_id)      \
                                    
            for i in hours:
                total +=i.completed
            axisY.append(sprint[0].estimated_hours - total)
        
        avg = sum(axisY)/len(axisY)
        sprintLength-day
    except:
        axisX = ["Start"]
        axisX.extend(["day 2","day 3","day 4","day 5","day 6","day 7","Week "+str(1)])
        axisY= ["0","0","0","0","0","0"]
    return axisX,axisY;

def inviteSearch(name,project_id):
    
    searchable = "%"+name+"%"

    """
    
    returns a list of potential invites with the same name from the same company
    
    @param name: email , name or username of the possible invite
    @param project_id: the id of the selected project
    
    @author: hitkid
    """
    searchable = "%"+name+"%"
    #Get Company ID
    company_id = Project.select(Project.company_id).where(Project.id == project_id)
    
    #Get a list of all project memebrs
    #this is to exclude them from the list
    project_members = ProjectMember.select(ProjectMember.username).where(ProjectMember.project_id == project_id)
    userList = []
    
    for member in project_members:
        userList.append(member.username.username)
    #TODO: refactor this using not_in
    #search for other company members
    ex_members =  [CompanyMember for CompanyMember in\
     CompanyMember.select(CompanyMember.username,User.email,User.actual_name)\
    .where(CompanyMember.company_id==company_id).join(User)\
    .where((User.username % searchable) | (User.email % searchable) |(User.actual_name % searchable) )\
      if CompanyMember.username.username  not in userList ]
    
    result = []

    for member in ex_members:
        result.append({"name":member.username.username,"email":member.username.email})
    
    return result

def get_content_type(ext):
    if( ext in mimetypes.types_map.keys()):
        return mimetypes.types_map[ext]
    else:
        return 'text/plain'

def authenticate_user(username,password):
    try:
        user = User.select(User.username).where((User.username == username) & (User.password == password)).limit(1).count()
        if(user>0):
            return True
    except Exception as e:
        print(e)
    return False


def checkTemp(link):
    try:
        tempLink = binascii.unhexlify(link)
    except:
        print(sys.exc_info())
        print("invalid link")
        return "","","","",""
    encrypter = AES.new('1234567890123456', AES.MODE_CBC, 'this is an iv456')
    decryption = encrypter.decrypt(tempLink)
    length = len(decryption)
    
    hash_code = decryption[length-32:length]
    data= decryption[:length-32].strip()
    hasher = hashlib.sha256()
    hasher.update(data)
    hashCode = hasher.digest()
    if(hash_code==hashCode):
        array = data.decode().split(",",5)

        creation_date = datetime.datetime.strptime(array[4], "%Y-%m-%d %H:%M:%S.%f")
        days_passed = (datetime.datetime.now()-creation_date).days
        if(days_passed > 3):
            raise Exception("Invite is out of date")
            return "","","","",""
        return array[0],array[1],array[2],array[3]
    else:
        raise Exception("Hash Didn't Match")
        return None

        
def createTemp(email,project_id,role="developer"):
    inviter = "hammad" #self.get_current_user()
    delim = ','
    data = inviter+delim+email+delim+role+delim+project_id+delim+str(datetime.datetime.now())
    data = data.encode(encoding='utf_8', errors='strict')
    # to make the link a multiple of 16 by adding for AES with the addition of spaces
    newData = data+b' '*(16-len(data)%16)
    
    hasher = hashlib.sha256()
    hasher.update(data)
    hashCode = hasher.digest()
    
    encrypter = AES.new('1234567890123456', AES.MODE_CBC, 'this is an iv456')
    result = encrypter.encrypt(newData+hashCode)
    link = binascii.hexlify(result).decode()
    print(email+" : ",link)
    #mailMessage(link, email)
    

def mailMessage(msg,email):
    server = smtplib.SMTP('smtp.gmail.com',587) #port 465 or 587
    server.ehlo()
    server.starttls()
    server.ehlo()
    server.login('emailsmtptester@gmail.com','testertest')
    server.sendmail('emailsmtptester@gmail.com',email,msg)
    server.close()

def recFolderTree(dir,parent):
    folder = {}
    files = []
    result = {}
    for file in os.listdir(dir):
        if os.path.isdir(os.path.join(dir,file)):
            folder = recFolderTree(os.path.join(dir,file),parent)
            result[(dir+"/"+file)[len(parent):].replace("\\","/")]=folder
        else:
            files.append(file)
        files.sort()
        result['>'] = files
    return result

def getTree(dir):
    return recFolderTree(dir,dir)

"""
users = inviteSearch("", 1)
emails = []
for i in users:
    emails.append(i["email"])
print(emails)
createTemp("hammad", "1", "admin")
"""

