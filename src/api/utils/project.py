'''

Project utility methods v0.1

Created on July 2, 2015

@author: One
'''

import peewee as orm
import api.db.model as model
import os
from api.utils.general import createTemp
import api.vcs.git2 as git
from DockerServer import DockerSocketClient
db_url = os.environ["DB_URL"] # localhost
db_name = os.environ["DB_NAME"] # team-studio
db_username = os.environ["DB_USERNAME"] # postgres
db_password = os.environ["DB_PASSWORD"] # password
db_port = os.environ["DB_PORT"] # port

# init database instance
model.database.init(db_name, host=db_url,user=db_username, password=db_password, port=db_port)

def get_project(comp_name, proj_name):
    #FIXME: Query is case Sensetive
    '''
    Get specific project.
    
    @param comp_name: Target company name as string
    @param username: Target project name as string.
    
    @author: One
    '''
    try:
        target_company = model.Company.get(model.Company.name == comp_name)
        target_project = model.Project.get((model.Project.company_id == target_company.id,)&
                                         (model.Project.name == proj_name))
        return target_project
    except orm.DoesNotExist:
        return None


def get_member(project_id, username):
    '''
    Get specific project member.
    
    @param project_id: Target project id
    @param username: The username as string.
    
    @author: One
    '''
    try:
        member = model.ProjectMember.get((model.ProjectMember.project_id == project_id)&
              (model.ProjectMember.username == username))
        return member
    except orm.DoesNotExist:
        return None
    
def is_admin(project_obj, username):
    '''
    Check if specific project member has the role admin.
    
    @param project_obj: Target project object
    @param username: The username as string.
    
    @author: One
    '''
    try:
        member = model.ProjectMember.get((model.ProjectMember.project_id == project_obj.id)&
                                        (model.ProjectMember.username == username) &
                                        (model.ProjectMember.role == "admin"))
        return True
    except orm.DoesNotExist:
        return False    
    
def is_project_member(uri,username):
    #TODO: Test them
    '''
    @param uri includes project name and company name
    @param company: company name
    @param project: project name
    @param username: wanted member name

    @author: hitkid
    '''
    result = uri.split("/")
    comp_name = result[2]
    proj_name = result[3][:-4]
    print(comp_name)
    print(proj_name)
    member = None
    project = get_project(comp_name, proj_name)
    if(project!=None):
        member = get_member(project, username)
    
    if(member!= None):
        return True
    return False

def invite_users_to_project(users,project_id):
    #TODO: reminder check for current user if he is authorized to add
    '''
    @param users: list of users that are going to be invited to project
    @param project_id: the id of the project
    
    @author: hitkid
    '''
    #result = [user for user in Users.select() if user.name not in a_list_of_names]
    #usernames = [user for user in model.User.select(model.User.username) if model.User.email not in users]
    usernames =model.User.select(model.User.username,model.User.email).where(model.User.email.in_(users))
    
    
    for user in usernames:
        try:
            users.remove(user.email)
            model.ProjectMember.create(project_id=project_id,username=user)
            proj_obj = model.Project.get(model.Project.id==project_id)
            comp_name = proj_obj.company_id.name
            #ProjectMember.create(project_id=project_id,username=data["username"])
            git.UserRepository(comp_name,proj_obj.name,user,create=True)
        except Exception as e:
            print("Exception Encountered")
            print(e)
    #TODO: Test this with actual values
    for user in users:
        createTemp(email=user,project_id=project_id)

def project_is_shared(username,target,project_id):
    available = model.SharedProjects.select().where((model.SharedProjects.owner==target) & (model.SharedProjects.viewer== username) & (model.SharedProjects.project_id== project_id )).limit(1).count()
    if available == 0:
        return False
    return True


#users = ['abc@gmail.com','addd@gmail.com']
#invite_users_to_project(users,"1")
#git.BaseRepository("Elite","Grad Project",create=True)
