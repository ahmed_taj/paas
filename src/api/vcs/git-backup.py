'''

Git version control system API

Created on July 2, 2015

@author: One
@version: v0.1

'''

import pygit2 as git
import os
import sys

# All repositories root path
repo_root = os.environ["REPO_ROOT"] 

class Repository(git.Repository):

    '''
    Repository class, inherit pygit2.Repository class
    
    @author: One
    '''

    company = ''
    project = ''
    user = None
    path = ''
    
    def __init__(self,company,project,user=None,create=False):
        '''
        Create repository object from existing repository url.
        
        @warrning: This class deals with NON-BARE repositories only.
        @author: One
        '''
        # prepare the path
        self.path = repo_root +'/projects/'+company+'/'+project
        self.company = company
        self.project = project
        self.user = user
        
        #FIXME: How can I do authentication with encrypted user password!!
        
        if create :
            # create the dirs
            os.makedirs(self.path, mode=0o777, exist_ok=True)
            
            # init the main repository
            git.init_repository(self.path, bare=False)
            set_permission(self.path, 0o777)
            
            super(Repository,self).__init__(self.path)
                                    
        else:
            super(Repository,self).__init__(self.path)
                    
    def add_files(self,files=[]):

        '''
        Add given files to repository stage

        @param files: List of files.

        @author: One
        '''

        try:
            # get entry index and add the files
            index = self.index

            for f in files:
                index.add(f)
                index.write()
                
            return True
        except Exception as e:
            print("\nException from add_files() :")
            print(e)
            return False
 
    def add_all(self):

        '''
        Add all files to the repository stage

        @author: One
        '''

        try:
            # get entry index and add the files
            index = self.index
            index.add_all()
            index.write()
            return True
        except Exception as e:
            print("\nException from add_all() :")
            print(e)
            return False 
 
    def do_commit(self,author=None,committer=None,message=""):

        '''
        Do commmit to the target repository.

        @param author: The author User object.
        @param committer: The committer User object.
        @param message: The commmit message.
        @param tree: Repository tree .

        @author: One
        '''

        author_sig = git.Signature("ahmed","a@a.com")
        committer_sig = git.Signature("ali","ali@a.com")
        # get repository tree
        tree = self.index.write_tree()

        # check if there is master branch
        try:
            ref = self.head.name
            # parent of the commit 
            str_list = [self.head.target]
        except:
            ref = 'refs/heads/master'
            # set the parent to empty if it's the first commit
            str_list = []

        # try to do the commit
        try:
            self.create_commit(ref,author_sig,committer_sig,message,tree,str_list)
            return True
        except Exception as e:
            print("\nException during create_commit  :")
            print(e)
            return False

    def create_branch(self,name):

        '''
        Create branch in the repository.
        
        @param name: Name of branch.

        @author: One
        '''
        try:
            self.create_branch(name, self.head.get_object())
            return True
        except Exception as e:
            print("\nException from create_branch() :")
            print(e)
            return False

    def checkout_branch(self,name):

        '''
        Checkout existing branch in the repository.

        @param name: Name of branch.

        @author: One
        '''

        try:
            branch = self.lookup_branch(name)
            ref = self.lookup_reference(branch.name)
            self.checkout(ref)
        except Exception as e:
            print("\nException from checkout_branch() :")
            print(e)
            return False













        
#
#               Create & Clone
#______________________________________________________________________________


#bare
def clone_repo(src_repo, to_path,checkout_branch=None):

    '''
    Clone from source repoistory to the target path
    
    @param src_repo: The source repository to be cloned
    @param to_path: The path which on it the new repository will be cloned.
    
    @author: One
    '''
    
    try:
        clone = git.clone_repository(src_repo.path,to_path,checkout_branch=checkout_branch)
        return clone
    except Exception as e:
        print("\nException from clone_repo() :")
        print(e)
        return None

#bare
def create_member_clone(company,project,username):

    '''
    Create a clone for a project member.

    @param project: Target project name    
    @param company: Target company name   
    @param username: Target username
    
    @author: One
    '''
    
    try:
        user_path = repo_path+"/users/"+username+"/"+company+"/"+project
        repo_dir = repo_path+"/projects/"+company+"/"+project+".git"
        os.makedirs(user_path, mode=0o777, exist_ok=True)
        set_permission(user_path,0o777)
        clone = git.clone_repository(repo_dir,user_path)
        return clone
    except Exception as e:
        print("\nException from create_member_clone() :")
        print(e)
        return None

#
#               Branches
#______________________________________________________________________________

    
#
#               Other utility
#______________________________________________________________________________
#TODO: move this method to api.util.general
def set_permission(dir,mode):

    '''
    Set permission to specific <dir> with specific <mode>
    
    @param dir: target directory
    @param mode: required mode
    
    @author: One
    '''
    
    for r,d,f in os.walk(dir):
        os.chmod(r,mode)
    """ if files stop working because insufficient permission
    os.chmod(dir,0o777)
    for r , d , _ in os.walk(dir):
        for dir in d:
            chmod(os.path.join(r,dir),0o777)
    """



# testing purpose only
repo = Repository(company="Elite",project="project")

repo.add_all()
repo.do_commit(message="initial commmit")
#print(repo.path)
#create_branch(repo,"test")
#b = repo.lookup_branch('foo')#, repo.head.get_object(),force=True)
#print(b)
#checkout_branch(repo,"master")
#b.delete()

'''
for rem in repo.remotes:
    rem.push_url = rem.url
    rem.credentials = git.UserPass("ahmed","123")
    sig = git.Signature("ahmed","a@a.com")#repo.default_signature
          #git.Signature("ahmed","a@a.com")#repo.default_signature

    rem.push('refs/heads/master',sig,"message")
'''    


'''
add_all(repo)

do_commit(repo,message="second commit",tree=tree)
'''



