'''

Git version control system API

Created on July 2, 2015

@author: One
@version: v0.1

'''

import pygit2 as git
import os
import sys
import subprocess # only for push and pull
import itertools

def set_permission(dir,mode):
    '''
    Set permission to specific <dir> with specific <mode>
    
    @param dir: target directory
    @param mode: required mode
    
    @author: One
    '''    
    for r,d,f in os.walk(dir):
        os.chmod(r,mode)

# All repositories root path
repo_root = os.environ['REPO_ROOT'].replace('\\','/') 

class Repository(git.Repository):
    '''
    Repository class, inherit pygit2.Repository class
    
    @author: One
    '''
    def __init__(self,company,project,branch,create=False):
        '''
        Create repository object from existing repository url.
        
        @warrning: This class deals with NON-BARE repositories only.
        @author: One
        '''
        # prepare the path
        self.url = repo_root +'/projects/'+company+'/'+project+'/'+branch
        self.company = company
        self.project = project
        self.branch = branch
        self.created = True
        
        #FIXME: How can I do authentication with encrypted user password!!
        
        if create :
            try:
                # create the dirs
                os.makedirs(self.url, mode=0o775, exist_ok=True)
                set_permission(self.url, 0o775)            
                clone = BaseRepository(company,project).clone(self.url,branch=branch)
                if(clone is None):
                    self.created = False
            except Exception as e:
                print('\nException during Repository.init :')
                print(e)
                self.created = False
        
        # call super
        super().__init__(self.url)
            
    def add_files(self,files=[]):

        '''
        Add given files to repository stage

        @param files: List of files.

        @author: One
        '''

        try:
            # get entry index and add the files
            index = self.index

            for f in files:
                index.add(f)
                index.write()
                
            return True
        except Exception as e:
            print('\nException from add_files() :')
            print(e)
            return False
 
    def add_all(self):

        '''
        Add all files to the repository stage

        @author: One
        '''

        try:
            # get entry index and add the files
            index = self.index
            index.add_all()
            index.write()
            return True
        except Exception as e:
            print('\nException from add_all() :')
            print(e)
            return False  

    def do_commit(self,user,message=''):

        '''
        Do commmit to the target repository.

        @param user: The User object.
        @param message: The commmit message.

        @author: One
        '''

        signature = git.Signature(user.actual_name,user.email)

        # get repository tree
        tree = self.index.write_tree()
        
        try:
            ref = self.head.name
            # parent of the commit 
            str_list = [self.head.target]
        except:
            ref = 'refs/heads/master'
            # set the parent to empty if it's the first commit
            str_list = []

        # try to do the commit
        try:
            self.create_commit(ref,signature,signature,message,tree,str_list)
            
            #FIXME: push code here
            return True
        except Exception as e:
            print('\nException during create_commit  :')
            print(e)
            return False
            
    def destory(self):
        if(os.path.exists(self.url)):
            import shutil
            shutil.rmtree(self.url, ignore_errors=True)
        return True


class UserRepository(git.Repository):
    #TODO: add: merge pull push fetch
    def __init__(self,company,project,user,create=False):

        self.company = company
        self.user = user
        self.project = project
        self.url = repo_root+'/users/'+user.username+'/'+company+'/'+project

        self.created = True
        
        if create :
            try:
                # create the dirs
                os.makedirs(self.url, mode=0o775, exist_ok=True)
                set_permission(self.url, 0o775)
                # clone the repo
                clone = BaseRepository(company,project).clone(self.url)
                if(clone is None):
                    self.create = False
            except Exception as e:
                print('\nException during Repository.init :')
                print(e)
                self.created = False
        # call super
        super().__init__(self.url)
    def add_files(self,files=[]):

        '''
        Add given files to repository stage

        @param files: List of files.

        @author: One
        '''

        try:
            # get entry index and add the files
            index = self.index

            for f in files:
                index.add(f)
                index.write()
                
            return True
        except Exception as e:
            print('\nException from add_files() :')
            print(e)
            return False
 
    def add_all(self):

        '''
        Add all files to the repository stage

        @author: One
        '''

        try:
            # get entry index and add the files
            index = self.index
            index.add_all()
            index.write()
            return True
        except Exception as e:
            print('\nException from add_all() :')
            print(e)
            return False  
            
    def do_commit(self,message=''):
        '''
        Do commmit to the target repository.

        @param user: The User object.
        @param message: The commmit message.

        @author: One
        '''
        signature = git.Signature(self.user.actual_name,self.user.email)

        # get repository tree
        tree = self.index.write_tree()
        
        #TODO: test it tooo much
        try:
            ref = self.head.name
            # parent of the commit 
            str_list = [self.head.target]
        except:
            ref = 'refs/heads/master'
            # set the parent to empty if it's the first commit
            str_list = []

        # trying to do the commit
        try:
            self.create_commit(ref,signature,signature,message,tree,str_list)
            return True
        except Exception as e:
            print('\nException during create_commit  :')
            print(e)
            return False

    def create_branch(self,name):

        '''
        Create branch in the repository.
        
        @param name: Name of branch.

        @author: One
        '''
        try:
            self.create_branch(name, self.head.get_object())
            return True
        except Exception as e:
            print('\nException from create_branch() :')
            print(e)
            return False
            
    def checkout_branch(self,name):

        '''
        Checkout existing branch in the repository.

        @param name: Name of branch.

        @author: One
        '''

        try:
            branch = self.lookup_branch(name)
            ref = self.lookup_reference(branch.name)
            self.checkout(ref)
        except Exception as e:
            print('\nException from checkout_branch() :')
            print(e)
            return False  
            
            
    def destory(self):
        if(os.path.exists(self.url)):
            import shutil
            shutil.rmtree(self.url, ignore_errors=True)
        return True
        

class BaseRepository(git.Repository):

    def __init__(self,company,project,create=False):

        '''
        Create repository object from existing repository url.
        
        @warrning: This class deals with bare repositories only.
        @author: One
        '''
        # prepare the path
        self.url = repo_root +'/projects/'+company+'/'+project+'.git'
        self.company = company
        self.project = project
        self.branches = {}

        
        #FIXME: How can I do authentication with encrypted user password!!
        
        if create :
            try:
                # create the dirs
                os.makedirs(self.url, mode=0o775, exist_ok=True)
                # init the main repository
                git.init_repository(self.url, bare=True)
                set_permission(self.url, 0o775)
            except Exception as e:
                print('\nException during Repository.init :')
                print(e)
                self.created = False        
        # call super
        super().__init__(self.url)

    def clone(self,to_url,branch=None):
        '''
        Clone this repoistory to the target url

        @param to_url: The url which on it the new repository will be cloned.

        @author: One
        '''
        try:
            clone = git.clone_repository(url=self.url,path=to_url,checkout_branch=branch)
            return clone
        except Exception as e:
            print('\nException from clone() :')
            print(e)
            return None
            
    def clones(self):
        branch_list = self.listall_branches()
        
        if (len(branch_list) != len(self.branches)):
            # new branches
            new_br = list(set(branch_list) -  set(self.branches.keys()))
            
            for branch in new_br:
                temp = Repository(self.company,self.project,branch,create=True)
                if(temp is not None):
                    self.branches[branch] = temp

            # deprecated/deleted branches
            depr_br = list(set(self.branches.keys()) -  set(branch_list))
            
            for branch in depr_br:
                self.branches[branch].destroy()
                self.branches.pop(branch)
        
        return self.branches
        
    def destory(self):
        if(os.path.exists(self.url)):
            import shutil
            shutil.rmtree(self.url, ignore_errors=True)
        return True
        
    def get_branch_ref(self,name):
        try:
            return self.lookup_reference("refs/heads/"+str(name))
        except:
            return None
            
    def get_entry(self,obj,dir):
        return obj.tree[dir]

    def get_tree(self,tree_obj):
        return self[tree_obj.hex]

    def get_blob(self,entry):
        return self[entry.hex]

    def get_diff_files(self,commit):
        diff = commit.tree.diff_to_tree(commit.parents[0].tree)
        files = []
        for patch in diff:
            files.append(patch.delta.new_file)
        return files

    def blob_history(self, file_name, sort=0):
        walker = repo.walk(self.head.target.hex, sort)
        c0 = next(walker)
        for c1 in walker:
            files_in_diff = ((x.delta.old_file.path, x.delta.new_file.path) for x in c1.tree.diff_to_tree(c0.tree))
            if file_name in itertools.chain(*files_in_diff):
                yield c0
            c0 = c1

'''
class User:
    def __init__(self):
        self.name = 'ahmed'
        self.email = 'a@a.com'
    def print(self):
        print(self.name)
        
user = User()

repo = BaseRepository('Elite','mytest')
print(repo.clones())
#usr_repo = UserRepository('Elite','mytest',user)
#repo.add_all()
#repo.do_commit('initial commmit')
'''


