import api.db.model as model
import os
# database env
db_url = os.environ["DB_URL"] # localhost
db_name = os.environ["DB_NAME"] # team-studio
db_username = os.environ["DB_USERNAME"] # postgres
db_password = os.environ["DB_PASSWORD"] # password
db_port = os.environ["DB_PORT"] # port

# init database instance
model.database.init(db_name, host=db_url,user=db_username, password=db_password, port=db_port)

model.database.create_tables([model.SharedProjects,model.ProjectInvites,
                            model.ProjectMember,
                              model.User, 
                              model.Company,
                              model.Project,
                              model.Sprint, 
                              model.User, 
                              model.CompanyMember,
                              model.Task, 
                              model.TaskProgress,
                              model.Event,
                              model.SprintColumn,
                              model.Notification,
                              model.UserNotification,
                              model.ChatGroup,
                              model.GroupMessage,
                              model.UserMessage,
                              model.Backlog])

# fill some values

# users

model.User.create(username="ahmed",password="123",actual_name="ahmed", email="ahmed@email.com")

model.User.create(username="ayman",password="123",actual_name="ayman", email="ayman@email.com")

model.User.create(username="hammad",password="123",actual_name="hammad",email="hammad@email.com")

model.User.create(username="hassan",password="123",actual_name="hassan",email="hassan@email.com")

model.User.create(username="mohammed",password="123",actual_name="mohammed",email="mohammed@email.com")

model.User.create(username="sharaf",password="123",actual_name="sharaf",email="sharaf@email.com")

model.User.create(username="suhail",password="123",actual_name="suhail",email="suhail@email.com")

model.User.create(username="taj",password="123",actual_name="taj",email="taj@email.com")

model.User.create(username="hussain",password="123",actual_name="hussain",email="hussain@email.com")


# company and company members

elite = model.Company.create(name="Elite",owner="ahmed")

# projects and project members

grad_pro = model.Project.create(name="Grad Project",company_id = elite.id,manager="ahmed")

model.ProjectMember.create(project_id=grad_pro.id,username="ahmed")

model.ProjectMember.create(project_id=grad_pro.id,username="ayman")

model.ProjectMember.create(project_id=grad_pro.id,username="hammad")

# add group for project
model.ChatGroup.create(project_id=grad_pro.id)


sprint = model.Sprint.create(project_id=grad_pro.id , name="Sprint 1",is_active=True)

model.SprintColumn.create(sprint_id = sprint.id , name="New", order=0)
model.SprintColumn.create(sprint_id = sprint.id , name="Commited", order=1)
model.SprintColumn.create(sprint_id = sprint.id , name="Approved", order=2)
model.SprintColumn.create(sprint_id = sprint.id , name="Done", order=3)


model.Backlog.create(name = "Backlog 1" , priority = 1 , description = "for test " , diffuclty = 1 , estimated_duaration = 2)
model.Backlog.create(name = "Backlog 2" , priority = 1 , description = "for test " , diffuclty = 1 , estimated_duaration = 2)
model.Backlog.create(name = "Backlog 3" , priority = 1 , description = "for test " , diffuclty = 1 , estimated_duaration = 2)
model.Backlog.create(name = "Backlog 4" , priority = 1 , description = "for test " , diffuclty = 1 , estimated_duaration = 2)
model.Backlog.create(name = "Backlog 5" , priority = 1 , description = "for test " , diffuclty = 1 , estimated_duaration = 2)

print("All went good")