import tornado.ioloop
import tornado.web
import sys
import base64
import urllib
from api.utils.general import authenticate_user
from api.utils.project import is_project_member


#CODE IS TESTED WORKS WHEN USERNAME = "hammad" & password = "haha"
class MainHandler(tornado.web.RequestHandler):
	def post(self):
		print(str(self.request))
		print("hello plz")
		raise tornado.web.HTTPError(401)
	def get(self):
		#to know if to raise error or not
		#flag is turned off when user and pass match
		raise_flag = True
		print("asdfdsf")
		try:
			#getting username and password from the header
			string = self.request.headers["Authorization"]
			print(string)
			#getting uri from header
			#splitting username and password, they reach as username:password
			#first 5 letters are always BASIC so we skip them, plus decoding from base64
			auth = b64(string[6:]).split(":")
			#username
			username = urllib.parse.unquote(auth[0])
			#password
			password = urllib.parse.unquote(auth[1])
			#if they match don't raise exception
			uri = urllib.parse.unquote(self.request.headers["X-Original-Uri"])
			
			if(authenticate_user(username,password)):
				print("user Authenticated")
				if(is_project_member(uri,username)):
					print("true")
					raise_flag = False
			else:
				print("false")
		except :
			print("Unexpected error:", sys.exc_info()[0])
		if(raise_flag):
			raise tornado.web.HTTPError(401)

def b64(b):
	return base64.b64decode(b).decode('utf-8')

application = tornado.web.Application([
    (r"/", MainHandler),
])

if __name__ == "__main__":
    application.listen(8070)
    tornado.ioloop.IOLoop.current().start()
