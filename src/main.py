'''
Created on Apr 2, 2015

@author: One
'''

import tornado.ioloop
import tornado.web
import api.base.auth as auth
import api.base.handle as handle
import os


class TestHandler(handle.BaseHandler):
    def get(self):
        self.render('calendar.html')

    
def make_app():
    settings = {
        "template_path": "./template",
        "login_url": "/signin",
        "cookie_secret": "this_is_secret_cookie",
        "static_path": "./template/static",
    }
    application = tornado.web.Application([
        # event handler 
        (r"/test", TestHandler),

        # backlog
        (r"/get-backlog",handle.GetBacklog),
        (r"/([^\\/]*)/([^\\/]*)/add-backlog",handle.AddBacklog),
        (r"/update-backlog-sorting",handle.UpdateSorting),
        (r"/update-backlog-table",handle.UpdateTable),
        (r"/delete-backlog",handle.DeleteBacklog),
        # Main                                     
        (r"/", handle.MainHandler),
        (r"/signin/?", auth.SignInHandler),
        (r"/signup/?", auth.SignupHandler),
        (r"/signout/?", auth.SignOutHandler),
        (r"/invite/.*", auth.InviteHandler),
        (r"/inviteUser/?", auth.InviteUserHandler),
        
        # users
        (r"/userSearch/?",handle.UserSearch),
        (r"/get-user-companies/?",handle.UserCompanies),
        
        # sprint 
        
        (r"/add-sprint-with-name/?", handle.CreateSprintWithName),
        (r"/get-sprint/?", handle.GetSprint),
        (r"/delete-sprint/?", handle.DeleteSprintHandler),
        (r"/get-chart/?",handle.SprintChartHandler),
        (r"/add-sprint-column/?",handle.AddSprintColumnHandler),
        (r"/get-column-number/?",handle.GetColumnNumberHandler),
        # tasks 
        
        (r"/add-task/?", handle.AddTask),
        (r"/update-task/?", handle.UpdateTask),
        (r"/update-task-name/?", handle.UpdateTaskName),
        (r"/([^\\/]*)/([^\\/]*)/get-task-data/?", handle.GetTaskData),
        (r"/add-task-object/?", handle.AddTaskObject),
        (r"/delete-task/?",handle.DeleteTask),
        
        # meeting
        (r"/([^\\/]*)/([^\\/]*)/meeting/start/?",handle.StartMeetingHandler),
        (r"/([^\\/]*)/([^\\/]*)/meeting/([^\\/]*)/?",handle.MeetingHandler),
        (r"/([^\\/]*)/([^\\/]*)/meeting/([^\\/]*)/ws/?",handle.MeetingWsHandler),
        
        # events
       
        (r"/([^\\/]*)/([^\\/]*)/add-event/?", handle.AddEvent),
        (r"/([^\\/]*)/([^\\/]*)/update-event/?", handle.UpdateEvent),
        (r"/([^\\/]*)/([^\\/]*)/update-full-event/?", handle.UpdateFullEvent),
        (r"/([^\\/]*)/([^\\/]*)/events/json-feed/?", handle.EventsJSONFeed),
        (r"/([^\\/]*)/([^\\/]*)/remove-event/?", handle.RemoveEvent),
        
        (r"/([^\\/]*)/([^\\/]*)/chat/?", handle.GroupChatHandler),
        (r"/([^\\/]*)/([^\\/]*)/chat/load?", handle.LoadChat),
        
        # repository
 
        (r"/([^\\/]+)/([^\\/]+)/commits/?", handle.CommitsLog),
        (r"/([^\\/]+)/([^\\/]+)/commits/([^\\/]+)", handle.CommitsDiff),
        (r"/([^\\/]+)/([^\\/]+)/tree/([^\\/]+)/(.*)/?", handle.SourceTree),
        (r"/([^\\/]+)/([^\\/]+)/tree/([^\\/]+)/?", handle.SourceTree),
        (r"/([^\\/]+)/([^\\/]+)/tree/?", handle.SourceTree),
        (r"/([^\\/]+)/([^\\/]+)/branches", handle.BranchLog),
        (r"/([^\\/]+)/([^\\/]+)/tags", handle.TagsLog),

        (r"/([^\\/]+)/([^\\/]+)/blob/([^\\/]+)/(.*)", handle.BlobViewer), 
        # notifications
        (r"/notification-center", handle.NotificationHandler),
        
        # projects
        (r"/create-project/?", handle.CreateProject),
        (r"/update-project/?", handle.UpdateProjectHandler),
        (r"/getProjectData/?",handle.GetProjectData),
        (r"/delete-project/?",handle.DeleteProject),
        (r"/invite-user",handle.UserInvite),
        (r"/([^\\/]*)/([^\\/]*)/?", handle.ProjectHandler),
        
        (r'/([^\\/]*)/([^\\/]*)/editor/raw/(.*)', handle.RawFileReader),
        (r'/([^\\/]*)/([^\\/]*)/editor/save/(.*)', handle.RawFileWriter),
        
        # ide handler
        (r"/([^\\/]*)/([^\\/]*)/editor/?",handle.IDEHandler),

        (r"/([^\\/]*)/([^\\/]*)/editor/tree/?",handle.UserRepositoryTree),
        (r"/([^\\/]*)/([^\\/]*)/editor/run/?",handle.VMHandler),
        

        

    ],debug=False, **settings)
    return application


def main():
    app = make_app()
    app.listen(8080)
    tornado.ioloop.IOLoop.instance().start()
    
if __name__ == "__main__":
    main()
