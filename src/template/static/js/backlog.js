$(document).ready(function(){

function newBacklog()
{
	console.log("new  backlog Element ");
	var tbody = document.querySelectorAll("#backlog-board #table #sort > tbody");

	var tr = document.createElement("tr");
	tr.innerHTML = '<th data-field="name">'+
						'<input type="text" onkeypress="enterPressed(event)" onblur="bluerd(this.id)"class="form-control" placeholder="name" aria-describedby="basic-addon1" id="name">'+
						'</th>'+
					'<th data-field="estimated_duaration">'+
						'<input type="text" onkeypress="enterPressed(event)" class="form-control" placeholder="duaration" aria-describedby="basic-addon1" id="duaration">'+
					'</th>'+
					'<th data-field="description">'+
						'<input type="text" onkeypress="enterPressed(event)" class="form-control" placeholder="description" aria-describedby="basic-addon1" id="description">'+
					'</th>'+
					'<th data-field="priority">'+
					'<input type="text" onkeypress="enterPressed(event)" class="form-control" placeholder="priority" aria-describedby="basic-addon1" id="priority">'+
					'</th>'+
					'<th data-field="diffuclty">'+
					'<input type="text" onkeypress="enterPressed(event)" class="form-control" placeholder="diffuclty" aria-describedby="basic-addon1" id="diffuclty">'+
					'</th>';
	$(tbody).prepend(tr);
}


function loadBacklog()
{
	console.log("on load backlog");
	var container = document.querySelector("#backlog-board");
	var table = document.querySelector("#backlog-board #table #sort");
	var tbody = document.querySelector("#backlog-board #table #sort > tbody");
	var button = document.querySelector("#backlog-board #add-button > button");

	$(button).on("click",newBacklog);

	removeAllChilds(tbody);

	data={};
	myAjax(method="POST", url="/get-backlog", data=data,
			response= function(data){
				var data_length = Object.keys(data).length;

                var i = 0 ;
				for(i ; i < data_length ; i++)
				{
					var tr = document.createElement("tr");
					tr.innerHTML =  '<td data-field="id" id='+data[i]["backlog_id"]+'>'+data[i]["backlog_id"]+'</td>'+
									'<td data-field="estimated_duaration">'+data[i]["estimated_duaration"]+'</td>'+
									'<td data-field="description">'+data[i]["description"]+'</td>'+
									'<td data-field="priority">'+data[i]["priority"]+'</td>'+
									'<td data-field="diffuclty">'+data[i]["diffuclty"]+'</td>';
					tbody.appendChild(tr);
				}
				table.appendChild(tbody);
	});
}

function bluerd (id)
{
	console.log(id);
}

function enterPressed(evt)
{
	console.log("Key pressed");
	console.log(evt);
	 if (evt.keyCode == 13) {
	 	var input = document.querySelector("#backlog-board > div > table > tbody > tr > th #name");
	 	var name = input.value ; 
	 	if(name=='')
	 	{
	 		loadBacklog() ;
	 	}
	 	else
	 	{
	 		var description = document.querySelector("#backlog-board > div > table > tbody > tr > th #description").value;
	 		var priority = document.querySelector("#backlog-board > div > table > tbody > tr > th #priority").value;
	 		var duaration = document.querySelector("#backlog-board > div > table > tbody > tr > th #duaration").value;
	 		var diffuclty = document.querySelector("#backlog-board > div > table > tbody > tr > th #diffuclty").value;

	 		console.log(description+"  "+priority+"  "+duaration+"  "+diffuclty);
	 		console.log("in enterPressed");
	 		console.log("name : "+name);
	 		console.log("description : "+description);
	 		console.log("priority : "+priority);
	 		console.log("duaration : "+duaration);
	 		console.log("diffuclty : "+diffuclty);
	 		addBacklog(name,description,priority,duaration,diffuclty);
	 	}
    }
}

function addBacklog(name,description,priority,duaration,diffuclty)
{
	console.log("an add backlog java script");
	var data ={
				"name":name,
				"description":description,
				"priority":priority,
				"duaration":priority,
				"diffuclty":priority,
			};
	myAjax(method="POST",url="/add-backlog",data=data,
		response=function(response){
			if(data["return"]=="secuss")
				loadBacklog() ;	
			
		})

}

function removeAllChilds(target){
	while(target.firstChild){
		target.removeChild(target.firstChild);
	}
}

$("#backlog-board #sort tbody").sortable({
	stop: function( event, ui ) {

		var item_dragged = ui.item ;
		var tr = document.querySelectorAll("#backlog-board #table #sort > tbody > tr");
		var backlogIDs = document.querySelectorAll("[data-field=id]");
		var priority = document.querySelectorAll("[data-field=priority]");
		var i=1 ;
		console.log("printing list");
		
		for ( i ; i<tr.length+1;i++){
			priority[i].innerHTML=i;

		}
		var dragged_id = $(item_dragged["0"]).find("td")[0].id;
		var dragged_priority = $(item_dragged["0"]).find("td")[3].innerHTML;
		array = {"id":dragged_id,"priority":dragged_priority};

		myAjax(method="POST",url="/update-backlog-sorting",data=array,
		response=function(response){
			if(response=="success"){
				console.log("yes");
			}
			else{
				console.log("no");
			}
		});

	}
});

loadBacklog();
});
