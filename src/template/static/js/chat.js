/*
 * Created by : Ahmed taj
 */

$(document).ready(function(){

	var pending = [];

	var url = decodeURIComponent(location.href.split("/").splice(0,5).join("/"));
	url 	= url.replace('http','ws');
	url += '/chat';

	var container = $("#slide-menu .direct-chat-messages");
    var send_btn  = $("#slide-menu .box-footer button");
    var textfield = $("#slide-menu .box-footer input");

    var ws = new WebSocket(url);

	ws.onmessage = function (e){
		var data = JSON.parse(e.data);

		if(data['status']){ // local message
			appendLocalMessage(textfield.val());
			textfield.val('');
		}
		else{	// remote message
			appendRemoteMessage(data['author'],data['date'],data['message']);
		}
	};

	function appendRemoteMessage(author,date,value){
		var msg = $('<div class="direct-chat-msg ">'+
						'<div class="direct-chat-info clearfix">'+
							'<span class="direct-chat-name pull-left"></span>'+
							'<span class="direct-chat-timestamp pull-right"></span>'+
						'</div>'+
						'<img class="direct-chat-img" src="/static/img/default-profile.png" >'+
						'<div class="direct-chat-text">'+
						'</div>'+
					'</div>');
		msg.find('.direct-chat-name').html(author);
		msg.find('.direct-chat-timestamp').html(moment(date).format("DD MMM h:mm a"));
		msg.find('.direct-chat-text').html(value);

		if($(".direct-chat-msg").length != 0){
			container.append(msg);
		}else{
			container.html(msg);
		}
		container.scrollTop(container[0].scrollHeight);
	}

	function appendLocalMessage(message,date){
		date = date || moment()
		var msg = $('<div class="direct-chat-msg right">'+
						'<div class="direct-chat-info clearfix">'+
							'<span class="direct-chat-name pull-right">Me</span>'+
							'<span class="direct-chat-timestamp pull-left"></span>'+
						'</div>'+
						'<img class="direct-chat-img" src="/static/img/default-profile.png"  >'+
						'<div class="direct-chat-text">'+
						'</div>'+
					'</div>');
		msg.find('.direct-chat-timestamp').html(date.format("DD MMM h:mm a"));

        msg.find('.direct-chat-text').html(message);

		if($(".direct-chat-msg").length != 0){
			container.append(msg);
		}else{
			container.html(msg);
		}
		container.scrollTop(container[0].scrollHeight);
	}

	function send_message(){
		var msg = textfield.val();
		ws.send(JSON.stringify({
			type:'message',
			message:msg
		}));
	}

	function loadChat(){
		$.ajax({
			url: (url+'/load').replace('ws','http'),
			dataType: 'json',
			success: function( data ,textStatus, jQxhr ){

				for ( var i = 0; i < data.length; i++){
					if(data[i]['remote'] == true){
						appendRemoteMessage(data[i]['author'],
											data[i]['date'],
											data[i]['message']);
					}else{
						appendLocalMessage(data[i]['message'],moment(data[i]['date']));
					}
				}
			},
			error: function( jqXhr, textStatus, errorThrown ){
				console.log(jqXhr );
			}
		});
	}

	$("#setting-btn").on('click',function(){
		$('#slide-menu').toggleClass('open');
	});

	$("#app-container").on('mousedown',function(e){
		$('#slide-menu').removeClass('open');
	});

	send_btn.on('click',function(evt){
		send_message();
	});
	textfield.on('keypress',function(evt){
		if(evt.keyCode == 13){
			send_message();
		}
	});

	// load chat history
	loadChat();
});