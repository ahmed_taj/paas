/******************************************************************************
						DOM core function
******************************************************************************/
'use strict';

(function(window){
	var dom = function(){};
	dom.version = 'v0.1';

	dom.get = function(selector){
		return new dom.fn.init( selector );
	}

	dom.fn = dom.prototype = {
		init: function( selector ) {
			// null, undefined, ''
			if(! selector){
				return null;
			}
			else if(typeof selector == 'object'){
				this[0] = selector;
				this.length = 1;
			}
			else if(typeof selector == 'string'){
				this[0] = document.querySelector(selector);
				this.length = 1;
				if(! this[0]){
					return null;
				}
			}
			return this;
		},
		initAll: function( selector ) {
			// null, undefined, ''
			if(! selector){
				return null;
			}
			else if(typeof selector == 'string'){
				var nodes = document.querySelectorAll(selector);
				if(nodes){
					for ( var i = 0; i < nodes.length; i++){
						this[i] = nodes[i];
					}
					this.length = nodes.length;
				}
				else{
					return null;
				}
			}
			else if( typeof selector == 'object'){
				console.log('dom.getAll() doesn\'t support object as selector');
				return null;
			}
			return this;
		}
	}

	dom.fn.init.prototype = dom.fn;

	return (window.dom = dom);
})(window);

/******************************************************************************
						extended functions
******************************************************************************/
dom.fromdata= function (arr) {
	var param = [], regEx = /%20/g;
	for (var k in arr){
		param.push(encodeURIComponent(k).replace(regEx, "+") + "=" + encodeURIComponent(arr[k].toString()).replace(regEx, "+"));
	}
	return param.join('&');
};
dom.ajax = function(options){
	/*
		options:
			url,
			method,
			type,
			on_error,
			on_ok,
			data
	*/

	var xmlhttp = new XMLHttpRequest();
	var data = null, url = options.url;
	var method = options.method ? method=options.method : method='GET';

	//flags
	var isPost = options.method == 'POST' || options.method == 'PUT';
	var isJson = options.type == 'JSON'

	xmlhttp.onreadystatechange=function(){
		if(xmlhttp.readyState==4 ){
			if(xmlhttp.status < 300){
				// success
				if(! xmlhttp.responseText){
					return options.on_ok(xmlhttp.responseText);
				}
				else{
					switch(xmlhttp.getResponseHeader('Content-Type').split(";")[0]){
						case "text/json":
						case "application/json":
						case "text/javascript":
						case "application/javascript":
						case "application/x-javascript":{
							options.on_ok(JSON.parse(xmlhttp.responseText));
							break;
						}
						default:
							options.on_ok(xmlhttp.responseText);
					}
				}
				/*else if(isJson){
					options.on_ok(JSON.parse(xmlhttp.responseText));
				}
				else{
					options.on_ok(xmlhttp.responseText);
				}*/
			}
			else {
				// error
				if(options.on_error){
					return options.on_error(xmlhttp, xmlhttp.status, xmlhttp.statusText);
				}
			}
		}
	}

	//GET
	if (!isPost && options.data) url += "?" + dom.fromdata(options.data);

	xmlhttp.open(method,url,true);

	//POST
	if(isPost){
		console.log("is json"+isJson);
		data = isJson ? JSON.stringify(options.data) : dom.fromdata(options.data);
		xmlhttp.setRequestHeader("Content-Type",isJson ? "application/json":"application/x-www-form-urlencoded");
	}

	xmlhttp.send(data);
};

/******************************************************************************
						plugins
******************************************************************************/

// show an element
dom.fn.show = function(){
	for(var i = 0; i < this.length;i++){
		this[i].style.display= 'block';
	}
	return this;
};

// check if an element is shown
dom.fn.isShown = function(){
	var flag = true;
	for(var i = 0; i < this.length;i++){
		if(this[i].style.display == 'none'){
			flag = false;
			return flag;
		}
	}
	return flag;
};

// hide an element
dom.fn.hide = function(){
	for(var i = 0; i < this.length;i++){
		this[i].style.display= 'none';
	}
	return this;
};

// check if an element is hidden
dom.fn.isHidden = function(){
	return !(this.isShown());
};

// show/hide an element if it's hidden/shown
dom.fn.toggle = function(){
	for(var i = 0; i < this.length;i++){
		var el = dom.get(this[i]);
		if(el.isHidden()){
			el.show();
		}else{
			el.hide();
		}
	}
	return this;
};

// Events: add , remove
dom.fn.on = function(ev,fn){
	for(var i = 0; i < this.length;i++){
		this[i].addEventListener(ev,fn);
	}
	return this;
};
dom.fn.removeEvent = function(ev,fn){
	for(var i = 0; i < this.length;i++){
		this[i].removeEventListener(ev,fn);
	}
	return this;
};

