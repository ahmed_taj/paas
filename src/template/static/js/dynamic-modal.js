"use strict";
/**
	Authored by Ahmed Taj elsir.

    DynamicModal is used to create new Bootstrap modal dynamically with the given options.

    * Possible options are :
		{
			title: 'your html', // default: 'loading'
			body : 'your html', // default: html for loading spinner
			buttons: {
				submit: {
					text: 'your text', // default: 'Submit'
					event: function , // default: empty
				},
				close: {
					text: 'your text', // default: 'Close'
					event: function , // default: empty
				},
			},
			style: { // default: empty
				// any css attribute with its value
				// Ex: width: '100px'
			}
		}
	* Methods:
		- renderBody:
			accept a string that represent modal body eithar regular text or HTML,
			this method automatically enable submit button after rendering the target body.
			However renderBody can also accept HTMLNodeElements

	* Notes:
		- Options: buttons text, and event title (although title is placed under h3 tag) can be regular text or HTML.
		  However body option can be HTML,text,HTMLElement

		- By default submit button will be disabled, unless you passed body option in the constructor, this can be avoided by

		  passing space-string for example: {body:" "//this is not empty string}. However submit button will be enabled automatically if a new body is rendered.

		- All styles are applied to directly to the modal dialog inside style attribute. so you can't pass CSS classes.

		- submit, close button's event are fired immediatly when user click the corresponding button, then if the specified event's method
		  returnd False, the modal will not be closed, else the modal will close automaticllay even if you don't pass an event's method at all.


	- Regards -
*/
function DynamicModal(options){

    // force new initilization
    if( !(this instanceof DynamicModal)){return new DynamicModal(options)}

    var self = this;
    self.o = {};
    options = options || {};

    self.o.title = options.title || "Loading";
    self.disableSubmit = options.body ? false:true;

    self.o.body  = options.body || '<div class="loader">'+
    									'<svg class="circular">'+
    										'<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10">'+
    										'</circle>'+
    									'</svg>'+
    								'</div>';
    self.o.buttons = {
        submit:{
            text:"Submit",
            event:"",
            style:{},
        },
        close:{
            text:"Close",
            event: "",
            style: {},
        }
    };
    self.o.style = "";

	if(typeof options.buttons == "object"){
	    // copy buttons properties
		for (var key in self.o.buttons){
			for (var subkey in self.o.buttons[key]){
				if(options.buttons[key][subkey]){
					self.o.buttons[key][subkey] = options.buttons[key][subkey];
				}
			}
		}
	}
    // generate css from options.style
    if(typeof options.style == "object"){
	    for (var key in options.style){
	        self.o.style += key+": "+options.style[key]+";";
	    }
	}

    // static template
    var html  = '<div class="modal">';
        html +=     '<div class="modal-dialog" style="'+self.o.style+'">';
        html +=         '<div class="modal-content">';
        html +=             '<div class="modal-header">';
        html +=                 '<h4 class="modal-title">'+self.o.title+'</h4>';
        html +=             '</div>';
        html +=             '<div class="modal-body" style="min-height:150px;">';
        html +=             '</div>';
        html +=             '<div class="modal-footer"></div>';
        html +=         '</div>';
        html +=     '</div>';
        html += '</div>';
    // generate
    html = $(html);

	var b = html.find(".modal-body");
	if(typeof self.o.body == "string"){
		b.html(self.o.body);
	}else if (typeof self.o.body == "object"){
		b.append(self.o.body);
	}

    // top close button
    var btn = $('<button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
    btn.on("click",function(){
        self.onClick('close');
    });
    html.find(".modal-header").prepend(btn);

    // submit button
    var submitBtn = $('<button type="button" class="btn btn-primary">'+self.o.buttons.submit.text+'</button>');
    submitBtn.on("click",function(){
        self.onClick('submit');
    });
    submitBtn.css(self.o.buttons.submit.style);
    submitBtn.attr('disabled',self.disableSubmit);

    // close button
    var closeBtn  = $('<button type="button" class="btn btn-default">'+self.o.buttons.close.text+'</button>');
    closeBtn.on("click",function(){
        self.onClick('close');
    });
    closeBtn.css(self.o.buttons.close.style);

    html.find(".modal-footer").append(submitBtn).append(closeBtn);

    self.container = $("#dynamic-modal");

    if (self.container.length == 0){// not exists
        $(document.body).append($("<div id='dynamic-modal'></div>"));
        self.container = $("#dynamic-modal");
    }
    self.container.empty(); // insure container is clear
    $('.modal-backdrop').remove();// remove the overly div

    self.container.append(html);
    self.container.find(".modal").modal({
        backdrop: false,
        keyboard: false
    });
    return this;
}
DynamicModal.prototype = {
	onClick : function(btn){
		var r = true;
		if($.isFunction(this.o.buttons[btn].event)){
			r = this.o.buttons[btn].event();
		}
		if(r){
			this.close();
		}
    },
    close: function(){
		this.container.find(".modal.in").modal("hide");
	},
    renderBody: function(html){
		var b = this.container.find(".modal-body");
		if(typeof html == "string"){
			b.html(html);
		}else if (typeof html == "object"){
			b.empty();
			b.append(html);
		}else{
			return;// invalid body
		}
		this.container.find(".modal-footer .btn-primary").attr('disabled',false);
		return this;
	},
};