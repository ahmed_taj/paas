$(document).ready(function() {

	var url = location.href;
	if(!url.endsWith('/')){
		url += '/';
	}
	var editorTabs = $( "#editor-tabs" ).tabs();

	editorTabs.find( ".ui-tabs-nav" ).sortable({
		axis: "x",
		stop: function() {
			editorTabs.tabs( "refresh" );
		}
    });

	// open file in editor
    function openFile(el){
		if( ! this instanceof openFile){
			return new openFile(el);
		}
		var ul = editorTabs.find('ul');

		var fid = Math.random() * 100000000000000000;


		$("<li><a href='"+("#"+fid)+"'>"+el.fileName+"</a><button>x</button></li>").appendTo(ul);
		$("<div id='"+fid+"'><pre id='"+(fid+'-pre')+"'></pre></div>").appendTo(editorTabs);

		ul.find('li:last-child button').on('click',function(){
			console.log("close");
		});

		editorTabs.tabs('refresh');
		$( "#editor-tabs" ).tabs("option",'active',ul.find('li').length-1)

		var file_path = url+'raw'+( el.path == ""? "": '/'+el.path);

		file_path += '/'+el.fileName;


		ul.find('li:last-child').data('file_path',( el.path == ""? "": '/'+el.path)+"/"+el.fileName);
		ul.find('li:last-child').data('raw_path', file_path);

		// load file content
		$.ajax({
			url: file_path,
			success: function( res, textStatus, jQxhr ){
				console.log(res);
				var editor = ace.edit(fid+'-pre');
				//editor.setTheme("ace/theme/chrome");
				editor.insert(res);
				$("#"+fid).data('ace_editor',editor);
			},
			error: function( jqXhr, textStatus, errorThrown ){
				toastr["error"]("Something went wrong");
				console.log("error", errorThrown );
			}
		});
	}


	$.ajax({
		url: url+'tree',
		dataType: "json",
		success: function( res, textStatus, jQxhr ){
			tree(".editor-container .box-body:nth-child(2)",{
				root:"Sources",
				list: res,
				open : openFile
			});
		},
		error: function( jqXhr, textStatus, errorThrown ){
			console.log("error", errorThrown );
			toastr["error"]("Something went wrong");
		}
	});

	$(".menu-container button .ion-android-document").on('click',function(ev){
		// run code here
	});

	$(".menu-container button:nth-child(2)").on('click',function(ev){
		// get index of selected tab
		var index = $( "#editor-tabs" ).tabs('option','active')
		var tab = $($("#editor-tabs ul li")[index]);

		var url = tab.data('raw_path');

		var editor = $(tab.find('a').attr('href')).data('ace_editor');
		url = url.replace('raw','save');
		console.log(url);

		$.ajax({
			url: decodeURIComponent(url),
			type:'post',
			dataType: "json",
			data: JSON.stringify({"content":editor.getValue()}),
			success: function( res, textStatus, jQxhr ){
				if(res['status'] == 'ok'){
					toastr["success"]("Saved");
				}else{
					toastr["error"]('error while saving the file');
				}
			},
			error: function( jqXhr, textStatus, errorThrown ){
				toastr["error"]('error while saving the file');
			}
		});
	});

	$(".menu-container button:nth-child(3)").on('click',function(ev){
		// run code here
		// get index of selected tab
		var index = $( "#editor-tabs" ).tabs('option','active')
		var tab = $($( "#editor-tabs ul li" )[index]);

		var file_path = tab.data('file_path');
		console.log(file_path);
		if(connected==0){
		inti_request();
		connected=1;
		}
		else
		{
			console.log("in else");
			execute_command("python3 /app/"+file_path);
		}
		//TODO: for hammad to use
	});
});

var connected = 0;