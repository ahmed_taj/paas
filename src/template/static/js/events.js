$(document).ready(function() {

    var root = location.href;
    if(!root.endsWith('/')){
        root += '/';
    }

    /* initialize Event's remove button
    -----------------------------------------------------------------*/

    $( "#remove-event-btn" ).droppable({
        tolerance:"pointer",
        over: function(event, ui ){
            $(this).css('background-color','red');
        },
        drop: function( event, ui ) {
           console.log("an item is going to be removed");
        }
     });

    /* Function to detect is event over Trash
    -----------------------------------------------------------------*/
    var isEventOverTrash = function(x, y) {
        var external_events = $( '#remove-event-btn' );
        var offset = external_events.offset();
        offset.right = external_events.width() + offset.left;
        offset.bottom = external_events.height() + offset.top;

        // Compare
        if (x >= offset.left
            && y >= offset.top
            && x <= offset.right
            && y <= offset .bottom){
                return true;
            }
        return false;
    };

    /* initialize the external events
    -----------------------------------------------------------------*/

    $('#external-events .event-type').each(function() {
        // store event type
        $(this).data('eventType',$(this).attr('data-type'));

        // make the event draggable using jQuery UI
        $(this).draggable({
            zIndex: 1070,
            cursor: "pointer",
            tolerance:"pointer",
            helper:"clone",
            cursorAt:{top:10,right:0},
            scroll:true,
            scrollSensitivity :0,
            containment:"#events-container",
            revert: true,      // will cause the event to go back to its
            revertDuration: 0,  //  original position after the drag
            start: function(e, ui){
                $(ui.helper).css("width","150px");
            }
        });

    });


    var displayFormat = "hh:mm a, MMM Do YYYY";
    /* initialize the calendar
    -----------------------------------------------------------------*/

    $('#calendar').fullCalendar({
        header: {
            left: 'today',
            center: 'prev title next',
            right: 'month,agendaWeek,agendaDay'
        },
        events:decodeURIComponent(root+"events/json-feed"),
        editable: true,
        droppable: true, // this allows things to be dropped onto the calendar
        dragRevertDuration: 0, // this is for deleting event using trash


        /* Controlling event rendering
        -----------------------------------------------------------------*/
        eventRender: function(event, el) {
            if(event.type_id == 0){
                el.css({
                    'background-color':'#13BC6E',
                    'color':'white',
                    'border-color':'#13BC6E',
                });
            }else if(event.type_id == 1){
                el.css({
                    'background-color':'#0089FF',
                    'color':'white',
                    'border-color':'#0089FF',
                });
            }else if(event.type_id == 2){
                el.css({
                    'background-color':'#F39C12',
                    'color':'white',
                    'border-color':'#F39C12',
                });
            }else{
                el.css({
                    'background-color':'#F012BE',
                    'color':'white',
                    'border-color':'#F012BE',
                });
            }

            var msg =   "<div style='text-align:left;opacity: 1;'>"+
                            "Title:&nbsp;&nbsp;&nbsp;&nbsp;"+event.title+"</br>"+
                            "Type:&nbsp;&nbsp;&nbsp;"+event.type+"</br>"+
                            "Start:&nbsp;&nbsp;&nbsp;"+event.start.format(displayFormat)+"</br>";
            if(event.end){
            	msg += "End:&nbsp;&nbsp;&nbsp;&nbsp;"+event.end.format(displayFormat)+"</br>"
			}
            msg += "Author:&nbsp;"+event.author+"</div>";

            // add tooltip
            el.tooltip({
                html:true,
                placement:"bottom",
                title:msg ,
                delay:500
            });
        },

        /* Remove event using trach
        -----------------------------------------------------------------*/
        eventDragStop: function(event,jsEvent) {
            // if event not over the trash
            if(! isEventOverTrash(jsEvent.clientX, jsEvent.clientY)){
                return;
            }

            // temparary remove the event from the calendar
            $('#calendar').fullCalendar('removeEvents', event.id);

            /*   Prepare update url
            ---------------------------------------------------------*/
            var removeURL = decodeURIComponent(root+"remove-event");

            /*   Do Ajax
            ---------------------------------------------------------*/
            $.ajax({
                url: removeURL,
                dataType: 'json',
                type: 'post',
                data: JSON.stringify({id:event.id}),
                success: function( data, textStatus, jQxhr ){
                    if(data.status == "ok"){
                        toastr["success"]("Event removed");
                    }else{
                        toastr["error"]("Something went wrong");
                        $('#calendar').fullCalendar('renderEvent',event,true);
                    }
                },
                error: function( jqXhr, textStatus, errorThrown ){
                    console.log("error", errorThrown );
                    toastr["error"]("Something went wrong");
                    $('#calendar').fullCalendar('renderEvent',event,true);
                }
            });
        },

        /* Update event on click
        -----------------------------------------------------------------*/
        eventClick: function(event, jsEvent, view) {
            /*   Prepare update url
            ---------------------------------------------------------*/
            var updateURL = decodeURIComponent(root+"update-full-event");

            /*   Do Ajax
            ---------------------------------------------------------*/
            var form = $('<form action="" method="POST">'+
                            '<div class="form-group">'+
                                '<label >Title</label>'+
                                '<input type="text" class="form-control" name="title" placeholder="Title" value="'+event.title+'">'+
                            '</div>'+
                            '<div class="form-group">'+
                                '<label >Start at </label>'+
                                '<div class="input-group">'+
                                    '<input type="datetime" name="start-date" class="form-control" value="'+event.start.format(displayFormat)+'">'+
                                    '<span class="input-group-addon">'+
                                        '<span class="fa fa-calendar"></span>'+
                                    '</span>'+
                                '</div>'+
                            '</div>'+
                            '<div class="form-group">'+
                                '<label >End at </label>'+
                                '<div class="input-group">'+
                                    '<input type="datetime" name="end-date" class="form-control" value="'+event.end.format(displayFormat)+'">'+
                                    '<span class="input-group-addon">'+
                                        '<span class="fa fa-calendar"></span>'+
                                    '</span>'+
                                '</div>'+
                            '</div>'+
                            '<div class="form-group">'+
                                '<label >Description</label>'+
                                '<textarea  class="form-control" row="2" name="desc" placeholder="Description" >'+event.description+'</textarea>'+
                            '</div>'+
                         '</form>');

            /*   Modal & DateTimePickers
            ---------------------------------------------------------*/

            // start date picker
            form.find('[name="start-date"]').datetimepicker({
                 format:displayFormat
            });

            // end date picker
            form.find('[name="end-date"]').datetimepicker({
                 minDate: form.find('[name="start-date"]')
                          .data("DateTimePicker").date().format(),
                 format:displayFormat
            });

            // prevent selecting date before start date
            form.find('[name="start-date"]').on("dp.change", function (e) {
                form.find('[name="end-date"]').data("DateTimePicker").minDate(e.date.add(15,"m"))
            });

            /*   Update function
            ---------------------------------------------------------*/
            var update = function(){
                var title = form.find('[name="title"]');
                var start = form.find('[name="start-date"]');
                var end = form.find('[name="end-date"]');
                var desc = form.find('[name="desc"]');

                var data = {
                    id: event.id,
                    title: title.val(),
                    start_date:start.data("DateTimePicker").date().format(),
                    end_date: end.data("DateTimePicker").date().format(),
                    desc: desc.val(),
                };

                $.ajax({
                    url: updateURL,
                    dataType: 'json',
                    type: 'post',
                    data: JSON.stringify(data),
                    success: function( data, textStatus, jQxhr ){
                        if(data.status == "ok"){
                            // update details
                            event.title = title.val();
                            event.start = start.data("DateTimePicker").date().format();
                            event.end = end.data("DateTimePicker").date().format()
                            event.description = desc.val();

                            $('#calendar').fullCalendar('updateEvent',event);
                            toastr["success"]("Event updated");
                        }else{
                            toastr["error"]("Something went wrong");
                        }
                    },
                    error: function( jqXhr, textStatus, errorThrown ){
                        console.log("error", errorThrown );
                        toastr["error"]("Something went wrong");
                    }
                });
                return true;
            };
            var modal = DynamicModal({
                    title: "Details",
                    body: form,
                    buttons:{
                        submit:{text:'Update',event:update},
                        close: {text:'Close'}
                    },
                    style:{
                        width:'350px',
                    }
                });
        },

        /* Update event dates on draggin between cells
        -----------------------------------------------------------------*/
        eventDrop: function( event, delta, revertFunc, jsEvent, ui, view ){

            /*   Prepare update url
            ---------------------------------------------------------*/
            var updateURL = decodeURIComponent(root+"update-event");

            /*   Prepare data
            ---------------------------------------------------------*/
            var data = {
                id : event.id,
                start_date : event.start.format(),
                end_date : event.end.format()
            };

            /*   Do Ajax
            ---------------------------------------------------------*/

            $.ajax({
                url: updateURL,
                dataType: 'json',
                type: 'post',
                data: JSON.stringify(data),
                success: function( data, textStatus, jQxhr ){
                    if(data.status == "ok"){
                        toastr["success"]("Event updated");
                    }else{
                        toastr["error"]("Something went wrong");
                        revertFunc();
                    }
                },
                error: function( jqXhr, textStatus, errorThrown ){
                    console.log("error", errorThrown );
                    toastr["error"]("Something went wrong");
                    revertFunc();
                }
            });
        },

        /* Add event when dragged to the calendar
        -----------------------------------------------------------------*/
        drop: function(date) {
            var self = this;

            /*   Prepare submit url
            ---------------------------------------------------------*/

            var submitURL = decodeURIComponent(root+"add-event");

            /*   Form template
            ---------------------------------------------------------*/

            var form = $('<form action="'+submitURL+'" method="POST">'+
                            '<div class="form-group">'+
                                '<label >Title</label>'+
                                '<input type="text" class="form-control" name="title">'+
                            '</div>'+
                            '<div class="form-group">'+
                                '<label >Start at</label>'+
                                '<div class="input-group">'+
                                    '<input type="datetime" name="start-date" class="form-control" value="'+date.format(displayFormat)+'">'+
                                    '<span class="input-group-addon">'+
                                        '<span class="fa fa-calendar"></span>'+
                                    '</span>'+
                                '</div>'+
                            '</div>'+
                            '<div class="form-group">'+
                                '<label >End at</label>'+
                                '<div class="input-group">'+
                                    '<input type="datetime" name="end-date" class="form-control" value="'+date.add(15,'m').format(displayFormat)+'">'+
                                    '<span class="input-group-addon">'+
                                        '<span class="fa fa-calendar"></span>'+
                                    '</span>'+
                                '</div>'+
                            '</div>'+
                            '<div class="form-group">'+
                                '<label >Description</label>'+
                                '<textarea  class="form-control" row="2" name="desc"></textarea>'+
                            '</div>'+
                         '</form>');

            /*   Submit form function
            ---------------------------------------------------------*/

            var onsubmit = function(){
                var title = form.find('[name="title"]');
                var start = form.find('[name="start-date"]');
                var end = form.find('[name="end-date"]');
                var desc = form.find('[name="desc"]');
                var type = $(self).data("eventType");

                var data = {
                    'title':title.val(),
                    'type': type,
                    'desc':desc.val(),
                    'start_date':start.data("DateTimePicker").date().format(),
                    'end_date':end.data("DateTimePicker").date().format()
                };

                $.ajax({
                    url: submitURL,
                    dataType: 'json',
                    type: 'post',
                    data: JSON.stringify(data),
                    success: function( data, textStatus, jQxhr ){
                        if(data.status == "ok"){
                            // event object
                            var eventObj = {};
                            eventObj.id = data.event['id'];
                            eventObj.title = data.event['title'];
                            eventObj.type = data.event['type'];
                            eventObj.type_id = data.event['type_id'];
                            eventObj.description = data.event['desc'];
                            eventObj.author = data.event['author'];
                            eventObj.start = data.event['start'];
                            eventObj.end = data.event['end'];
                            eventObj.creation_date = data.event['creation_date'];

                            $('#calendar').fullCalendar('renderEvent',eventObj,true);

                            toastr["success"]("Event added");
                        }else{
                            toastr["error"](data['message']);
                        }


                    },
                    error: function( jqXhr, textStatus, errorThrown ){
                        console.log("error", errorThrown );
                        toastr["error"](errorThrown);
                    }
                });
                return true;
            };

            form.on('submit',function(){
                    onsubmit();
                    return false;
            });

            /*   Modal & DateTimePickers
            ---------------------------------------------------------*/

            // start date picker
            form.find('[name="start-date"]').datetimepicker({
                 minDate:date.format(),
                 format:displayFormat
            });

            // end date picker
            form.find('[name="end-date"]').datetimepicker({
                 minDate:date.add(15,'m').format(),
                 format:displayFormat
            });

            // prevent selecting date before start date
            form.find('[name="start-date"]').on("dp.change", function (e) {
                form.find('[name="end-date"]').data("DateTimePicker").minDate(e.date.add(15,"m"))
            });
            var modal = DynamicModal({
                    title: "New Event",
                    body: form,
                    buttons:{
                        submit:{text:'Add',event:onsubmit},
                        close: {text:'Cancel'}
                    },
                    style:{
                        width:'350px',
                    }
                });
        },
    });

});