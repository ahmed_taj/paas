function temp_proj(){
	var temp = prompt("project name");
	var data={
	    "_name":temp ,
	    "_company_id":  "1",
	    "_description": "this is temperory",
	}
	myAjax(method="POST",url="/create-project",data=data,
		response=function(data){
			if(data["status"] =="ok"){
				alert("ok");
			}
			else{
				alert("something went wrong");
			}
		});
}

function openfile(file_name){
	var file_name = file_name.innerHTML.trim();

	var test = document.getElementById('file-'+file_name+'-id');
	if(test){
		$(test).click();
		return;
	}
	else{
		var comp = document.getElementById('company').innerHTML.trim();
		var proj = document.getElementById('project').innerHTML.trim();

		var path = '/'+comp+'/'+proj+'/'+'ahmed/raw/'+file_name

		dom.ajax({
			url: path,
			method: "GET",
			on_ok : function(file){
				var container = document.getElementById('text-editor-tabs');
				var editor_tab_links = container.querySelector(".tab-links");
				var editor_tab_content = container.querySelector(".tab-content");

				var file_link = document.createElement('li');
				file_link.dataset.toggle= ('file-'+file_name);
				file_link.id= ('file-'+file_name+'-id');
				file_link.innerHTML = file_name;
				editor_tab_links.appendChild(file_link);

				var file_content = document.createElement('div');
				file_content.id = 'file-'+file_name;
				var pre = document.createElement('pre');
				pre.id = 'file-'+file_name+'-editor';
				file_content.appendChild(pre);

				editor_tab_content.appendChild(file_content);

				createTab(container);
				//fire click event
				$(file_link).click();


				var editor = ace.edit(pre.id);
				editor.setTheme("ace/theme/twilight");
				editor.getSession().setMode("ace/mode/javascript");
				editor.insert(file);
				/*editor.commands.addCommand({
				    name: 'Save',
				    bindKey: {win: 'Ctrl-S',  mac: 'Command-S'},
				    exec: function(editor) {
				        saveDocument();
				    },
				    readOnly: false
				});*/

			},
			on_error: function(obj, status, text){
				alert("can't open file");
			}
		});
	}
}
function saveDocument(){
	// get current open
	var current_id = document.querySelector('#text-editor-tabs .tab-links').dataset.current;
	var container = document.getElementById(current_id);
	var editor = ace.edit(container.querySelector("pre").id);
	current_id = current_id.substring(5,current_id.length);

	//FIX for static path
	var comp = document.getElementById('company').innerHTML.trim();
	var proj = document.getElementById('project').innerHTML.trim();

	var file_url = '/'+comp+'/'+proj+'/'+'ahmed/save/'+current_id
	dom.ajax({
		url: file_url,
		method: "POST",
		type: "JSON",
		data:{
			'content':editor.getValue()
		},
		on_ok : function(text){
			alert("file saved");
		},
		on_error: function(obj, status, text){
			alert("can't save file");
		}
	});
}