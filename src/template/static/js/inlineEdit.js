function inlineEdit(inputs,func){
    if (!this instanceof inlineEdit){
        return new inlineEdit(inputs);
    }

    $.each(inputs, function( index, value ) {
        $(value).on('dblclick',function(evt){
            var el = $(evt.target)
            var old =el.html();

            var txtf = $("<input class='form-control input-sm' type='text'/>");
            txtf.on('blur',function(evt){
                func(el,txtf.val());
                el.html(old);
            });
            txtf.on('keypress',function(evt){
                if(evt.keyCode == 13){
                    func(el,txtf.val());
                    el.html(old);
                }
            });
            el.html(txtf);
            txtf.focus();
        });
    });
}

// example 
//inlineEdit($("#test tbody tr"),function(el,val){
//    console.log(el,val);
//});