function createModals(){
	var model_buttons = document.querySelectorAll(".modal-button");

	for ( i = 0;i <model_buttons.length; i++){
		model_buttons[i].addEventListener("click",function(e){
				if( e.target !== this ){
       				return;
				}
				var modal = document.getElementById(this.dataset.modal);
				var to_call = this.dataset.call;
				if(to_call){
					window[to_call](this);// call pre-execute function
				}
				modal.style.visibility = "hidden";
				modal.style.display="block";
				var width = modal.offsetWidth;
				var height = modal.offsetHeight;
				modal.style.removeProperty("visibility");
				width = width/2;
				height = height/2;
				modal.style.marginLeft= -width+"px";
				modal.style.marginTop= -height+"px";
			});
	}
	var modal_close_buttons = document.querySelectorAll(".modal-close");
	for ( i = 0;i <modal_close_buttons.length; i++){
		modal_close_buttons[i].addEventListener("click",function(evt){
				findParentBySelector(this,".modal").style.display="none";
			});
	}
}