 search = 0;
toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
    }

function getProjectData(id)
{
    console.log(id);


    myAjax(method= "POST", url='/getProjectData' , data ={"name":id },

                response = function(data){
                    console.log("in responce from project data");
                    console.log(data);

                    var div = document.querySelector(" #projects-container .project-name") ;
                    div.innerHTML = data["name"];
                    div.id = id;
                    console.log(data);
                    div.href=data["company"]+"/"+data["name"];
                    document.querySelector("#projects-container .projectManager").innerHTML = data["manager"];


                    var i = 0 ;
                    var ul = document.querySelector(".left-side-details ul ");
                    if(document.querySelector(".left-side-details ul li") != null)
                        removeAllChilds(ul);

                    for (i ; i<Object.keys(data["member"]).length ; i++)
                    {
                        var para = document.createElement("li");
                        var innerhtml = "<span>"+data["member"][i]+"</span>";
                        para.innerHTML = innerhtml;

                        ul.appendChild(para);
                    }
                        drawChart(data["sprint_id"]);
                });
}


function drawChart(sprint){

data = {"sprint_id":sprint};

myAjax(method="POST",url="/get-chart",data,response=function(response){

        var ctx = document.getElementById("char-canvas").getContext("2d");
        var lineChartData = {
                        labels :response[0],
                        datasets : [
                                {
                                        label: "My First dataset",
                                        fillColor : "rgba(220,220,220,0.2)",
                                        strokeColor : "rgba(220,220,220,1)",
                                        pointColor : "rgba(220,220,220,1)",
                                        pointStrokeColor : "#fff",
                                        pointHighlightFill : "#fff",
                                        pointHighlightStroke : "rgba(220,220,220,1)",
                                        data : response[1] //data to be represented
                                }
                        ]

                }

                window.myLine = new Chart(ctx).Line(lineChartData,{scaleLabel: "<%=value%>",});

});


}

function projectSearch(object)
{
        console.log("projectSearch"+object.id);
        var input = document.getElementById(object.id);
        console.log(" text value input : "+input);
        console.log(" text value  : "+input.value);
        var text = input.value.toLocaleLowerCase() ;
        console.log(" text value after loweer : "+text);

        var li = document.querySelectorAll("#projects-container #project-selector ul li span");
        if(li != null)
        {
                console.log("in if ");
                for ( var i = 0; i < li.length;i++){
                if(li[i].innerHTML.trim().toLocaleLowerCase().indexOf(text)>-1){
                        li[i].parentNode.style.display="block";
                        console.log("shown a span");
                        }
                else{
                        li[i].parentNode.style.display="none";
                        console.log("hidden a span");
                }
                console.log(li[i].innerHTML);
                }
        }
        else
                console.log("empty");

}

function userSearch(object)
{
        console.log("projectSearch"+object.id);
        var input = document.getElementById(object.id);
        console.log(" text value input : "+input);
        console.log(" text value  : "+input.value);
        var text = input.value.toLocaleLowerCase() ;
        console.log(" text value after loweer : "+text);

        var li = document.querySelectorAll("#projects-container .left-side-details ul li span");
        if(li != null)
        {
                console.log("in if ");
                for ( var i = 0; i < li.length;i++){
                if(li[i].innerHTML.trim().toLocaleLowerCase().indexOf(text)>-1){
                        li[i].parentNode.style.display="block";
                        console.log("shown a span");
                        }
                else{
                        li[i].parentNode.style.display="none";
                        console.log("hidden a span");
                }
                console.log(li[i].innerHTML);
                }
        }
        else
                console.log("empty");

}
function returnUsersData()
{
        console.log("in user data");

}

/*function checkUser(object)
{
    //alert("in invite user js ");
    var users_tag = document.querySelector("#inviteUserModel .modal-content .users ul");

    var input = document.getElementById(object.id);
    var text = input.value.toLocaleLowerCase();

    var data = {
            "project_id":document.querySelector("#projects-container .project-name").id,
            "text":text
    };
    //alert("ajax will called ");
    myAjax(method= "POST", url="/userSearch" , data =data,
    response = function(data){

        removeAllChilds(users_tag);

        var result = data["return"];
        console.log("this is the result "+result);
        for(var i = 0 ; i<Object.keys(result).length ; i++)
        {
            var id = result[i]['email'] ;

            var para = document.createElement("li");
            para.setAttribute("onclick","addToList(this)");
            para.id = id ;

            var innerhtml = "<span>"+result[i]["name"]+"   "+result[i]["email"]+"</span>";

            var select = document.createElement('select');
            select.innerHTML = "<option value='developer'>"+'developer'+"</option>\
                                <option value='tester'>"+'tester'+"</option>\
                                <option value='leader'>"+'leader'+"</option> ";

            para.innerHTML = innerhtml;
            para.appendChild(select);

            var br = document.createElement('br');
            para.appendChild(br);

            users_tag.appendChild(para) ;

        }
    });
    console.log(text);


}

function addToList(id)
{
    alert("li cliked ");
    //var select =document.querySelector("#inviteUserModel .modal-content .users ul li select");
    //var check = document.querySelector("#inviteUserModel .modal-content .users ul li ");
    //alert();
}


*/

function updateUsers(object)
{
    var data = {"id":1};
    if(search == 0 ){
        search = 1;
    myAjax(method="POST",url="/userSearch",data=data,
                response=function(data){
                    var input = document.getElementById("invite-user-input");
                    var awesomplete =
                    new Awesomplete(input, {
                    filter: function(text, input) {
                        return Awesomplete.FILTER_CONTAINS(text, input.match(/[^,]*$/)[0]);
                    },

                    replace: function(text) {
                        var before = this.input.value.match(/^.+,\s*|/)[0];
                        this.input.value = before + text + ", ";
                    }
                    });
                    awesomplete.list = data;
                });
    };
}

function inviteUser(){
    var email = document.getElementById("invite-user-input").value;
    var id = document.querySelector(" #projects-container .project-name").id ;
    var data = {"project_id":id,"user":email};
   myAjax(method="POST",url="/invite-user",data=data,
                response=function(data){
                console.log(data['status']);
                    if(data['status']=='ok'){
                toastr["success"]("hammad", "hammad")
                }
                else{
                toastr["success"]("hammad", "hammad")
                }

                })
}



function getCompanies()
{
    var dat = {}; 

        myAjax(method="POST",url="/get-user-companies",data=data,
                response=function(data){
                    var input = document.getElementById("_company_id");
                    var awesomplete = new Awesomplete(input);
                    awesomplete.list = data;
                    console.log(data);
                });
}

function updateProject()
{
        console.log(document.querySelector("#projects-container .project-name").id);
        var data={
            "_id":document.querySelector("#projects-container .project-name").id,
            "_name": document.querySelector('#projectId').value,
            "_company_id":  document.querySelector('#company_id').value,
            "_manager":  document.querySelector('#Manager').value,
            "_description": document.querySelector('#description').value,
            "_start_date" : document.querySelector('#start').value,
            "_end_date" : document.querySelector('#end').value,
        }
        console.log("update project will send arequest ");
        myAjax(method="POST",url="/update-project",data=data,
                response=function(data){
                        alert(data["status"])
                });
}

function deleteProject(id)
{


    var x = confirm("Are you sure you want to delete this?");
    if(x){
        myAjax(method="POST",url='/delete-project',data = {"id":id},
            response = function(data){
                if(data['status']=='ok'){
                toastr["success"](data["name"], "Deleted!");
                }
                else{
                toastr["error"]("Project Not Deleted!", "Failed");
                }
            }

            )
    }

}

function updateProjectModal()
{
    console.log("in update project modal ");
    var div = document.createElement('div');
    div.id="update-project-data"; 

    var name_span = document.createElement('span');
    name_span.class = "form-control input-sm"; 
    name_span.innerHTML ="Project Name : ";    

    var name_input = document.createElement('input');
    name_input.class="form-control input-sm" ; 
    name_input.id ="projectId" ; 

    var manager_span = document.createElement('span');
    manager_span.class = "form-control input-sm"; 
    manager_span.innerHTML = "Manager : ";

    var manager_input = document.createElement('select');
    manager_input.class = "form-control input-sm" ; 
    manager_input.id = "Manager";

    var compnay_span = document.createElement('span');
    compnay_span.class = "form-control input-sm"; 
    compnay_span.innerHTML="Company : ";

    var compnay_input = document.createElement('input');
    compnay_input.class = "form-control input-sm" ;
    compnay_input.id = "company_id";
    compnay_input.disabled = true; 

    var description_span = document.createElement('span');
    description_span.class = "form-control input-sm"; 
    description_span.innerHTML = "Descriptioin : " ;

    var description_input = document.createElement('textarea');
    description_input.class = "form-control input-sm" ; 
    description_input.id = "description" ;

    var start_span = document.createElement('span');
    start_span.class = "form-control input-sm" ; 
    start_span.innerHTML ="Start Date : " ;

    var start_input = document.createElement('input');
    start_input.class = "form-control input-sm"; 
    start_input.id = "start"; 

    var end_span = document.createElement('span');
    end_span.class =  "form-control input-sm" ; 
    end_span.innerHTML = "End Date : "; 

    var end_input = document.createElement("input");
    end_input.class =  "form-control input-sm" ; 
    end_input.id  = "end" ; 

    var id = document.querySelector(".project-name").id;
    console.log("the id is : "+id);

    myAjax(method= "POST", url='/getProjectData', data ={"name":id },
        response = function(data){
        console.log(data);
        name_input.value = data["name"]; 
        description_input.value = data["description"];   
        manager_input.value = data["manager"];
        compnay_input.value = data["company"];
        start_input.value = data["startDate"];
        end_input.value = data["endDate"];
        var option = document.createElement("option");
        option.value = data["manager"]
        option.innerHTML=data["manager"]
        manager_input.appendChild(option);

        /*
        var col_list = data["columns"];
                        for (var i = 0 ; i< col_list.length ; i++){
                            var option = document.createElement("option");
                            option.value = col_list[i]["id"];
                            option.innerHTML = col_list[i]["name"];
                            status_option.appendChild(option);
                        }

        */

        var members_list = data["member"]
        console.log(Object.keys(data["member"]).length);
        for(var i =0 ; i<Object.keys(data["member"]).length ; i++)
        {
            var opt = document.createElement('option');
            opt.value = members_list[i];
            opt.innerHTML = members_list[i] ; 
            manager_input.appendChild(opt);
        } 

        div.appendChild(name_span);
        div.appendChild(name_input);
        div.appendChild(manager_span);
        div.appendChild(manager_input);
        div.appendChild(compnay_span);
        div.appendChild(compnay_input);
        div.appendChild(description_span); 
        div.appendChild(description_input);
        div.appendChild(start_span);
        div.appendChild(start_input);
        div.appendChild(end_span);
        div.appendChild(end_input);

        var form = $(div);
        var d = DynamicModal({
            tilte:"Update Project",
            body:form,
            buttons:{
                submit: {
                    text: 'Add', // default: 'Submit'
                    event: updateProject , // default: empty
                },
                close: {
                    text: 'Close', // default: 'Close'
                    event : function(){
                        d.close() ; 
                    }
                },
            }
        });

    
        });
}

function addProjectModal()
{
    var div = document.createElement('div');
    div.id = 'createProject';

    var project_name = document.createElement('input');
    project_name.placeholder="Name : "; 
    project_name.id="_name" ;

    var project_company = document.createElement('select');
    project_company.id="_company_id"; 
    project_company.placeholder="Company : "; 

    var project_description = document.createElement('textarea');
    project_description.placeholder = "Description : ";
    project_description.id="_description";

    myAjax(method= "POST", url='/get-user-companies', data={},
        response = function(data){
            console.log("in user companies ");
            console.log(data.length);
            for (var i =0 ; i<data.length ;i++)
            {
                var option = document.createElement("option");
                option.value = data[i];
                option.innerHTML= data[i];
                project_company.appendChild(option);

                var form = $(div);
                var d = DynamicModal({
                    tilte:"Add Project",
                    body:form,
                    buttons:{
                        submit: {
                            text: 'Add', // default: 'Submit'
                            event: createNewProject , // default: empty
                        },
                        close: {
                            text: 'Close', // default: 'Close'
                            event : function(){
                                d.close() ; 
                            }
                        },
                    }
                });

                div.appendChild(project_name);
                div.appendChild(project_company);
                div.appendChild(project_description);
            }
        });


}

function createNewProject(){
    var project_name = document.querySelector('#_name').value;
    console.log("in create project "+project_name);
    var data={
        "_name":project_name ,
        "_company_id":  document.querySelector('#_company_id').value,
        "_description": document.querySelector('#_description').value,
    }

    var u = location.href ;
    if(! u.endsWith("/")){
        u +='/';
    }
    u += '';
    u = decodeURIComponent(u);

    myAjax(method="POST",url="/create-project",data=data,
        response=function(data){
            if(data["status"] =="ok"){
                document.getElementById("create_project_modal").style.display="none";
                var container = document.querySelector("#overview-container #project-table");
                var new_project = document.createElement("div");
                new_project.className = "project";
                new_project.innerHTML = '<div class="logo">'+
                                            '<a href="'+data['company_name']+'/'+project_name+'"><img src=""></a>'+
                                        '</div>'+
                                        '<div class="details">'+
                                            '<a href="'+data['company_name']+'/'+project_name+'">'+project_name+'</a>'
                                            '<span class="time">'+data['last_activity']+'</span>'+
                                            '<span class="fa fa-close close"></span>'+
                                        '</div>';
                container.insertBefore(new_project,container.childNodes[0]);
            }
            else{
                alert("something went wrong");
            }
        });
}


