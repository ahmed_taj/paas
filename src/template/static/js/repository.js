$(document).ready(function(){
	// this is for tabs in repository's tab
	$("#repository-tabs").tabs({
		activate: function( event, ui ) {
			var index = ui.newTab.index();

			if(index == 0){ // source tab
				$("#commit-viewer").hide();
				$("#file-viewer").show();
			}
			else if(index ==1){ // commits tab
				$("#file-viewer").hide();
				$("#commit-viewer").show();
			}
			else{ // branches or tags
				$("#file-viewer").hide();
				$("#commit-viewer").hide();
			}
		}
	});



	/*	Source table
	-------------------------------------------------*/

	var sourceTable = $("#source-table").DataTable({
		"paging":   false,
		"info":     false,
		"ordering": false,
		"filter":false,
		 columns: [
		    { data:"name"},
		    { data:"size"},
  		],
		"language": {
			"emptyTable": "This repository is empty ;)",
			"processing": "Loading source tree ..."
		},
		"processing": true,

		"ajax": function (data, callback, settings) {
			var url = location.href;
			url += url.endsWith('/') ? 'tree':'/tree';
			$.ajax({
				url: url,
				success: function( data, textStatus, jQxhr ){
					var table = JSON.parse(data);
					callback(table);
				},
				error: function(jqXhr, textStatus, errorThrown){
					console.log("error",textStatus);
				}
			});
		},

		"rowCallback": function( row, data, index ) {
			var a = $("<a>");
			var link = location.href;
			link += link.endsWith('/') ? data['url']:'/'+data['url'];
			// tree
			if (data['type'] == 'tree' || data['type'] == '..'){
				$('td:eq(0)',row).html($("<span class='ion-android-folder'></span>"));
				a.on('click',function(ev){
					sourceTable.ajax.url(link).load();
					return false;
				});
			}
			// blob
			else {
				$('td:eq(0)',row).html($("<span class='ion-document'></span>"));
				a.on('click',function(ev){
					$.ajax({
						url:$(this).attr('href'),
						success: function(response, callback, settings) {
							response = JSON.parse(response);
							$("#file-viewer").find(".box-title").text(response["name"]);
							$("#file-viewer").find(".box-body").html($("<pre>"));
							$("#file-viewer").find(".box-body pre").html(response['content']);

							// highlight code
							hljs.highlightBlock(document.querySelector("#file-viewer pre"));
						},
						error: function(jqXhr, textStatus, errorThrown){
							console.log("error",textStatus);
						}
					});
					return false;
				});
			}

			a.attr('href', link);
			a.html(data['name']);

			// name
			$('td:eq(0)',row).addClass('table-item');
			$('td:eq(0)',row).append(a);

			// size
			$('td:eq(1)', row).html(data['size']);

		}
	});

	/*	Commits table
	-------------------------------------------------*/

	var commitsTable = $("#commits-table").DataTable({
		"paging":   true,
		"info":     false,
		"ordering": false,
		"search": false,
		 columns: [
		    { data:"author"},
		    { data:"commit"},
		    { data:"message" },
		    { data: "date" },
  		],
		"language": {
			"emptyTable": "This repository is empty ;)",
			"processing": "Loading..."
		},
		"processing": true,

		"ajax": function (data, callback, settings) {
			var url = location.href;
			url += url.endsWith('/') ? 'commits':'/commits';
			$.ajax({
				url: url,
				success: function( data, textStatus, jQxhr ){
					var table = JSON.parse(data);
					for ( var r in table['data']){
						var m = moment(table['data'][r]['date']);
						table['data'][r]['date'] = m.fromNow();
					}
					callback(table);
				},
				error: function(jqXhr, textStatus, errorThrown){
					console.log("error",textStatus);
				}
			});
  		},

		"rowCallback": function( row, data, index ) {
			var url = location.href;
			url += url.endsWith('/') ? 'commits':'/commits';

			var a = $("<a href='"+url+"/"+data['commit']+"' >"+
						data['commit'].substring(0,7)+"</a>");

			// handle diff
			a.on('click',function(evt){
				$.ajax({
					url:$(this).attr('href'),
					success: function(content, callback, settings) {
						console.log("diff received");
						setTimeout(function() {
							var diffJson = Diff2Html.getJsonFromDiff(content);

							// collect all the file extensions in the json
							var allFileLanguages = diffJson.map(function (line) {
								return line.language;
							});
							// remove duplicated languages
							var distinctLanguages = allFileLanguages.filter(function (v, i) {
								return allFileLanguages.indexOf(v) == i;
							});

							// pass the languages to the highlightjs plugin
							hljs.configure({languages: distinctLanguages});

							var diff = Diff2Html.getPrettyHtmlFromJson(diffJson);
							$("#commit-viewer .box-body").html(diff);
							hljs.highlightBlock(document.querySelector("#commit-viewer .box-body"));
						}, 0);

					},
					error: function(jqXhr, textStatus, errorThrown){
						console.log("error",textStatus);
					}
				});

				return false;
			});

			// author
			$('td:eq(0)',row).css('width','120px');

			// commit
			$('td:eq(1)',row).css({'width':'70px','color':'blue'});
			$('td:eq(1)', row).html(a);

			// message
			$('td:eq(2)',row).css('white-space','pre-line');

			// date
			$('td:eq(3)',row).css('width','155px');


		}
	});

	/*	Branches table
	-------------------------------------------------*/

	$("#branches-table").DataTable({
		"paging":   false,
		"info":     false,
		"ordering": false,
		"filter":true,
		 columns: [
		    { data:"name"},
		    { data:"number_of_commits"},
  		],
		"ajax": function (data, callback, settings) {
			var url = location.href;
			url += url.endsWith('/') ? 'branches':'/branches';
			$.ajax({
				url: url,
				success: function( data, textStatus, jQxhr ){
					var table = JSON.parse(data);
					callback(table);
				},
				error: function(jqXhr, textStatus, errorThrown){
					console.log("error",textStatus);
				}
			});
  		},
	});
	/*	Tags table
	-------------------------------------------------*/

	$("#tags-table").DataTable({
		"paging":   false,
		"info":     false,
		"ordering": false,
		"filter":false,
		 columns: [
		    { data:"name"},
  		],
		"ajax": function (data, callback, settings) {
			var url = location.href;
			url += url.endsWith('/') ? 'tags':'/tags';
			$.ajax({
				url: url,
				success: function( data, textStatus, jQxhr ){
					var table = JSON.parse(data);
					callback(table);
				},
				error: function(jqXhr, textStatus, errorThrown){
					console.log("error",textStatus);
				}
			});
  		},
	});
});
