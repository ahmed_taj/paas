var signin_form = document.querySelector("#signin-form");
var signup_form = document.querySelector("#signup-form");

if(signin_form){
    signin_form.onsubmit = signin;
}
else if(signup_form){
    signup_form.onsubmit = signup;
}

function signin(){
    var err_msg = document.querySelector(".error-msg > div p");

    var _username = document.getElementById("_username_");
    var _password = document.getElementById("_password_");

    var flag = true;
    // dummy test for empty values
    if(_username.value.trim() === ""){
        flag = false;
        _username.classList.add("error");
    }else{
        _username.classList.remove("error");
    }
    if(_password.value === ""){
        flag = false;
        _password.classList.add("error");
    }else{
        _password.classList.remove("error");
    }

    // if there is an error
    if(!flag){
        err_msg.innerHTML = "Please fill all fields";
        err_msg.style.display="block";
    }else{
        var data={
                "_username":_username.value.trim(),
                "_password":_password.value
        };
        myAjax(
                method="POST",
                url="/signin",
                data=data,
                response=function(data){
                    if(data["status"] != "ok"){
                        err_msg.innerHTML = "Invalid username or password!";
                        err_msg.style.display="block";
                    }
                    else{
                        err_msg.style.display="none";
                        window.location.href = "/";
                    }
                });
    }
    return false;
};

function signup(){
    var _username = document.getElementById("_username_");
    var _password = document.getElementById("_password_");
    var _actualName = document.getElementById("_fullname_");
    var _email = document.getElementById("_email_");
    var _companyName = document.getElementById("_companyname_");
    var _phone = document.getElementById("_phone_");
    var _timezone = document.getElementById("_timezone_");

    var data = {
        "_username":_username.value,
        "_password":_password.value,
        "_actualName":_actualName.value,
        "_email":_email.value,
        "_companyName":_companyName.value,
        "_phone":_phone.value,
        "_timezone":_timezone.value
    };

    myAjax(
        method="POST",
        url="/signup",
        data=data,
        response=function (data){
            if(data["status"] != "ok"){
                //TODO: find a javascript library to show suitable error message
                _username.classList.add("error");
                _password.classList.add("error");
                _actualName .classList.add("error");
                _email.classList.add("error");
                _companyName.classList.add("error");
                _phone.classList.add("error");
            }
            else{
                _username.classList.remove("error");
                _password.classList.remove("error");
                _actualName .classList.remove("error");
                _email.classList.remove("error");
                _companyName.classList.remove("error");
                _phone.classList.remove("error");
                window.location.href = "/";
            }
        }
        );

    return false;
}