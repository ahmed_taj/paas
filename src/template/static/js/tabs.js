/*===========================================================================*/
/*                        Header tab links                                   */
/*===========================================================================*/


function createTab(container) {
    //hide others tab contents
    var pages = container.querySelectorAll(".tab-content > div");
    for (var i = 1; i < pages.length; i++) {
      pages[i].style.display="none";
    }

    //this adds click event to tabs
    var tabs_ul = container.querySelector("ul");
    var tabs = tabs_ul.querySelectorAll("li");

    for (var i = 0; i < tabs.length; i++) {
      tabs[i].addEventListener("click",function displayTab(){
		  	var curr_mark = container.querySelector("ul").dataset.current;
		  	var current = document.getElementById(curr_mark);
		  	console.log(this.dataset.toggle);
		  	var target = document.getElementById(this.dataset.toggle);
		  	// hide the current
		  	if(current){
			  	current.style.display="none";
			}
		  	// show the target
		  	target.style.display="block";
			for(var i = 0;i <tabs.length;i++){
				classie.remove(tabs[i],"active-tab");
			}
		  	container.querySelector("ul").dataset.current = this.dataset.toggle;
		  	classie.add(this,"active-tab");
		  });
    }
}