/*
 * Created by : Ayman Adam
 */


    toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
    }

function enableEdit(id){
    element = document.getElementById(id) ;
    element.disabled = false ;
    element.focus();
}

function updateTask(id,input){
	var value = input.value;
	if(value =="" || value == null){
		input.value = input.defaultValue;
	}
	else{
		data ={
			"id":id ,
			"newValue":value
		};
	    myAjax(method= "POST", url="/update-task-name" , data=data,
	                response = function(data){
	                    console.log("blur response data ");
	                    console.log(" this is the status for blur: "+data["status"]);
	                    if(data["status"] == "ok"){
	                        console.log("update done secuss");
	                    }
	                    else if (data["status"] == "error"){
	                    	alert("Error ");
	                    }

	                });
	    input.disabled = true ;
        
	}
}

/*===========================================================================*/
/* add new task                                                              */
/*===========================================================================*/
function newtaskElement(){
        var element = document.getElementById("board").firstChild;
    	var li = document.createElement("li");
    	li.name = "this is test";
    	li.className = "task_card task_type_item ui-sortable-handle";
    	li.innerHTML ='<div class="task_header">\
    						<input class="task_title " type="text" onblur="newTask(this)" ></input>\
    						<div class="task_edit" >\
    							<div class="fa fa-pencil fa-fw pull-right"></div>\
    						</div>\
    					</div>\
    					<div class="task-details">\
    						<div class="task-owner">\
    							<img src="">\
    							<div>\
    								<span>Unassigned</span>\
    							</div>\
    						</div>\
    						<div class="task-priority">\
    							<span class="task-priority-value">5</span>\
    						</div>\
    					</div>\
    					<div class="card-footer">\
    						<div class="task-progress">\
    							<div class="progress-container"></div>\
    						</div>\
    					</div>';
    	element.insertBefore(li,element.childNodes[1]);
    	var nameInput = li.querySelector("input");

    	nameInput.focus();
}


function newTask(input){
	console.log("new task function");
	var container = document.querySelector("#board ul");
	var curr_sprint_id = document.querySelector("#work-container .sprint-list ul").id;
	curr_sprint_id = curr_sprint_id.substring(12,curr_sprint_id.length);
    var value = input.value;
    console.log("my new value is : "+value);
    if(value == "" || value == null){
    	container.removeChild(container.childNodes[1]);
    }
    else
    {
    	var data = {
    		"name" : value,
    		"sprint_id" : curr_sprint_id
    	};
        myAjax(method= "POST", url="/add-task " , data =data,

                response = function(data){
                    console.log("ajax new task response : ");
                    console.log(data["status"]);
                    if(data["status"] == "ok"){
                        console.log("success on add");
                        container.childNodes[1].id = 'task-'+data['id'];
                        // TODO: update on blure event 
                    }
                    else {
                    	container.removeChild(container.childNodes[1]);
                        console.log("fail on add ");
                    }

                });
        input.disabled = true ;
    	console.log("else case");

    }
    load_sprint(thisSprint);
}

var task_id ;
function addTaskDetails(object){

    var div = document.createElement('div');
    div.id = "add-task-object";

    var name_span = document.createElement('span');
    name_span.class="label label-info";
    name_span.innerHTML = "Name : " ;

    var name = document.createElement('input');
    name.class= "form-control input-sm";
    name.id="task-name";
    name.type="text";

    var assign_span = document.createElement('span');
    assign_span.class="label label-info";
    assign_span.innerHTML="Assign To : ";

    var assign_input = document.createElement('select');
    assign_input.class ="form-control input-sm";
    assign_input.id="assigntTo";

    var status_span = document.createElement('span');
    status_span.class="label label-info";
    status_span.innerHTML ="Status : ";

    var status_option = document.createElement('select');
    status_option.class="form-control input-sm";
    status.id ="task-columns";

    var estimated_span = document.createElement('span');
    estimated_span.class ="form-control input-sm";
    estimated_span.innerHTML = "Estimated : ";

    var estimated_input = document.createElement('input');
    estimated_input.class = "form-control input-sm"; ;
    estimated_input.id = "estimated";

    var worked_span = document.createElement('span');
    worked_span.class = "form-control input-sm" ;
    worked_span.innerHTML="Worked : "; 

    var worked_input = document.createElement('input');
    worked_input.class = "form-control input-sm" ; 
    worked_input.id="worked";

    var sprint_span = document.createElement('span');
    sprint_span.class = "form-control input-sm";
    sprint_span.innerHTML ="Sprint : ";

    var sprint_option = document.createElement('select');
    sprint_option.class = "form-control input-sm"; 
    sprint_option.id ="Sprint";

    

    id =object.id;
	// to be used when click "save" button
    task_id = id.substring(5,id.length);
    console.log("task id clikced "+id);

    var u = location.href ;
    if(! u.endsWith("/")){
        u +='/';
    }
    u += 'get-task-data';
    u = decodeURIComponent(u);

    myAjax(method= "POST", url=u , data ={"id":task_id},
                //console.log("in ajax methood");
                response = function(data){
                    console.log(data);
                    if(data["status"] == "ok"){
						name.value = data["name"];
                        estimated_input.value = data["estimated_hours"];
                        worked_input.value = data["worked_hours"];
                        status_option.innerHTML="";
						var col_list = data["columns"];
						for (var i = 0 ; i< col_list.length ; i++){
							var option = document.createElement("option");
							option.value = col_list[i]["id"];
							option.innerHTML = col_list[i]["name"];
							status_option.appendChild(option);
						}
                        var sprint_list = data["sprint"];
                        for(var i =0 ; i<sprint_list.length ; i++)
                        {
                            var option = document.createElement("option");
                            option.value = sprint_list[i]["name"]; 
                            option.innerHTML = sprint_list[i]["name"];
                            sprint_option.appendChild(option);
                        }
						if(data["assignTo"]!=  null){
                            var firstOption = document.createElement('option');
                            firstOption.value = data["assignTo"];
                            firstOption.innerHTML =data["assignTo"];
                            assign_input.appendChild(firstOption);

                            var user_list = data["users"];
                            for(var i =0 ; i<user_list.length ; i++)
                            {
                                var option = document.createElement("option");
                                option.value = user_list[i]["name"];
                                option.innerHTML =user_list[i]["name"];
                                assign_input.appendChild(option);
                            }
						}
						else
                        {
                            var firstOption = document.createElement('option');
							firstOption.value = "Unassigned";
                            firstOption.innerHTML ="Unassigned";
                            assign_input.appendChild(firstOption);
                            var user_list = data["users"];
                            for(var i =0 ; i<user_list.length ; i++)
                            {
                                var option = document.createElement("option");
                                option.value = user_list[i]["name"];
                                option.innerHTML =user_list[i]["name"];
                                assign_input.appendChild(option);
                            }
						}

                        div.appendChild(name_span);
                        div.appendChild(name);
                        div.appendChild(assign_span);
                        div.appendChild(assign_input);
                        div.appendChild(status_span);
                        div.appendChild(status_option);
                        div.appendChild(estimated_span);
                        div.appendChild(estimated_input);
                        div.appendChild(worked_span);
                        div.appendChild(worked_input);
                        div.appendChild(sprint_span);
                        div.appendChild(sprint_option);

                        var form = $(div);
                        var d = DynamicModal({
                            tilte:"Add Backlog",
                            body:form,
                            buttons:{
                                submit: {
                                    text: 'Add', // default: 'Submit'
                                    event: addTaskObject , // default: empty
                                },
                                close: {
                                    text: 'Close', // default: 'Close'
                                    event : function(){
                                        d.close() ; 
                                    }
                                },
                            }
                        });

					}
					else{
						console.log("something went wrong");
					}



                });
}

function addTaskObject(){
    var name = document.querySelector("#task-name").value;
    var assign = document.querySelector(".modal-body > div:nth-child(1) > select:nth-child(4)").value;
    var status = document.querySelector(".modal-body > div:nth-child(1) > select:nth-child(6)").value;
    var sprint = document.querySelector("#Sprint").value;
    var worked_hours = document.querySelector("#worked").value;
    var estimated_hours = document.querySelector("#estimated").value ;

    var data = {"id":task_id ,"name":name ,"estimated":estimated_hours, "worked" :worked_hours,"assign":assign , "status":status , "sprint":sprint } ; 
    myAjax(method= "POST", url="/add-task-object" , data = data,
                response = function(data){
                    console.log("save responce");
                    //console.log("ajax new task response : ");
                    if(data["status"] == "ok"){
						alert("ok ok ok ");
                        return true ; 
                    }
                    else
                    {
                        return false ; 
                    }

                });
    load_sprint(thisSprint);
}

function deleteTask(id){
    var x = confirm("Are you sure you want to delete this?");
    if(x){
        myAjax(method="POST",url='/delete-task',data = {"id":id},
            response = function(data){
                if(data['status']=='ok'){
                toastr["success"](data["name"], "Deleted!")
                load_sprint(thisSprint);
                }
                else{
                toastr["error"]("Task Not Deleted!", "Failed")    
                }
            }

            )
    }
    

    
}