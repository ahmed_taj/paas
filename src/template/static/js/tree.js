/*
 * Directory Tree plugin
 *
 *
 */

(function () {
	// helper methods
	var isDict = function(dic){
		return (dic.constructor === Object);
	};

	// core method
	var tree = function (el,o){
		// setup
		this._setup(o);
		this.root = el;

		// init
		/*
			* generate gui elements
			* add events
		*/
		// build tree UI
		this._buildTree();

	};
	tree.prototype = {
		_setup : function(o){

			// default options
			var opts = {
				root : 'Root',
				list : null,
				classes : {},
			};
			// configure user options
			/*for(var el in o){
				console.log(o.el);
				if(opts.el){
					//if(isDict(o.el)){}
					opts.el = o.el;
				}
			}*/
			opts.root = o.root || opts.root;
			opts.list = o.list;
			opts.open = o.open || this._openFile;
			this.opts = opts;
		},
		/*


			<div>
				<ul>
					<li>
					name folder
					<ul>
						<li>name folder
							<span></span>
							<ul>

							</ul>
						</li>

					</ul>
					<li>
				</ul>
			</div>
		*/

		_buildTree : function(){
			var that = this;
			var rootUL = document.createElement('ul');
			var li =  document.createElement('li');

			rootUL.appendChild(this._generate(this.opts.list,this.opts.root));
			this.root.appendChild(rootUL);
		},
		_generate : function(list,parentName){
			// create dir element
			var that = this;
			var dirElement = document.createElement('li');
				dirElement.addEventListener('click',function(evt){
					evt.stopPropagation();
					that._openFolder(this);
				});
				dirElement.className='tree-folder';
				$(dirElement).contextPopup({
					  items: [
						{label:'New file',  action:function(evt) { console.log(evt.target); } },
						{label:'Delete',    action:function(evt) { console.log(evt.target); } },
				]});

			var ulElement = document.createElement('ul');
				ulElement.path = this.opts.root == parentName? '': parentName.slice(1);
				ulElement.style.display = 'none';

			// order the list here and place the > last
			//order list method
			list = this._sortDictionary(list);
			for (key in list){
				if(key === '>' ){
					// append files to parent folder
					this._appendFiles(ulElement,list[key]);
				}
				else{
					// recursion for folders
					//append child folder to this folder
					var result = this._generate(list[key],key);

					ulElement.appendChild(result);
					//ulElement.insertBefore(newItem, list.childNodes[0]);
				}
			}
			dirElement.innerHTML = '<span><span class="ion-android-folder"></span>'+parentName.split('/').pop()+'</span>';
			dirElement.appendChild(ulElement);

			return dirElement;
			// return this folder to be appended to parent
		},
		_appendFiles: function (parent , filesList){
			for(var i = 0 ;i< filesList.length;i++){
				var that = this;
				var li = document.createElement('li');
				li.innerHTML = '<span><span class="ion-document"></span>'+filesList[i]+'</span>';
				li.fileName = filesList[i];
				li.className='tree-file';

				li.addEventListener('click',function(evt){
					evt.stopPropagation();
					this.path = this.parentNode.path;
					that.opts.open(this);
				});

				parent.appendChild(li);
			}
		},
		_openFolder : function(el){
			var ul = el.querySelector('ul');
			if(ul.style.display === 'none'){
				ul.style.display = '';
			}else{
				ul.style.display = 'none';
			}
		},
		_openFile : function(el){
			console.log(el.path+'/'+el.fileName);
		},
		_sortDictionary: function(target){
			// get a sorted list of dictionary keys
			var keys = Object.keys(target).sort(function (a, b){
				return a.toLowerCase().localeCompare(b.toLowerCase());
			});
			var dict = {};
			var iterator = 0;
			for(var i = 0; i < keys.length ; i++){
				if(keys[i]!=">"){
				dict[keys[iterator]] = target[keys[iterator]];
				iterator++;
				}
				else{
				dict[keys[keys.length-1]] = target[keys[keys.length-1]];
				}
			}
			return dict;
		}

	};

	if(window){
		window.tree = function (el,o){
			var e = document.querySelector(el);
			return new tree(e,o);
		};
	}
}());