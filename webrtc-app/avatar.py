import urllib, hashlib
 
# Set your variables here
email = "someone@somewhere.com"
default = "http://s.ytimg.com/yts/img/avatar_720-vflYJnzBZ.png"
size = 50
 
# construct the url
gravatar_url = "http://www.gravatar.com/avatar/" + hashlib.md5(email.lower()).hexdigest() + "?"
gravatar_url += urllib.urlencode({'d':default, 's':str(size)})