from tornado.ioloop import IOLoop
from tornado.web import Application, RequestHandler
import tornado.websocket
from tornado.escape import json_encode,json_decode
from hashlib import sha256
from datetime import datetime # for fake Event class
"""
        START duplication
"""
class BaseHandler(RequestHandler):
    def get_current_user(self):
        cookie_user =  self.get_secure_cookie("_username_")
        return cookie_user

class WebSocketHandler(tornado.websocket.WebSocketHandler):
    '''
    This is the base class of all websocket handers.
    '''
    def get_current_user(self):
        cookie_user =  self.get_secure_cookie("_username_")
        return cookie_user

def get_member(project_id, username):
    '''
    Get specific project member.
    
    @param project_id: Target project object
    @param username: The username as string.
    
    @author: One
    '''
    try:
        #member = model.ProjectMember.get((model.ProjectMember.project_id == project_id)&
        #      (model.ProjectMember.username == username))
        return True
    except orm.DoesNotExist:
        return None
class Event(object):
    def __init__(self,id,author,title,pid):
        self.id =  id
        self.author_id = author
        self.title = title
        self.event_date = datetime.now().strftime("%m/%d/%Y %H:%M%S")
        self.project_id =  pid
class LoginHandler(BaseHandler):
    def get(self):
        self.render('login.html')
    def post(self):
        username = self.get_argument('username')
        self.set_secure_cookie('_username_',username)
        self.write('done')
"""
        END duplication
"""

global_rooms = {}

class Room(object):
    def __init__(self,meeting):
        self.name       = meeting.title
        self.meeting_id = meeting.id
        self.admin      = meeting.author_id
        self.project_id = meeting.project_id
        self.clients    = {}
        self.id = self.__keygen(meeting.event_date)

    def __keygen(self,date):
        #txt = str(self.name)+str(self.project_id)+str(date)
        # do we should care about encoding in production mode?
        #return sha256(txt.encode('utf-8')).hexdigest()
        return self.meeting_id
        
    def add_user(self,user):      
        # if user not in project return False
        self.clients[user.current_user] = user
        return True

    def remove_user(self,user):
        if user.current_user in self.clients:
            self.clients.pop(user.current_user)

    def __repr__(self):
        return self.name
    
class StartMeetingHandler(BaseHandler):

    @tornado.web.authenticated
    def get(self):
        global global_rooms
        
        id = self.get_argument('id',False)
        
        #if no meeting id supplied
        if not id:
            raise tornado.web.HTTPError(400)
        
        meeting = None
        try:
            #meeting = Event.get(id=id)
            meeting = Event(id,self.current_user,"Fake Meeting",1)
        except:
            raise tornado.web.HTTPError(404)
        #if Event.author_id != self.current_user:
        #    raise tornado.web.HTTPError(403)

        room = Room(meeting)
        if room.id in global_rooms:
            print('Meeting Already started!')
        global_rooms[room.id] = room
        
        self.redirect("/meeting/"+str(room.id))

class MeetingHandler(BaseHandler):

    @tornado.web.authenticated
    def get(self,meeting_id):
        global global_rooms
        print(meeting_id)
        if meeting_id not in global_rooms:
            raise tornado.web.HTTPError(404)
        self.render('meeting.html')

class MeetingWsHandler(WebSocketHandler):
    """
    When websocket connection opend
    """
    @tornado.web.authenticated
    def open(self, meeting_id):
        global global_rooms
        self.is_available = True
        self.current_user = self.get_current_user().decode()
        
        if meeting_id in global_rooms:
            self.room = global_rooms[meeting_id]
        else:
            raise tornado.web.HTTPError(404)

    """
    When a message received
    """
    @tornado.web.authenticated
    def on_message(self, message):
        global global_rooms
        data = json_decode(message)
        send_to = False
        res = {}
        
        if data['type'] == 'join':
            participants = list(self.room.clients.keys())
            reply = {}
            
            if self.room.add_user(self):
                print(self.current_user,"connected to room ",self.room)
                reply  = {'type':'join','success':True,'peers':participants} 
            else:
                reply = {'type':'join','success':False}
                
            self.send_json(reply)
                
        elif data['type'] == 'offer':
            res = {'type':'offer','offer':data['offer'],'from':self.current_user}
            send_to = True
            
        elif data['type'] == 'answer':
            res = {'type':'answer','answer':data['answer'],'from':self.current_user}
            send_to = True
            
        elif data['type'] == 'candidate':
            res = {'type':'candidate','candidate':data['candidate'],'from':self.current_user}
            send_to = True
          
        elif data['type'] == 'leave':
            res = {'type':'leave','who':self.current_user}
            print(self.current_user,"disconnect from room ",self.room)
            
            # remove from room
            self.room.remove_user(self)
            
            # notify others
            for c in self.room.clients:
                self.room.clients[c].send_json(res)
            
        else:
            err = {'type':'error','message':'Unrecognized command ['+data['type']+']'}
            self.send_json(err)
        
        # we have a message to be delivered to someone!
        if send_to:
            if data['to'] in self.room.clients:
                self.room.clients[data['to']].send_json(res)

    """
    When websocket connection closed
    """
    @tornado.web.authenticated
    def on_close(self):
        res = {'type':'disconnected','who':self.current_user}
        self.room.remove_user(self)
        
        # notify others
        for c in self.room.clients:
            self.room.clients[c].send_json(res)
    
    """
    Shortcut for sending json message to client
    """
    def send_json(self,msg):
        self.write_message(json_encode(msg))

def main():
    settings = {
        "login_url":'/login',
        "template_path": ".",
        "cookie_secret": "this_is_secret_cookie",
        "static_path": "./static",
    }
    application = tornado.web.Application([
        (r"/login", LoginHandler),
        (r"/meeting/start", StartMeetingHandler),
        (r"/meeting/([^/]*)", MeetingHandler),
        (r"/meeting/ws/([^/]*)", MeetingWsHandler),
    ], debug=True,**settings)
    application.listen(9001)
    tornado.ioloop.IOLoop.instance().start()
main()