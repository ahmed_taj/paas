/*
shims:
	var RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection || window.msRTCPeerConnection;

	var IceCandidate = window.RTCIceCandidate|| window.mozRTCIceCandidate || window.webkitRTCIceCandidate;

	var RTCSessionDescription = window.RTCSessionDescription || window.mozRTCSessionDescription ||window.webkitRTCSessionDescription || window.msRTCSessionDescription;

	navigator.getUserMedia = navigator.getUserMedia || navigator.mozGetUserMedia || navigator.webkitGetUserMedia || navigator.msGetUserMedia;
*/

var name,
    connectedUser;

var username = "ayman";
mid = window.location.pathname.substring(9);
var connection = new WebSocket('ws://'+window.location.host+'/meeting/ws/'+mid);

connection.onopen = function () {
  console.log("Connected");
    var msg = {'type':'join'}
    send(msg);
};

// Handle all messages through this callback
connection.onmessage = function (message) {

  var data = JSON.parse(message.data);

  switch(data.type) {
    case "join":
      	onLogin(data.success);
      	break;
    case "offer":
    	console.log("an offer received from ",data.from)
    	username = data.from;
      	onOffer(data.offer, data.from);
      	break;
    case "answer":
    	console.log("an answer received from ",data.from)
      	onAnswer(data.answer,data.from);
      	break;
    case "candidate":
    	console.log("an ice candidate from ",data.from)
      	onCandidate(data.candidate);
      	break;
    case "leave":
      	onLeave();
      	break;
    default:
      	break;
  }
};

connection.onerror = function (err) {
  console.log("Got error", err);
};

// Alias for sending messages in JSON format
function send(message) {
  if (connectedUser) {
    message.name = connectedUser;
  }

  connection.send(JSON.stringify(message));
};


function onLogin(success) {
  if (success === false) {
    alert("Login unsuccessful, please try a different name.");
  } else {
    // Get the plumbing ready for a call
    startConnection();
  }
};

var yourVideo = document.querySelector('#localStream'),
    theirVideo = document.querySelector('#remote'),
    callButton = document.querySelector('button'),
    yourConnection, connectedUser, stream;

function startConnection() {
  if (hasUserMedia()) {
    navigator.getUserMedia({ video: true, audio: true }, function (myStream) {
      stream = myStream;
      yourVideo.src = window.URL.createObjectURL(stream);

      if (hasRTCPeerConnection()) {
        setupPeerConnection(stream);
      } else {
        alert("Sorry, your browser does not support WebRTC.");
      }
    }, function (error) {
      console.log(error);
    });
  } else {
    alert("Sorry, your browser does not support WebRTC.");
  }
}

function setupPeerConnection(stream) {
  var configuration = {
    "iceServers": [{ "url": "stun:127.0.0.1:9876" }]
  };
  yourConnection = new RTCPeerConnection(configuration);

  // Setup stream listening
  yourConnection.addStream(stream);
  yourConnection.onaddstream = function (e) {
    theirVideo.src = window.URL.createObjectURL(e.stream);
  };

  // Setup ice handling
  yourConnection.onicecandidate = function (event) {
    if (event.candidate) {
      send({
        type: "candidate",
        candidate: event.candidate,
        to : username
      });
    }
  };
}


function hasUserMedia() {
  navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
  return !!navigator.getUserMedia;
}

function hasRTCPeerConnection() {
  window.RTCPeerConnection = window.RTCPeerConnection || window.webkitRTCPeerConnection || window.mozRTCPeerConnection;
  window.RTCSessionDescription = window.RTCSessionDescription || window.webkitRTCSessionDescription || window.mozRTCSessionDescription;
  window.RTCIceCandidate = window.RTCIceCandidate || window.webkitRTCIceCandidate || window.mozRTCIceCandidate;
  return !!window.RTCPeerConnection;
}

callButton.addEventListener("click", function () {
    startPeerConnection();
});

function startPeerConnection() {

  // Begin the offer
  yourConnection.createOffer(function (offer) {
    send({
      type: "offer",
      offer: offer,
      to : username
    });
    yourConnection.setLocalDescription(offer);
  }, function (error) {
    alert("An error has occurred.");
  });
};

function onOffer(offer, name) {
  connectedUser = name;
  yourConnection.setRemoteDescription(new RTCSessionDescription(offer));

  yourConnection.createAnswer(function (answer) {
    yourConnection.setLocalDescription(answer);
    send({
      type: "answer",
      answer: answer,
      to: username
    });
  }, function (error) {
    alert("An error has occurred");
  });
};

function onAnswer(answer,from) {
  yourConnection.setRemoteDescription(new RTCSessionDescription(answer));
};

function onCandidate(candidate) {
  yourConnection.addIceCandidate(new RTCIceCandidate(candidate));
};


function onLeave() {

};
