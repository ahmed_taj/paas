document.addEventListener("DOMContentLoaded", function() {

	function signout(){
		myAjax(method="POST", url="/signout",data=undefined,response=function(data){
			alert("you are logged out");
			window.location.href = "/";
		})
	}

	function setupConference(){
	    var mid = window.location.pathname.substring(9);
		var roomUrl = 'ws://'+window.location.host+'/meeting/ws/'+mid;
		var startButton = document.querySelector('#start-button');

		var room = new Room(roomUrl);

		room.onConnect = function(status){
			if(! status){
				// someting went wrong
				console.log("someting went wrong");
			}
		};

		room.onJoin = function(status){
			if(status){
				// GUI stuff here to let user accept media
			}else{
				// GUI stuff here to notify user he shouldn't be here
			}
		};

		room.setupUserMedia = function(){
			navigator.mediaDevices.getUserMedia({
				video: true,
				audio: true
			})
			.then(function(stream){
				attachMediaStream(document.querySelector("#localStream"),stream);
				room.setupLocalStream(stream);
			})
			.catch(function(err) {
				//TODO: UI
				console.log("Error: Can't get user media.",err);
			});
		};

		startButton.addEventListener("click", function () {
			console.log("click");
			room.startPeersConnections();
		});
	}
	setupConference();
});