/*============================================================================*/
/*                			websocket server								  */
/*============================================================================*/

var ws_notifi
function openWsNotify(){
	ws_notifi = new WebSocket("ws://localhost:8888/notification-center");
	ws_notifi.onmessage = function (e){
		console.log("new message received")
		//var data = JSON.parse(e.data);
		console.log(e.data);
	};

	ws_notifi.onclose = function (e){
		ws_notifi = new WebSocket("ws://localhost:8888/notification-center");
	};
}

window.onload = function(){
	openWsNotify();
}


/*============================================================================*/
/*                			  ajax 											  */
/*============================================================================*/
function myAjax(method,url,data,response){
	if(data == undefined) {
		data= "empty";
	}
	var xmlhttp=new XMLHttpRequest();
	xmlhttp.onreadystatechange=function(){
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			response(JSON.parse(xmlhttp.responseText));
		}
	}
	xmlhttp.open(method,url,true);
	if(data != "empty"){
		xmlhttp.send( JSON.stringify(data));
	}else{
		xmlhttp.send();
	}
}
function myAjaxWithout(method,url,data,response){
	if(data == undefined) {
		data= "empty";
	}
	var xmlhttp=new XMLHttpRequest();
	xmlhttp.onreadystatechange=function(){
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			response(xmlhttp.responseText);
		}
	}
	xmlhttp.open(method,url,true);
	if(data != "empty"){
		xmlhttp.send( data);
	}else{
		xmlhttp.send();
	}
}
/*============================================================================*/
/*                			other core methods								  */
/*============================================================================*/
function signup(){
	var _username = document.getElementById("_username-sup").value;
	var _password = document.getElementById("_password-sup").value;
	var _actualName = document.getElementById("_actualName").value;
	var _Email = document.getElementById("_Email").value;
	var _CompanyName = document.getElementById("_CompanyName").value;
	var _teamSize = document.getElementById("_teamSize").value;
	var _industry = document.getElementById("_industry").value;
	var _Phone = document.getElementById("_Phone").value;

	var data = {
		"_username":_username,
		"_password":_password,
		"_actualName":_actualName,
		"_Email":_Email,
		"_CompanyName":_CompanyName,
		"_teamSize":_teamSize,
		"_industry":_industry,
		"_Phone":_Phone
	};
	myAjax(
		method="POST",
		url="/signup",
		data=data,
		response=function (data){
			if(data["status"] != "ok"){
				//TODO: need a way to good error handling
				if(data["_username"] != "ok"){
					alert("wrong username");
				}
				if(data["_password"] != "ok"){
					alert("wrong password");
				}
				if(data["_actualName"] != "ok"){
					alert("wrong actual name");
				}
				if(data["_Email"] != "ok"){
					alert("wrong email");
				}
				if(data["_CompanyName"] != "ok"){
					alert("wrong company name");
				}
				if(data["_teamSize"] != "ok"){
					alert("wrong team size");
				}
				if(data["_industry"] != "ok"){
					alert("wrong industy");
				}
				if(data["_Phone"] != "ok"){
					alert("wrong phone");
				}
			}
			else{
				alert("OK");
			}
		}
		);

	return false;
}

function signin(){
	var _username = document.getElementById("_username-sin").value;
	var _password = document.getElementById("_password-sin").value;
	var data={
			"_username":_username,
			"_password":_password
	};
	myAjax(
			method="POST",
			url="/signin",
			data=data,
			response=function(data){
				if(data["status"] != "ok"){
					alert("wrong username or password");
				}
				else{
					window.location.href = "/";
				}
			});
	return false;
}

function signout(){
	myAjax(method="POST", url="/signout",data=undefined,response=function(data){
		alert("you are logged out");
		window.location.href = "/";
	})
}
//this function is to check if the user matches the requirements and that the url is correct and hasn't been altered
//TODO:add validation client and server side
function inviteUser(){
	var _uri = window.location.href;
	var _username = document.getElementById("_username-sup").value;
	var _password = document.getElementById("_password-sup").value;
	var _actualName = document.getElementById("_actualName").value;
	var _email = document.getElementById("_Email").value;
	var _phone = document.getElementById("_Phone").value;
	var _timezone = document.getElementById("_Timezone").value;

	var data = {
		"username":_username,
		"password":_password,
		"uri":_uri,
		"actualName":_actualName,
		"email":_email,
		"phone":_phone,
		"timezone":_timezone
	};

	myAjax(method="POST",url="/inviteUser",data=data,response=function(data){
		console.log(data["status"])
		if(data["status"]!="ok"){
			if(data["uri"]!="ok"){
				console.log("corrupted uri");
			}
			if(data["username"]!="ok"){
				console.log("username is already used");
			}
		return false;
		}
		else{
			alert("sign up success")		}
	})

}
//this method is to actually invite the user and add him inside database

function createNewProject(){
	var project_name = document.getElementById('_name').value;
	var data={
	    "_name":project_name ,
	    "_company_id":  document.getElementById('_company_id').value,
	    "_description": document.getElementById('_description').value,
	}
	myAjax(method="POST",url="/create-project",data=data,
		response=function(data){
			if(data["status"] =="ok"){
				document.getElementById("create_project_modal").style.display="none";
				var container = document.querySelector("#overview-container #project-table");
				var new_project = document.createElement("div");
				new_project.className = "project";
				new_project.innerHTML = '<div class="logo">'+
											'<a href="'+data['company_name']+'/'+project_name+'"><img src=""></a>'+
										'</div>'+
										'<div class="details">'+
											'<a href="'+data['company_name']+'/'+project_name+'">'+project_name+'</a>'
											'<span class="time">'+data['last_activity']+'</span>'+
											'<span class="fa fa-close close"></span>'+
										'</div>';
				container.insertBefore(new_project,container.childNodes[0]);
			}
			else{
				alert("something went wrong");
			}
		});
}
function getColumnNumber()
{
	var curr_sprint = document.querySelector("#work-container .sprint-list ul").id;
	var curr_sprint_id = curr_sprint.substring(12,curr_sprint.length);
	var data = {"sprint_id":curr_sprint_id};
	myAjax(method="POST",url="/get-column-number",data=data,
		response=function(response){
			var number = response["number"];
			var columns = Array.range(2,number);
			console.log(columns)
			var order = document.getElementById("new_column_order");
			var html = ""
			for (var i in columns){
				var elem = document.createElement("option");
				elem.value = columns[i];
				elem.innerHTML = columns[i];
				order.appendChild(elem);
			}

		})



}
function addColumn()
{
	var name = document.getElementById("new_column_name").value;
	var order = document.getElementById("new_column_order").value;
	var curr_sprint = document.querySelector("#work-container .sprint-list ul").id;
	var curr_sprint_id = curr_sprint.substring(12,curr_sprint.length);

	var data = {"name":name,
				"order":order,
				"sprint_id":curr_sprint_id
				};
	myAjax(method="POST",url="/add-sprint-column",data=data,
		response=function(response){
			alert(response["status"]);
			console.log("load sprint "+curr_sprint);
				load_sprint(curr_sprint);
		})
}

function addSprint(){
	var container = document.querySelector(".sprint-list ul");
	var new_sprint = document.createElement("li");
	var input_text  = document.createElement("input");
	input_text.addEventListener("blur",function(evt){
		if(this.value == "" | this.value == null){
			container.removeChild(container.childNodes[0]);
		}
		else{
			var link = document.location.pathname.split("/");

			var data ={
				"name":this.value,
				"company_name":link[1].replace("%20"," "),
				"project_name":link[2].replace("%20"," ")
			};

			myAjax(method="POST",url="/add-sprint-with-name",data=data,
				response=function (data){
				if(data["status"]=="ok"){
					new_sprint.id = "sprint-list-"+data["sprint_id"];
					new_sprint.setAttribute("onclick","load_sprint(this.id)");
					new_sprint.innerHTML = "<span>"+input_text.value+"</span>" +"<a>overview</a>" ;
					var active_sprint = container.querySelector(".active");
					container.removeChild(active_sprint);
					container.insertBefore(active_sprint,container.childNodes[0]);
				}
				else{
					console.log("something went wrong :/");
				}
			});

		}
	});
	new_sprint.appendChild(input_text);
	container.insertBefore(new_sprint,container.childNodes[0]);
	input_text.focus();
}


function load_sprint(sprint_id){
	data = {"sprint_id":sprint_id.substring(12,sprint_id.length)};
	myAjax(method="POST", url="/get-sprint", data=data,
		response=function(data){
			var container = document.querySelector("#work-container #board");
			var col_name_container = document.querySelector("#work-container #col-menu");
			// set current sprint to this sprint
			document.querySelector("#work-container .sprint-list ul").id = sprint_id;

			// clear contents divs
			removeAllChilds(container);
			removeAllChilds(col_name_container);

			var curr_col = document.createElement("ul");
			// add 'add task' button
			var btn = document.createElement("button");
			btn.setAttribute("onclick","newtaskElement()");
			btn.className = "add-new-item";
			btn.innerHTML = '<div class="plus-sign">'+
								'<span class="fa">+</span>'+
							'</div>'+
							'<span>New</span>';
			curr_col.appendChild(btn);
			// get the length of received data
			var data_length = Object.keys(data).length;
			// if it has elements
			if(data_length > 0){
				curr_col.id = "col-"+data[0]["col_id"]
				curr_col.className = "task-col col-lg-3 ui-draggable";

				// for first colname
				var col_div = document.createElement("div");
				col_div.className = "col-item";
				col_div.innerHTML = "<span>"+data[0]["col_name"]+"</span>";
				col_name_container.appendChild(col_div);

				var i = 0;
				// the for loop
				for (i; i < data_length ; i++){
					if(curr_col.id != ("col-"+data[i]["col_id"])){
						// for column name
						var col_div = document.createElement("div");
						col_div.className = "col-item";
						col_div.innerHTML = "<span>"+data[i]["col_name"]+"</span>";
						col_name_container.appendChild(col_div);

						// for column itslef
						container.appendChild(curr_col);
						curr_col = document.createElement("ul");
						curr_col.id = "col-"+data[i]["col_id"];
						curr_col.className = "task-col col-lg-3 ui-draggable";
					}
					if(data[i]["task_id"] != null){
						var curr_task = data[i];
						var task = document.createElement("li");
						task.className = "task_card task_type_item modal-button";
						task.id = "task-"+curr_task["task_id"];
						task.setAttribute("data-modal","add-task-object");
						task.setAttribute("data-call","addTaskDetails");
						task.innerHTML = '<div class="task_header">'+
								            '<input  class="task_title " type="text" disabled id="'+curr_task["task_id"]+'" value="'+curr_task["task_name"]+'"  onblur="updateTask('+curr_task["task_id"]+',this)" >'+
								            '<div class="task_edit" onclick="enableEdit(\''+curr_task["task_id"]+'\')">'+
								                '<div class="fa fa-pencil fa-fw pull-right"></div>'+
								            '</div>'+
										  '</div>'+
								          '<div class="task-details">'+
								              '<div class="task-owner">'+
								                  '<img src="" >'+
								                  '<div><span>'+(curr_task["assign_to"] == null ? "Unassigned":curr_task["assign_to"])+'</span></div>'+
								              '</div>'+
								               '<div class="task-priority">'+
								                  '<span class="task-priority-value">'+curr_task["priority"]+'</span>'+
								              '</div>'+
								          '</div>'+
								          '<div class="card-footer">'+
								              '<div class="task-progress">'+
								                  '<div class="progress-container" style="width:'+((Number(curr_task["worked_hours"]) / Number(curr_task["estimated_hours"]))*100)+'px">'+
								                  '</div>'+
								              '</div>'+
								          '</div>';
						curr_col.appendChild(task);
					}
				}
				container.appendChild(curr_col);

				// add dragging event to these tasks
				$(".ui-draggable"  ).sortable({
				    connectWith: ".task-col",
				    receive: function(e, ui) {
				            var drag_id = $(this).attr("id");
				            drag_id = drag_id.substring(4,drag_id.length);
				            var item_id = $(ui.item).attr('id');
				            item_id = item_id.substring(5,item_id.length);
				            console.log("the item "+item_id+" was draged to "+drag_id);
				            myAjax(method= "POST", url="/update-task" , data ={"id":item_id , "draged_id":drag_id},
				                response = function(data){
				                    console.log(" the response of server is  : "+data["status"]);
				                    if(data["status"] == "ok"){
				                        console.log("task status updated succesfully ");
				                    }

				                });
				    return true;
				    }

				    });
				// add show modals event to these tasks
				createModals();
				console.log("end");
			}
	});
}

function addEvent(){
	title= document.querySelector("#_evt_title").value;
	desc = document.querySelector("#_evt_desc").value;
	date = document.querySelector("#_evt_date").value;
	group = document.querySelector("#_evt_group").value;

	var data = {
			"title":title,
			"desc":desc,
			"date":date,
			"group_id":group
	}
	myAjax(method="POST", url="/add-event", data=data,
			response= function(data){
				alert(data["status"]);
				document.querySelector("#add_event_modal").style.display="none";
				temp = JSON.stringify({"group":group,"message":desc});
				ws_notifi.send(temp);
				console.log("message send");
	});

}

function updateCurrentEvent(object){
	var target_id = object.id.substring(6,object.id.length);
	var data = {"event_id":target_id};
	myAjax(method="POST", url="/get-event", data=data,
			response=function(data){
				if(data["status"] == "ok"){
					var container = document.querySelector("#events-container .event-body .container-div");
					container.querySelector(".title span").innerHTML = data["title"];
					container.querySelector(".details .day").innerHTML = data["day"];
					container.querySelector(".details .hour").innerHTML = '<i class="fa fa-clock-o"></i>'+data["hour"];
					container.querySelector(".details .location").innerHTML = '<i class="fa fa-clock-o"></i>'+data["location"];
				}
	});
}

function loadMoreEvent(){
	var container = document.querySelector("#events-container .event-body .content .sub-menu ul");
	var last_event = container.querySelector("li:last-child").id;
	console.log(last_event.substring(6,last_event.length));
	var data = {
		"event_id":last_event.substring(6,last_event.length)
	};
	console.log("here");
	myAjax(method="POST", url="/load-more-events", data=data,
			response=function(data){
				if(data[0]["status"] == "ok"){
					for(var i = 1 ; i < data.length ; i++){
						var li = document.createElement("li");
						li.id = "event-"+data[i]["id"];
						li.setAttribute("onclick","updateCurrentEvent(this)")
						li.innerHTML = '<div><span>'+data[i]["title"]+'</span></div>'+
			      						'<span class="place">'+data[i]["place"]+'</span>'+
			      						'<span class="date">'+data[i]["date"]+'</span>';
						container.appendChild(li);
					}
				}
	});
}
/*******************************************************************************
							helper functions
*******************************************************************************/
function collectionHas(a, b) {
    for(var i = 0, len = a.length; i < len; i ++) {
        if(a[i] == b) return true;
    }
    return false;
}
function findParentBySelector(elm, selector) {
    var all = document.querySelectorAll(selector);
    var cur = elm.parentNode;
    while(cur && !collectionHas(all, cur)) {
        cur = cur.parentNode;
    }
    return cur;
}
function removeAllChilds(target){
	while(target.firstChild){
		target.removeChild(target.firstChild);
	}
}

Array.range= function(a, b, step){
    var A= [];
    if(typeof a== 'number'){
        A[0]= a;
        step= step || 1;
        while(a+step<= b){
            A[A.length]= a+= step;
        }
    }
    else{
        var s= 'abcdefghijklmnopqrstuvwxyz';
        if(a=== a.toUpperCase()){
            b=b.toUpperCase();
            s= s.toUpperCase();
        }
        s= s.substring(s.indexOf(a), s.indexOf(b)+ 1);
        A= s.split('');
    }
    return A;
}
