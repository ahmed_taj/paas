document.addEventListener("DOMContentLoaded", function() {

/*
 *  Peer handler class
 *
 *  Author: ahmed_taj
 */
function Peer(options){
    var self = this;
    if (! (self instanceof Peer))
        return new Peer(options);

    /**
     *  options:
     *      - localstream
     *		- spdOptions
     *      - remoteID
     *      - channel
     */
    for (var o in options) {
        self[o] = options[o];
    }
    self.conn = new RTCPeerConnection(this.sdpOptions);

    // Setup stream listening
    self.conn.addStream(self.localStream);
    self.conn.onaddstream = function (e) {
        // use polyfill (adapter.js) to attach stream to remote element
        attachMediaStream(self.element(),e.stream);
    };

    // Setup ice handling
    self.conn.onicecandidate = function (event) {
        if (event.candidate) {
            self.send({
                type: "candidate",
                candidate: event.candidate,
                to : self.remoteID,
            });
        }
    };
}
Peer.prototype = {
    send : function (msg){
        this.channel.send(JSON.stringify(msg));
    },
    start : function(){
		var self = this;
		// Begin the offer
		self.conn.createOffer(function (offer){
			self.send({
				type: "offer",
				offer: offer,
				to : self.remoteID,
			});
			self.conn.setLocalDescription(offer);
		}, self.onError);
	},
    onOffer: function (offer){
        var self = this;
        self.conn.setRemoteDescription(new RTCSessionDescription(offer));

        self.conn.createAnswer(function (answer) {
            self.conn.setLocalDescription(answer);
            self.send({
                type: "answer",
                answer: answer,
                to: self.remoteID
            });
        },self.onError);
    },
    onAnswer: function (answer){
        this.conn.setRemoteDescription(new RTCSessionDescription(answer));
    },
    onCandidate: function(candidate){
        this.conn.addIceCandidate(new RTCIceCandidate(candidate));
    },
    onError : function(err){
        console.log("Error: ",err);
    },
    element: function(){
        var el = document.querySelector("#"+this.remoteID);
        if (!el){
            el = document.createElement('video');
            el.autoplay = true;
            el.id = this.remoteID;
            document.body.appendChild(el);
        }
        return el;
    }
};



var mid = window.location.pathname.substring(9);
var remotes = {};
var localStream;
var callButton = document.querySelector('button');
var config = {
	"iceServers": [{ "url": "stun:127.0.0.1:9876" }]
};
var ws = new WebSocket('ws://'+window.location.host+'/meeting/ws/'+mid);

// Alias for sending messages in JSON format
function send(message) {
	ws.send(JSON.stringify(message));
};

ws.onopen = function () {
	console.log("Connected");
    var msg = {'type':'join'}
    send(msg);
};

// Handle all messages through this callback
ws.onmessage = function (message) {

  var data = JSON.parse(message.data);

  switch(data.type) {
    case "join":
      	onLogin(data);
      	break;
    case "offer":
    	console.log("an offer received from ",data.from)
    	if(! remotes[data.from]){
			remotes[data.from] = new Peer({
				sdpOptions : config,
				localStream : localStream,
				channel	: ws,
				remoteID : data.from,
			});
		}
      	remotes[data.from].onOffer(data.offer);
      	break;
    case "answer":
    	console.log("an answer received from ",data.from)
      	remotes[data.from].onAnswer(data.answer);
      	break;
    case "candidate":
    	console.log("an ice candidate from ",data.from)
      	remotes[data.from].onCandidate(data.candidate);
      	break;
    case "leave":
      	//onLeave();
      	break;
    default:
      	break;
  }
};

ws.onerror = function (err) {
  console.log("Got error", err);
};

function onLogin(data) {
  if (data.success === false) {
    alert("Login unsuccessful, please try a different name.");
  } else {
	console.log("peers = ",data.peers);
    startConnection(data.peers);
  }
};
function startConnection(peers) {
	navigator.mediaDevices.getUserMedia({
		video: true,
		audio: false
	})
	.then(function(stream){
		//attach stream to local element (using adapter.js)
		attachMediaStream(document.querySelector("#localStream"),stream);
		localStream = stream;


		// setup peers objects
		for (var i in peers){
			remotes[peers[i]] = new Peer({
				sdpOptions : config,
				localStream : localStream,
				channel	: ws,
				remoteID : peers[i],
			});
		}
	})
	.catch(function(err) {
		console.log("Error: Can't get user media.",err);
	});
}

function startPeerConnection() {

	for (var k in remotes){
		remotes[k].start();
	}
};
callButton.addEventListener("click", function () {
    startPeerConnection();
});

});