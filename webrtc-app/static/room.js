/**
 *   Room.js
 *
 *   Depends on:
 *   		- webrtc adapter.js
 *
 *   Author: ahmed_taj
 */

'use strict';
(function(){

	var sdpOptions = {
		"iceServers": [
			{ "url": "stun:127.0.0.1:9876" }
		]
	};


	// Peer Handler

	function Peer(options){
		var self = this;
		if (! (self instanceof Peer))
			return new Peer(options);

		/**
		 *  options = {
		 *      localstream,
		 *      remoteID,
		 *      channel
	 	 *	}
		 */
		for (var o in options) {
			self[o] = options[o];
		}
		self.conn = new RTCPeerConnection(sdpOptions);

		// Setup stream listening
		self.conn.addStream(self.localStream);

		self.conn.onaddstream = function (e) {
			attachMediaStream(self.element(),e.stream);
		};

		// Setup ice handling
		self.conn.onicecandidate = function (event) {
			if (event.candidate) {
				self.send({
					type: "candidate",
					candidate: event.candidate,
					to : self.remoteID,
				});
			}
		};
	}
	Peer.prototype = {
		send : function (msg){
			this.channel.send(JSON.stringify(msg));
		},

		connect : function(){
			console.log("attemping to connect");
			var self = this;
			// Begin the offer
			self.conn.createOffer(function (offer){
				self.send({
					type: "offer",
					offer: offer,
					to : self.remoteID,
				});
				self.conn.setLocalDescription(offer);
			}, self.onError);
		},

		onOffer: function (offer){
			var self = this;
			self.conn.setRemoteDescription(new RTCSessionDescription(offer));

			self.conn.createAnswer(function (answer) {
				self.conn.setLocalDescription(answer);
				self.send({
					type: "answer",
					answer: answer,
					to: self.remoteID
				});
			},self.onError);
		},

		onAnswer: function (answer){
			this.conn.setRemoteDescription(new RTCSessionDescription(answer));
		},

		onCandidate: function(candidate){
			this.conn.addIceCandidate(new RTCIceCandidate(candidate));
		},

		onError : function(err){
			console.log("Error: ",err);
		},

		element: function(){
			var el = document.querySelector("#"+this.remoteID);
			if (!el){
				el = document.createElement('video');
				el.autoplay = true;
				el.id = this.remoteID;
				document.body.appendChild(el);
			}
			return el;
		}
	};

	// Room function

	function Room(roomURL){
		var self = this;

		// forcing instantiation with new
    	if (! (self instanceof Room)){
        	return new Room(roomURL);
        }

		self.channel = new WebSocket(roomURL);

		self.channel.onopen = function () {
			console.log("Room connected");
			// emit connect event
			self.join();
			if(self.onConnect){
				self.onConnect(true);
			}
		};

		// Handle all channel's messages
		self.channel.onmessage = function (message) {

			// expected JSON fromat from server
		  	var data = JSON.parse(message.data);

		  	self.switcher(data);
		};

		self.channel.onerror = function (err) {
			if(self.onConnect){
				self.onConnect(false);
			}
			self.onError(err);
		}
	}

	Room.prototype = {
		remotes : {}, // holds all connected remotes peer's data

		send : function(msg){
			this.channel.send(JSON.stringify(msg));
		},

		join : function(){
			this.send({'type':'join'});
		},

		leave: function(){
			this.send({'type':'leave'});
		},

		startPeersConnections: function(){
			for (var p in this.remotes){
				this.remotes[p].connect();
			}
		},

		peerDisconnected: function(who){
			if(this.remotes[who]){
				var el = this.remotes[who].element();
				el.parentNode.removeChild(el);
				delete this.remotes[who];
			}
		},

		switcher: function(data){
			var self = this;

			switch(data.type){
				case "join":
					self.handleJoin(data);
					break;

				case "offer":
					self.handleOffer(data.offer, data.from);
					console.log("an offer from ",data.from);
					break;

				case "answer":
					self.handleAnswer(data.answer, data.from);
					console.log("an answer from ",data.from);
					break;

				case "candidate":
					self.handleCandidate(data.candidate,data.from);
					console.log("a candidate from ",data.from);
					break;

				case "disconnected":
					self.handleDisconnect(data.who);
					console.log(" disconnected ",data.who);
					break;

				case "leave":
					self.handleLeave(data.who);
					break;

				case "error":
					self.onError(data.message);
					break;

				default:
					self.onError("Unknown type ["+data.type+"]");
					break;
			}
		},

		setupLocalStream : function(stream){
			this.localStream = stream;
			// setup peers objects
			for (var i in this.peers){
				this.remotes[this.peers[i]] = new Peer({
					localStream : this.localStream,
					channel	: this.channel,
					remoteID : this.peers[i],
				});
			}
		},

		// handlers

		handleJoin : function(data){
			if(data.success){
				// emit onJoin event
				if(this.onJoin){
					this.onJoin(true);
				}
				this.peers = data.peers;
				console.log("Available peers are: ",this.peers);
				this.setupUserMedia();

			}else{
				// emit onJoin event
				if(this.onJoin){
					this.onJoin(false);
				}
				self.onError("can't join room.");
			}
		},

		handleOffer: function(offer,from){
			var self = this;

			// if no remote peer already connected
			if(! this.remotes[from]){
				this.remotes[from] = new Peer({
					localStream : self.localStream,
					channel	: self.channel,
					remoteID : from,
				});
			}
      		this.remotes[from].onOffer(offer);
		},

		handleAnswer: function(answer,from){
			this.remotes[from].onAnswer(answer);
		},

		handleCandidate : function(candidate,from){
			this.remotes[data.from].onCandidate(candidate);
		},

		handleDisconnect: function(who){
			//notify use has disconnected
			this.peerDisconnected(who);
		},

		handleLeave: function(who){
			//notify use has left
			this.peerDisconnected(who);
		},

		onError : function(err){
			console.log("Error: ",err);
		},
	};
	window.Room = Room;
})();
