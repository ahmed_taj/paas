/**
*  Room.js
*
*  Depends on:
*  		- adapter.js from [https://github.com/webrtc/adapter/blob/master/adapter.js]
*
*  Author: ahmed_taj
*/
'use strict';
(function(){
    var ws,
    	container=document.body,
    	local = {
			conn: null,
			stream: null,
			element: document.getElementById("localStream"),
		},
		remotes  ;///{}; // list of remote's objects

	var mediaconstraints = {
		video: true,
		audio: true,
	};
	var sdpconfig = {
		"iceServers": [{ "url": "stun:127.0.0.1:9876" }]
	};

	// alias for sending json to server
	function send(msg){
		ws.send(JSON.stringify(msg));
	}

	function rtc(url,cont){
		var self = this;
		// force instantiation without new
		if ( ! (this instanceof rtc) ){
			return new rtc(url,cont);
		}

		ws = new WebSocket(url);
		container = cont || container;

		ws.onopen = function(){
			self.join();
		};
		ws.onmessage = function(message){
			var data = JSON.parse(message.data);
			switch(data.type) {
				case "join":
					self.onJoin(data);
					break;
				case "disconnected":
					self.onDisconnected(data.who);
					break;
				case "offer":
					self.onOffer(data.offer, data.from);
					break;
				case "answer":
					self.onAnswer(data.answer, data.from);
					break;
				case "candidate":
					self.onCandidate(data.candidate,data.from);
					break;
				case "leave":
					self.onLeave(data.who);
					break;
				default:
					console.log("TypeEror:",data.type);
					break;
			}
		};
		ws.onerror = function(err){
			console.log("WebSocketError: ",err);
		};
	}
	rtc.prototype = {
		join:	function(){
			send({
				type:'join'
			});
		},
		leave:	function(){
			send({
				type:'leave'
			});
			//I must stop stream
		},
		start : function(){
			var self = this;
			local.conn.createOffer(function (offer) {
				local.conn.setLocalDescription(offer);
				send({
					type: "offer",
					offer: offer
				});
			}, function (err) {
				console.log("Error: can't start connection.");
			});
		},
		setup:	function(peers){
			var self = this;

			navigator.mediaDevices.getUserMedia({
				video: true,
				audio: false
			})
			.then(function(stream){
				//attach stream to local element (using adapter.js)
				attachMediaStream(local.element,stream);

				local.stream = stream;
				local.conn = new RTCPeerConnection(sdpconfig);
				local.conn.onicecandidate = self.onIceCandidate;
				local.conn.addStream(local.stream);
				local.conn.onaddstream = function(e){
					attachMediaStream(document.querySelector("#remote"),e.stream);
				};
				//for (var i in peers){
				//	self.addPeer(peers[i]);
				//}
			})
			.catch(function(err) {
				console.log("Error: Can't get user media.",err);
			});
		},
		addPeer: function(uid){

		},
		makeElement: function(uid){
			var el = document.createElement('video');
			el.id = uid;
			el.autoplay = true;
			container.appendChild(el);
			return el;
		},
		//events
		onJoin:	function(data){
			if(data.success){
				this.setup(data.peers);
				console.log("peers = ",data.peers);
			}else{
				console.log("Error: can't join room.");
			}
		},
		onDisconnected:	function(who){
			console.log("user ",who," disconnected.");
		},
		onOffer: function(offer,from){
			console.log("offer from ",from);
			var self = this;
			local.conn.setRemoteDescription(new RTCSessionDescription(offer));

			local.conn.createAnswer(function (answer) {
				local.conn.setLocalDescription(answer);
				send({
					type:"answer",
					answer:answer
				});

			}, function (err) {
				console.log("Error: can't create answer from connection.");
			});
		},
		onAnswer: function(answer,from){
			console.log("answer from ",from);
			local.conn.setRemoteDescription(new RTCSessionDescription(answer));
		},
		onCandidate: function(candidate,from){
			console.log("candidate from ",from);
			local.conn.addIceCandidate(new RTCIceCandidate(candidate));
		},
		onIceCandidate: function(event){
			console.log("onIcecandidate");
			if (event.candidate) {
			  send({
				type: "candidate",
				candidate: event.candidate
			  });
			}
		},
		onLeave	: function(who){
			//TODO: just remote the video element
		},
	};
    //export to window
    window.rtc = rtc;
    window.remotes = remotes;
})();
